package libgajaf;

# $Id: libgajaf.pm,v 1.2 2005/08/31 05:17:16 white Exp $ 

$version  = "2.5";
$update   = "20050831";
$expunged = 0;

use vars qw($HASHBYTES $METABYTES);
my($HASHBITS,$METALENG,$RECBYTES);

# Length of Signeture per file.
# if you change $HASHBITS, 
#  you should to change &trHASH() to keep hash-function more suitable.

sub import{
    my $libname = shift;
    my $conffile = shift;
    $conffile = './gajaf.conf' if(!$conffile || $conffile eq 'libgajaf');
    require $conffile;

    $METABYTES ||= 128;
    $HASHBYTES ||= 256;
    $HASHBITS = ($HASHBYTES / 2) * 8;
    $RECBYTES = $METABYTES + $HASHBYTES;
}

sub readDBfile{
    my(%dbfile);
    
    $dbfile{$currentDB} = 1 if defined $currentDB;
    foreach(@stableDB){
	$dbfile{$_} = 1;
    }

    foreach(keys %dbfile){
	open(DBFILE,$_);
	while(!(eof(DBFILE))){
	    read(DBFILE,$data,$RECBYTES);
	    my($uri,$hash) = unpack("A$METABYTES a$HASHBYTES",$data);
	    $bitvecDB{$uri} = { uri => $uri, hash => $hash };
	}
	close(DBFILE);
    }
    $is_DBfile_read = 1;
}

sub openDB{
    open(DBFILE,">>$currentDB");
    $is_DBfile_open = 1;
}

sub closeDB{
    close(DBFILE);
    $is_DBfile_open = 0;
}

sub vacuumDB{
    readDBfile() unless $is_DBfile_read;
    close(DBFILE)  if $is_DBfile_open;
    open(DBFILE,">$currentDB");
    foreach my $uri (sort keys %bitvecDB){
	my($hash) = $bitvecDB{$uri}{hash};
	syswrite(DBFILE,pack("a$METABYTES a$HASHBYTES",$uri,$hash ),$RECBYTES);
    }
    close(DBFILE);
    open(DBFILE,">>$currentDB") if $is_DBfile_open;
}

sub set_currentDB{
    my($file) = @_;
    $currentDB = $file;
}

sub addSignature{
    my($uri,$text) = @_;
    my($hash,$i);

    if($currentDB){
	open(DBFILE,">>$currentDB") if($is_DBfile_open != 1);
	$hash = &make_hash($text);
	syswrite(DBFILE,pack("a$METABYTES a$HASHBYTES",$uri,$hash ),$RECBYTES);
	close(DBFILE)  if($is_DBfile_open != 1);
    }
    if($is_DBfile_read == 1){
	$bitvecDB{$uri} = { uri => $uri, hash => $hash };
    }
}

sub renewSignature{
    my($uri,$text) = @_;
    my($hash,$i);

    readDBfile() unless $is_DBfile_read;
    
    $hash = &make_hash($text);
    $bitvecDB{$uri} = { uri => $uri, hash => $hash };
    
    if($currentDB){
	open(DBFILE,">>$currentDB") if($is_DBfile_open != 1);
	syswrite(DBFILE,pack("a$METABYTES a$HASHBYTES",$uri,$hash ),$RECBYTES);
	close(DBFILE)  if($is_DBfile_open != 1);
    }
}

sub sigCheck{#return list of pointers.
    my($keywords) = @_;
    my($i,$KEY);
    my(@result);
    $KEY = make_hash($keywords);
    foreach $uri (sort keys %bitvecDB){
	if( ($KEY & $bitvecDB{$uri}{hash}) eq $KEY ){
	    push(@result,$uri);
	}else{
	    $expunged++;
	}
    }
    return @result;
}

sub make_hash{
    my($key) = @_;
    my(%wordlist);

    &Jcode::convert(\$key,'euc');
    if($HTMLMODE eq "yes"){$key =~ s/<.*?>//g;}

    %wordlist = make_wordlist($key);
    return make_signature(%wordlist);
}

sub make_wordlist{
    local($_) = @_;
    local($leng) = length($_);

    local(%wordlist);
    local($i,$wordtype) = (0,"parse");
    local($char,$word);
    local($headbyte,$footbyte);
    
    while($i < $leng){
	$char = substr($_,$i++,1);
	if($char =~ /[0-9a-zA-Z]/){# ASCII Alphabet charcter;
	    RESOLVEWORD("ascii");
	}elsif($char le "\xA0"){# ASCII word-parse character;
	    RESOLVEWORD("parse");
	}else{
	    $headbyte = $char;
	    $footbyte = substr($_,$i++,1);
	    $char = $headbyte.$footbyte;
	    
	    if($headbyte ge "\xB0"){# EUC Kanji character;
		RESOLVEWORD("kanji");
	    }elsif($headbyte eq "\xA4"){# EUC hirakana character;
		RESOLVEWORD("kana");
	    }elsif($headbyte eq "\xA5"){# EUC KATAKANA character;
		RESOLVEWORD("KANA");
	    }else{
		if($headbyte eq "\xA1"){# EUC Other character;
		    if($footbyte ge "\xB3"){
			if($footbyte le "\xBA" || $footbyte eq "\xBC"){
			    #EUC supportive character;
			    RESOLVEWORD("EUCsup");
			    next;
			}
		    }
		    if($footbyte lt "\xDC"){
			RESOLVEWORD("other");
			next;
		    }
		}
		RESOLVEWORD("other");
	    }
	}
    }
    RESOLVEWORD("parse");
    return %wordlist;
}

sub RESOLVEWORD{
    local($_) = @_;

    if($_ eq 'kanji'){
	$wordlist{$char} = $_;
    }
    if(/EUCsup/ && $wordtype =~ /(kanji|kana|KANA)/){
	$_ = $wordtype;
    }
    if($_ eq $wordtype){# extend word;
	$word = $word.$char;
    }else{
	if($wordtype eq "ascii" && length($word) >= $HASHLENG{$wordtype}){
	    my($leng) = length($word);
	    $word = lc($word);
	    $wordlist{$word} = $wordtype;
	    $wordlist{substr($word,0,($leng-1))} = $wordtype;
	    $wordlist{substr($word,1,($leng-1))} = $wordtype;
	}
	$word = $char;$wordtype = $_;
	return;
    }
    if(/kanji/){
	if(length($word) >= 4){# 4 = 2 x 2;
	    $wordlist{$word} = $wordtype;
	    $word = substr($word,2);
	}
    }
    if($_ eq 'kana' || $_ eq 'KANA'){
	if(length($word) >= $HASHLENG{$wordtype} * 2){
	    $wordlist{$word} = $wordtype;
	    $word = substr($word,2);
	}
    }
    if($_ eq 'parse' || $_ eq 'other'){
	if($wordtype eq "ascii" && leng($word) >= $HASHLENG{$wordtype}){
	    $wordlist{$word} = $wordtype;
	}
	$word = "";$wordtype = $_;
    }
}

sub make_signature{
    my(%wordlist) = @_;
    my($i,@HASH);
    vec($HASH[0],$HASHBITS - 1,1) = 0;
    vec($HASH[1],$HASHBITS - 1,1) = 0;

    foreach $word (keys %wordlist){
	addHash(\@HASH,$word) if($word ne "");
    }
    return $HASH[0].$HASH[1];
}

sub addHash{
    my($word,@hash);
    (*HASH,$word) = @_;

    $hash[0] = ( modHASH($word,9973) )  % $HASHBITS ;
    $hash[1] = ( modHASH($word,10007) ) % $HASHBITS ;

    vec($HASH[0],$hash[0],1) = 1;
    vec($HASH[1],$hash[1],1) = 1;
}

sub modHASH{
    my($word,$prime) = @_;
    my($i,$result);
    $i = 0; map {$i = $i * 256 + $_;} unpack("C*",$word);
    # can't use % because of overflow.
    $result = $i - int($i / $prime) * $prime;
    return $result;
}

1;
