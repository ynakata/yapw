#!/usr/bin/perl
use lib qw(./);
use Jcode;
use EncodedTreeStore;
opendir(DIR,"DATA");


while(($dir = readdir(DIR))){
    next if ($dir =~ /\.{1,2}/);
    $euc = EncodedTreeStore::filenameDecode($dir);
    $utf8= EncodedTreeStore::filenameEncode($dir),"\n";
    if($euc ne $utf8){
	rename("DATA/$euc","DATA/$utf8") or die $utf8;
    }
}
