package MyCGIlib;
use strict;
use vars qw(@EXPORT @EXPORT_OK @ISA %EXPORT_TAGS);
require Exporter;
@ISA = qw(Exporter);
@EXPORT = qw();
@EXPORT_OK = qw(&setCookie &getCookie &urlencode &urldecode
		&base64encode &base64decode &getQuery);
%EXPORT_TAGS = (
		ALL => [qw(setCookie getCookie urlencode urldecode
			   base64encode base64decode getQuery)]
		);

sub getQuery{
#ARG = () | RETURNS = %cgi_query;
    my(%cgi,$str);

    if($ENV{REQUEST_METHOD} eq "GET"){
	$str = $ENV{QUERY_STRING};	
    }else{	
	read(STDIN,$str,$ENV{'CONTENT_LENGTH'});
    }

    if($ENV{CONTENT_TYPE} =~ m{^multipart/form-data;\s*boundary=(.*)}){
	my $boundary = $1;
#	my $tmp = $str;
#	$tmp =~ s/\n/[LF]/g;
#	$tmp =~ s/\r/[CR]/g;
#	warn $tmp;
	foreach my $part (split(/\-*$boundary\-*(?:\r\n|\r|\n)/,$str)){
#	    my $tmp = $part;
#	    $tmp =~ s/\n/[LF]/g;
#	    $tmp =~ s/\r/[CR]/g;
#	    warn $tmp;
	    
	    my($meta,$body) = split(/(?:\r\n|\r|\n)/,$part,2);
	    next unless($meta);
	    next unless($meta =~ /^Content-Disposition: form-data;/);
	    $meta =~ /name="(.*?)"/;
	    my $name = $1;
	    if($meta =~ /filename="(.*?)"/){
		$cgi{"$name:filename"} = $1;
	    }
	    while($body){
		($meta,$body) = split(/(?:\r\n|\r|\n)/,$body,2);
		last unless $meta;
		if($meta =~ /^Content-Type: (.*)/){
		    $cgi{"$name:Content-Type"} = $1;
		}
	    }
	    $body =~ s/\-+$boundary\-*(?:\r\n|\r|\n)?//s;
	    $body =~ s/\r\n$//;
	    $cgi{$name} = $body;
	}
    }else{
	if($str =~ /=/){
	    foreach my $i (split('&',$str)){
		my($name,$value);
		if($i !~ /\=/){
		    ($name,$value) = ($i,1);
		}else{
		    ($name,$value) = split('=',$i);
		}
		$name = urldecode($name);
		$value =~ tr/+/ /;
		$value = urldecode($value);
		&Jcode::convert(*value,'euc');
		$value =~ s/\015\012/\012/g;
		$value =~ s/\015/\012/g;
		$cgi{$name} = $value;
	    }
	}else{
	    $cgi{$str} = 1;
	}
    }
    return %cgi;
}

sub setCookie{
    my($name,$value,$option) = @_;
    my($result,@day,@month);

    @day = ("Sun","Mon","Tue","Wed",
	    "Thu","Fri","Sat");
    @month = ("","Jan","Feb","Mar","Apr","May","Jun",
	      "Jul","Aug","Sep","Oct","Nov","Dec");
    my($sec,$min,$hour,$day,$month,$year,$wday,$yday,$dst)
        = gmtime(time + 365 * 24 * 60 * 60);
    $month++;$year += 1900;
    my $dateMes = 
        sprintf("%s, %02d-%s-%04d %02d:%02d:%02d GMT"
                ,$day[$wday]
                ,$day,$month[$month],$year
                ,$hour,$min,$sec);
    my $path = $option->{path};
    $path ||= '/';
    if(substr($path,-1) ne '/'){
	$path .= '/';
    }
	
    $result .= "Set-cookie: $name=$value; path=$path; expires=$dateMes;\n";
    return $result;
}

sub getCookie{
#ARG = () | RETURNS = %cookie;
    my(%cookie,$line,$name,$value);
    foreach $line (split(/;\s*/,$ENV{HTTP_COOKIE})){
	($name,$value) = split(/=/,$line);
	$value = urldecode($value);
	$cookie{$name} = $value;
    }
    return %cookie;
}

sub urlencode{
#ARG = $plain | RETURNS = $encoded;
    local($_) = @_;
    s/([^0-9a-zA-Z\-\_\.\!\~\*\(\)\/])/sprintf("%%%02X",unpack("C", $1))/eg;
    return $_;
}

sub urldecode{
#ARG = $encoded | RETURNS = $plain;
    local($_) = @_;
    s/\%([0-9A-Fa-f][0-9A-Fa-f])/pack("C", hex($1))/eg;
    return $_;
}

# SimpleBase64 : 
# URL base64 encoding function for Opera support.
# [Opera6.0 can not treat correctry URL-encoded URL includes non-ascii charctor.]
#  (It's more better divide then into other module.)
# make tables from $base64table.
my(%b64table);
sub base64_init{
    return if scalar(%b64table);
    local($_,@_);
    $b64table{pad} = [0,5,4,3,2,1];
    my $base64table = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_-';
    @_ = split(//,$base64table);
    my $i = 0;
    while(length($_ = shift(@_))){
	my($a,$b) = ($_,substr(unpack("B*",chr($i++)),2));
	die "SimpleBase64 : base table containts '$a' more than twice." if(defined $b64table{dec}{$a});
	$b64table{enc}{$b} = $a;
	$b64table{dec}{$a} = $b;
    }
    die "SimpleBase64 : base table length not enough $i" if($i != 64);
}
sub base64encode{
#ARG = $plain | RETURNS = $encoded;
    local($_) = @_;
    base64_init() if not (%b64table);
    $_  = unpack("B*",$_);
    $_ .= "0" x $b64table{pad}->[length($_) % 6];
    $_  =~ s/(.{6})/$b64table{enc}{$1}/og;
    return $_;
}

sub base64decode{
#ARG = $encoded | RETURNS = $plain;
    local($_) = @_;
    my($pad,$length);
    base64_init() if not (%b64table);
    $_  =~ s/(.)/$b64table{dec}{$1}/og;
    $pad    = length($_) % 8;
    $length = length($_) - $pad;
    $_  = substr($_,0,$length);
    $_  = pack("B*",$_);
    return $_;
}


1;
