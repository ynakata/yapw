#!/usr/bin/perl
print "Content-type: text/plain\r\n";
print "Last-Modified: Fri, 14 Sep 2001 03:26:10 GMT\r\n" if($ENV{REQUEST_METHOD} eq "HEAD");
print "\r\n";
foreach(keys %ENV){
    print "$_\t$ENV{$_}\n";
}
print "\n";
print '$0 ',$0."\n";
($sec,$min,$hour,$day,$mon,$year,$wday,$yday,$stim) = gmtime();

$wday = ("Sun","Mon","Tue","Wed","Thu","Fri","Sat")[$wday];
$mon  = ("Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec")[$mon];
$year += 1900;
print "Content-type: text/html\r\n";
print "Last-Modified: $wday, $day $mon $year $hour:$min:$sec GMT\r\n";
