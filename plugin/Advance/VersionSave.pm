package plugin::Advance::VersionSave;
use strict;
#
# $Id: VersionSave.pm,v 1.9 2007/06/20 07:01:22 white Exp $
#
my $root;
my $VERSIONING_SECOND = 7200;
my $PLUGINNAME = "plugin::Advance::VersionSave";

sub install{
    my $class = shift;
    my $PlugIns = shift;
    $root = shift;
    $PlugIns->add('pre_writepage',\&pre_writepage);
    $PlugIns->add('NavigationToAdvance',\&navigation);
}

sub navigation{
    return "<a href=\"%self%?plugin::Advance::VersionSave::view=current\">����</a>";
}

sub pre_writepage{
    my($target) = shift;
    my($oldsource) = shift;
    my($newsource) = shift;
    
    my $oldtimestamp = $root->__($target)->__timestamp('SOURCE');
    if($oldtimestamp + $VERSIONING_SECOND < time()){
	unless($oldsource){
	    $oldsource = $root->__($target)->SOURCE();
	}
	$root->__($target)->__('VERSION')->$oldtimestamp($oldsource);
    }
    return $newsource;
}

sub view{
    my $source = shift;
    my $target = shift;
    my $cgi    = shift;
    
    my $version = $cgi->{$PLUGINNAME.'::view'};
    return('','redirect') if(! main::existcheck($target));
    my @version = sort $root->__($target)->__('VERSION')->__();
    my $path = main::mklink_getpath($target,0);
    foreach(@version){
	if($version == $_){
	    $source = $root->__($target)->__('VERSION')->$_();
	}
    }
    map {
	my($t) = $_;
	my($y,$m,$d) = (localtime($_))[5,4,3];
	$y +=1900;$m++;
	if($version == $t){
	    $_ = sprintf("{%4d/%02d/%02d\\[<a href='%s?%s::view=%s&diff=1'>��ʬɽ��</a>]}"
			 ,$y,$m,$d,$path,$PLUGINNAME,$_);
	}else{
	    $_ = sprintf("<a href='%s?%s::view=%s'>%4d/%02d/%02d</a>"
			 ,$path,$PLUGINNAME,$_,$y,$m,$d);
	}
    } @version;
    my $list = join(' ',reverse @version);

    if($version == 'current'){
	my $source = $root->__($target)->SOURCE();
	$list = "{<a href='$path?$PLUGINNAME"."::view=current'>�ǿ�</a>} ".$list;
    }else{
	$list = "<a href='$path?$PLUGINNAME"."::view=current'>�ǿ�</a> ".$list;
    }
    
    unless($cgi->{diff}){
	return "�������: $list\n<hr />$source";
    }
    my $tmp;
    my $cur_source = $root->__($target)->SOURCE();
    my $diff_src = main::difftext($source,$cur_source);
    my $mode = '';
    foreach(split("\n",$diff_src)){
	$_ =~ s/(.)$//;
	$tmp .= "$1$_\n";
    }
    $tmp = "<pre>$tmp</pre>";
    $tmp = "�������: $list\n----\n$tmp";
    return $tmp;
}
