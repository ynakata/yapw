package plugin::Advance::DominionSetter;
#
# $Id: DominionSetter.pm,v 1.1 2012/01/10 14:32:28 white Exp $
#
use strict;
my $root;
my $PlugIns;

		
my(@CARD_POOL) = qw{
 �����ڤ� �⾦ ̩�� �ϡ���� �Х��� ������Ƨ�� ���ȿ��� ���� ���� ��������
 ��±�� �ꤤ�ΰ�� ��² �Ļ�Ʋ ��¼ ���ż� �� �̺¤δ� ���ߤ� ����� ��ĥ�� ����̱��¼
 ��׾� ���� ��˼ �� �ҳ��� ��ʪ �ۻ� �ۻ���¼ ����� ������ ���� �Ծ� ���� ����
 �˱� �˺� ��� ���� ����Τ� �޽�� ���� ������ �Ҹ� ¼ �繭�� ��� õ����
 ���결 �˼� �ϲ���¢�� ���� ���� ��α �廡�� ���� ť�� Ŵ���� �� ����
 Ƽ�ٹ��� �˺� �Ȼ߾� �˲������ ��̩������ ��̱�� �����Ͽ� ��ʪ�� ������
 �� ��� ̩�� ̩͢�� ̱ʼ �ڤ��� ��� ͩ���� ����Ʋ
    };
 
sub install{
    my $class = shift;
    $PlugIns = shift;
    $root = shift;

    $PlugIns->add('pre_edit',\&PreEdit);
}

sub PreEdit{
    my $pagename = shift;
    my $content  = shift;

    my $source_page = "���ܡܱ��šܳ���";
    my(@cardset) = map {s/\s+//g;$_} $root->__($source_page)->SOURCE();

    if($content eq ""){
	$content = create_set_text(@cardset);
    }
    return $content;
}

sub create_set_text{
    my(@cardset) = @_;
    my @randomed =
	map  {$_->[0]} sort {$a->[1] <=> $b->[1]} map {[$_, rand()]} @cardset;
    
    my(%selected) = map { ($_ ,1) } @randomed[0...9];
    my(@selected) = grep { $selected{$_} } @cardset;

    return "----\n".join("\n",@selected)."\n----\n";
}


1;
