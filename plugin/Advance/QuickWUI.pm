package plugin::Advance::QuickWUI;
use strict;
#
# $Id: QuickWUI.pm,v 1.1 2004/04/22 09:36:31 white Exp $
#
sub install{
    my $class   = shift;
    my $PlugIns = shift;
    $PlugIns->add('linkconvert',\&linkconvert);
}

sub linkconvert{
    my $text = shift;
    my $page_name = shift;

    unless ($text =~ /^\#/){
	return $text,0;
    }

    my $result;
    $text =~ /^\#(.*?)(?::(.*?)(?::(.*?))?)?$/s;
    my($name,$opt,$val) = ($1,$2,$3);
    if($opt =~ /(cols|rows)/i){
	$result .= "<textarea name='$name' $opt>$val</textarea>";
    }elsif($opt =~ /^submit/){
	if($val){
	    $result .=
		sprintf('<form action="yapw.cgi/%s"><input type="submit" value="%s" /></form>',
			&main::URLencode($val),$name);
	}else{
	    $result .= "<input type='submit' value='$name' />";
	}
    }elsif($opt =~ /^c$/){ 
	my $checked = "checked" if($val);
	$result .= "<input type='checkbox' name='$name' $checked />";
    }elsif($opt =~ /^r$/){
	my $checked = "checked" if($val);
	$result .= "<input type='radio' name='$name' $checked />";
    }elsif($opt eq 'select'){
	$result .= "<select name='$name'>";
	foreach(split(/\,/,$val)){
	    $result .= "<option value='$_'>$_</option>";
	}
	$result .= "</select>";
    }else{
	$result .= "<input type='text' name='$name' value='$val' $opt />";
    }
    return $result,1;
}
1;
