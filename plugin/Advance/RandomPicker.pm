package plugin::Advance::RandomPicker;
#
# $Id: RandomPicker.pm,v 1.3 2012/02/10 15:37:49 white Exp $
#
use strict;
my $root;
my $PlugIns;
my $params;
my $pickset;
		
sub install{
    my $class = shift;
    $PlugIns = shift;
    $root = shift;

    $PlugIns->add('preprocess',\&preprocess);
}

sub is_target{
    my $content = shift;
    return ($content =~ m{^\%RandomPicker:.*\%}m);
}

sub parse_define{
    my $content = shift;
    $content =~ s{^\%RandomPicker:(.*)\%}{}m;
    my $prematch = $`;
    my $postmatch = $';#'
    my $params_def = $2;
    my @tmp = split(/\s+/,$params_def);
    $params = {};
    foreach(@tmp){
	my($key,$value) = split(/=/,$_);
	$params->{$key} = $value;
    }
    $postmatch =~ s{(.*?)\%EndRandomPicker\%}{}s;
    $pickset = $1;
    return ($prematch,$postmatch);
}

sub preprocess{
    my $content = shift;
    my $pagename = shift;
    return $content unless is_target($content);
    my ($pre,$last) = parse_define($content);
    #$content = $pre.qq{<a href='?plugin::Advance::RandomPicker::pick=0'>セット作成</a>}.$last;
    return pick($content,$pagename,{});
}

sub pick{
    my $content = shift;
    my $pagename = shift;
    my $cgi = shift;

    return $content unless is_target($content);
    my ($pre,$last) = parse_define($content);
    my $seed = $cgi->{'plugin::Advance::RandomPicker::pick'};
    if($seed == 0){
	$seed = time ^ ($$ + ($$ << 15));
    }
    srand($seed);
    return $pre.qq{<a href='?plugin::Advance::RandomPicker::pick=$seed'>このセットのURL</a> }.
	qq{<a href='?plugin::Advance::RandomPicker::pick=0'>別のセット</a>\n}.
	create_set_text().$last;
}

sub create_set_text{
    my(@pickset) = grep {!/^\s*$/} split(/\n/,$pickset);
    $params->{pick} ||= 10;
    warn $params->{pick};
    my @randomed =
	map  {$_->[0]} sort {$a->[1] <=> $b->[1]} map {[$_, rand()]} @pickset;
    
    my(%selected) = map { ($_ ,1) } @randomed[0...($params->{pick} -1)];
    my(@selected) = grep { $selected{$_} } @pickset;

    return "----\n*".join("\n*",@selected)."\n----\n";
}
1;
