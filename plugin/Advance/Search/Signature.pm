package plugin::Advance::Search::Signature;
use strict;
use libgajaf;

my $root;
my $PLUGINNAME = 'plugin::Advance::Search::Signature';

sub install{
    my $class = shift;
    my $PlugIns = shift;
    $root = shift;
    $PlugIns->add('Search',\&Search);
    $PlugIns->add('post_writepage',\&post_writepage);
    my $base = $root->__('__')->__('Search')->__('Signature');
    my $file = $base->__path();
    $file .= '/index';
    &libgajaf::set_currentDB($file);
    my $inited = $base->inited();
    if($inited ne 'OK'){
	__init();
	$base->inited('OK');
    }
}

sub __init{
    my(@Index) = $root->__();
    @Index = grep { $_ ne '__' } @Index;
    libgajaf::openDB();
    map {
	my $source = $root->__($_)->SOURCE();
	&libgajaf::addSignature($_,$source);
    } @Index;
    libgajaf::closeDB();
}

sub post_writepage{
    my $pagename  = shift;
    my $oldsource = shift;
    my $newsource = shift;
    &libgajaf::renewSignature($pagename,$newsource);
    return $pagename;
}

sub Search{
    my $key = shift;
    libgajaf::readDBfile();
    return libgajaf::sigCheck($key);
}
