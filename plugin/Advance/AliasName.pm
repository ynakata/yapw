package plugin::Advance::AliasName;
use strict;
#
# $Id: AliasName.pm,v 1.3 2007/06/28 10:49:29 white Exp $
#
my $root;
my %AliasName;
my %ReplaceName;
my %ExternalAliasName;
my $initialized;

sub install{
    my $class   = shift;
    my $PlugIns = shift;
    $root = shift;

    $PlugIns->add('resolve_pathinfo',\&resolve_pathinfo);
    $PlugIns->add('resolve_pagename',\&resolve_pagename);
    $PlugIns->add('linkconvert',\&linkconvert);
    $PlugIns->add('linkconvert2',\&linkconvert2);
    $PlugIns->add('pagenamelist',\&pagenamelist);
}

sub resolve_pathinfo{
    my($src) = shift;
    _read_config() unless($initialized);
    return $AliasName{$src} || $src;
}

sub resolve_pagename{
    my($src) = shift;
    _read_config() unless($initialized);
    if($ReplaceName{$src}){
	return $ReplaceName{$src};
    }
    return $AliasName{$src} || $src;
}

sub pagenamelist{
    _read_config() unless($initialized);
    return keys %AliasName;
}

sub linkconvert{
    my($src) = shift;
    if($ExternalAliasName{$src}){
	return "<a href='$ExternalAliasName{$src}'>$src</a>",1;
    }elsif($ReplaceName{$src}){
	return $ReplaceName{$src},1;
    }else{
	return $src;
    }
}

sub linkconvert2{
    my($src) = shift;
    if($ReplaceName{$src}){
	return sprintf('%s,%s',$src,$ReplaceName{$src});
    }
    return $src;
}

sub _read_config{
    my $alias_str = $root->__('AliasName')->SOURCE();
    foreach my $line (split(/\r\n|\n|\r/,$alias_str)){
	if($line =~ /^\s*(.*?)\s+as\s+(.*)/){
	    my($alias,$entity) = ($1,$2);
	    unless($alias eq 'AliasName'){
		if($entity =~ $main::PATTERN{URL}){
		    $ExternalAliasName{$alias} = $entity;
		}elsif($entity =~ /.*?\#.*?/){
		    $ReplaceName{$alias} = $entity;
		    $AliasName{$alias} = $entity;
		}else{
		    $AliasName{$alias} = $entity;
		}
	    }
	}
    }
    $initialized = 1;
}
1;
