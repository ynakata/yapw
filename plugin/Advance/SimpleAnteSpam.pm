package plugin::Advance::SimpleAnteSpam;
use strict;
#
# $Id: SimpleAnteSpam.pm,v 1.7 2012/02/10 15:36:06 white Exp $
#
my $root;
my $passed;
sub install{
    my $class = shift;
    my $PlugIns = shift;
    $root = shift;
    $PlugIns->add('updatepage',\&updatepage);
    $PlugIns->add('pre_edit',\&pre_edit);
    $PlugIns->add('lastprocess',\&postprocess);
}

sub updatepage{
    my $newsource = shift;
    my $target = shift;
    my $action = shift;
    return $newsource if $action eq 'through';
    return $newsource if $passed;
    my $oldsource = $root->__($target)->SOURCE();

    my $antecode = antecode($oldsource);
    my(@lines) = split(/\r\n|\r|\n/,$newsource);
    if($antecode =~ /$lines[-1]/){
	$passed = 1;
	pop(@lines);
	return join "\n",@lines;
    }else{
	return $newsource,'abort';
    }
}

sub pre_edit{
    my $target  = shift;
    my $content = shift;
    my $keycode = _keycode($content);
    $content .= "\n".antecode($content);
    return $content;
}

sub postprocess{
    my $content = shift;
    my $target  = shift;
    my $oldsource = $root->__($target)->SOURCE();
    my $antecode = antecode($content);
    $content =~ s/\%_simpleAnteSpam_\%/$antecode/;
    return $content;
}

sub antecode{
    my $content = shift;
    return _keycode($content).": spam避けのため、書き込むときはこの行を残したままにしてください。\n";
}

sub _keycode{
    return 'ajleixse';
}

1;
