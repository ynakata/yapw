package plugin::Advance::AAView;
use strict;

my $root;
my $PlugIns;
my $PLUGINNAME = 'plugin::Advance::AAView';

sub install{
    my $class = shift;
    $PlugIns = shift;
    $root = shift;
    $PlugIns->add('NavigationToAdvance',\&navigation);
}

sub navigation{
    return sprintf('<a href="%s?%s::view">AAView</a>',
		   '%self%',
		   $PLUGINNAME);
}

sub view{
    my $content = shift;
    my $target  = shift;
    my $cgi     = shift;

    my $view;
    foreach my $line ( split("\n",$content) ){
	$view .= $line."<br>\n";
    }

    return $view,'through';
}
