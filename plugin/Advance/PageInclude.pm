package plugin::Advance::PageInclude;
use strict;
#
# $Id: PageInclude.pm,v 1.1 2004/03/31 10:02:09 white Exp $
#
my $root;

sub install{
    my $class = shift;
    my $PlugIns = shift;
    $root = shift;

    $PlugIns->add('preprocess',\&preprocess);
}

sub preprocess{
    my $content = shift;
    $content =~ s{(^|[^\\])\%include:(.*?)\%}{my $src = $root->__($2)->SOURCE();$1."<blockquote>\n".$src."\n</blockquote>";}eg;
    return $content;
}

1;
