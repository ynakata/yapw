package plugin::Advance::RemoteWikiLinks;
use strict;
#
# $Id: RemoteWikiLinks.pm,v 1.5 2009/03/25 05:31:13 white Exp $
#
my $root;
my $RWLToken = "::";
my $ShowInterWikiDef = 1;

sub install{
    my $class   = shift;
    my $PlugIns = shift;
    $root = shift;

    $PlugIns->add('linkconvert',\&linkconvert);
#    $PlugIns->add('SpecialPage',\&SpecialPage);
}

sub SpecialPage{
    my $pagename = shift;
    unless($pagename eq 'RemoteWikiLinks'){
	return 0;
    }
}

sub linkconvert{
    my $text = shift;
    my $page_name = shift;

    if($text =~ /(?:InterWiki|RemoteWikiURL)=(.*)/){
	if($ShowInterWikiDef){
	    return "$text ",1;
	}else{
	    return "",1;
	}
    }
    my($face,$body) = main::mklink_splittext($text);
    unless($body =~ /(.*?)$RWLToken(.*)/){
	#記法チェック
	return $text,0;
    }
    my($base,$query) = ($1,$2);
    warn "RemoteWikiLinks: base=",$base," query=",$query,"\n";
    # 分散記述のマッチング：ページ名を元に探索
    my $PAT_PAGES = main::pattern_index($page_name);
    if($body =~ /^($PAT_PAGES)$RWLToken(.*)$/){
	my($base,$query) = ($1,$2);
	my $source = $root->__($base)->SOURCE;
	if($source =~ /\[(?:InterWiki|RemoteWikiURL)::$query=(.*?)\]/){
	    my $url = $1;
	    return "<a href='".$url."'>$face</a>",1;
	}
	if($source =~ /\[(?:InterWiki|RemoteWikiURL)=(.*?)\]/){
	    my $url = $1;
	    $url  .= '%q' unless $url =~ /\%q/;
	    return mkRemoteWikiLink($face,$url,$query);
	}
    }
    # 集中記述のマッチング：'RemoteWikiURL'という名のページをDBとして使用
    my $source = $root->__('RemoteWikiURL')->SOURCE;
    if($source =~ /^$base (.*?)$/m){
	my $url = $1;
	$url  .= '%q' unless $url =~ /\%q/;
	return mkRemoteWikiLink($face,$url,$query);
    }
}
1;

sub mkRemoteWikiLink{
    my $face = shift;
    my $url = shift;
    my $query = shift;
    if($url =~ s/sjis\(\%q\)/\%q/){
	main::codeconv(\$query,'sjis');
    }
    if($url =~ s/euc\(\%q\)/\%q/){
	main::codeconv(\$query,'euc');
    }
    if($url =~ s/jis\(\%q\)/\%q/){
	main::codeconv(\$query,'jis');
    }
    $query = main::urlencode($query);
    $query =~ s/%26amp%3B/&amp;/g;
    $query =~ s/%26/&/g;
    $url   =~ s/\%q/$query/;
    return "<a href='".$url."'>$face</a>",1;
}
