package plugin::Advance::Attach;
use strict;

my $root;
my $PLUGINNAME = 'plugin::Advance::Attach';
my $LimitSize  = 10 * 1024 * 1024; # 1MB

sub install{
    my $class = shift;
    my $PlugIns = shift;
    $root = shift;
    $PlugIns->add('SpecialPage',\&SpecialPage);
    $PlugIns->add('preprocess',\&preprocess);
    $PlugIns->add('postprocess',\&postprocess);
}

sub SpecialPage{
    my $pagename = shift;
    my $method   = shift;
    my $PATTERN = &main::pattern_index();
    if($pagename =~ m{($PATTERN)/(\d+)}){
	my $content = $root->__($1)->__('_attach_')->__($2)->content();
	if(ref($content)){
	    return;
	}
	my $type    = $root->__($1)->__('_attach_')->__($2)->type();
	print &main::httpHead(undef,'content-type' => $type);
	unless($method eq 'HEAD'){
	    print $content;
	}
	exit;
    }
}

sub preprocess{
    my $content = shift;
    my $target  = shift;
    $content =~ s{(^|[^\\])\%attach:(\d+)%}{
	my $prefix    = $1;
	my $attach_no = $2;
	$prefix.$main::baseURL.'/yapw.cgi/'.main::URLencode($target)."/$attach_no";
    }eg;
    return $content;
}

sub postprocess{
    my $content = shift;
    my $target  = shift;
    $content =~ s{(^|[^\\])\%Attach\%}{
	<form method=post enctype="multipart/form-data"><p>File upload:<input type="file" name="filename" /> <input type="submit" name="plugin::Advance::Attach::upload" value="送信" /><input type="reset" value="リセット" /></p>
	    </form>
	}g;
    return $content;
}

sub upload{
    my($source,$page_name,$cgi) = @_;
    my $content  = $cgi->{'filename'};
    my $type     = $cgi->{'filename:Content-Type'};
    my $filename = $cgi->{'filename:filename'};

    exit if(length($content) > $LimitSize);
    exit if(length($content) == 0);
    exit unless $source =~ /\%Attach\%/;
    
    my(@list) = sort { $b <=> $a } $root->__($page_name)->__('_attach_')->__();
    my $newname = $list[0];
    $newname++;
    $root->__($page_name)->__('_attach_')->__($newname)->content($content);
    $root->__($page_name)->__('_attach_')->__($newname)->type($type);
    $root->__($page_name)->__('_attach_')->__($newname)->filename($filename);
    
    my $newsrc = $source;
    if($type =~ /image/){
	$newsrc =~ s/\%Attach\%/\%Attach\%\n<img src='\%attach:$newname\%' \/>/;
    }else{
	$filename ||= $newname;
	$newsrc =~ s/\%Attach\%/\%Attach\%\n[$filename,\%attach:$newname\%\/$filename]/;
    }
    &main::write_page($page_name,$source,$newsrc);
    return '','redirect';
}

1;
