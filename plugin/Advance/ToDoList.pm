package plugin::Advance::ToDoList;
#
# $Id: ToDoList.pm,v 1.3 2007/06/28 10:49:29 white Exp $
#
my $root;
my $Plugins;
my $TargetName = 'ToDo';

sub install{
    my $class = shift;
    $PlugIns = shift;
    $root = shift;

    $PlugIns->add('SpecialPage',\&SpecialPage);
    $PlugIns->add('PostDelete',\&PostDelete);
    $PlugIns->add('pagenamelist',\&PageNameList);
    $PlugIns->add('post_writepage',\&PostWritePage);
    $PlugIns->add('IgnoreEdit',\&IgnoreEdit);
}

sub IgnoreEdit{
    my $pagename = shift;
    return ($pagename eq $TargetName)?1:undef;
}

sub SpecialPage{
    my $pagename = shift;
    unless($pagename eq $TargetName){
	return undef;
    }

    my $content = $root->__($TargetName)->SOURCE();
    return 1,main::makeView($pagename,$content,'through');
}

sub PageNameList{
    return qw(ToDo);
}

sub PostWritePage{
    my $page_name = shift;
    make_todo();
    return $page_name;
}

sub PostDelete{
    make_todo();
}

sub make_todo{
    my(@Index) = $root->__();
    @Index = grep { $_ ne '__' } @Index;
    @Index = sort { $a cmp $b } @Index;
    @Index =
	map{
	    my $source = $root->__($_)->SOURCE();
	    if($source){
		if($source =~ /^\@ToDo/m){
		    $source = $';#';
		    if($source =~ /^\@[^@]/m){
			$source = $`;
		    }
		    $source = "\@$_\n$source";
		}else{
		    '';
		}
	    }
	} @Index;
    my $content = join("\n",('%listofheadings%',@Index));
    $content = main::makeView($TargetName,$content,'template');
    $root->__($TargetName)->SOURCE($content);
}

1;
