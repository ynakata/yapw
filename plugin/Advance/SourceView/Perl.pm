package plugin::Advance::SourceView::Perl;
use strict;

my $PLUGINNAME = 'plugin::Advance::SourceView::Perl';
my $PlugIns;
my $root;
my %Subs;

sub install{
    my $class = shift;
    $PlugIns = shift;
    $PlugIns->add('preprocess',\&preprocess);
    $PlugIns->add('post_writepage',\&post_writepage);
    $root = shift;
}

sub preprocess{
    undef %Subs;
    my $content = shift;
    my $page_name = shift;
    unless($content =~ s/^\%use SourceView::Perl\%\s*\n?//){
	return $content;
    }
    $PlugIns->add('pagenamelist',\&pagenamelist);
    $PlugIns->add('linkconvert2',\&linkconvert2);
    $content =~
	s{^sub ([\w\d_]*)}{
	    $Subs{$1} = sprintf('%s,#%s',$1,$1);
	    sprintf("</pre>\n[*%s]\n<pre>\nsub %s",$1,$1);
	}meg;
    # 他のページからシンボルをimport
    while($content =~ s/^\%SourceView import (.*?)%\s*\n?//){
	my $target_page = $1;
	if(main::existcheck($target_page)){
	    map {$Subs{$_} = sprintf('%s,%s#%s',$_,$target_page,$_)}
	    $root->__($target_page)->__SourceView_symbols();
	}
    }

    return "<pre>\n".$content."\n</pre>";
}

sub pagenamelist{
    return keys %Subs;
}

sub linkconvert2{
    my($src) = shift;
    return $Subs{$src} || $src;
}

sub post_writepage{
    my $page_name = shift;
    my $oldsource = shift;
    my $newsource = shift;
    unless($newsource =~ s/^\%use SourceView::Perl\%\s*\n?//){
	return $page_name;
    }
    undef %Subs;
    while($newsource =~ m{^sub ([\w\d_]*)}mg){
	$Subs{$1} = 1;
    }
    $root->__($page_name)->__SourceView_symbols(keys %Subs);
    warn $PLUGINNAME.'::post_writepage : page=',$page_name,' subs=',join(',',keys %Subs),"\n";
    return $page_name;
}
1;
