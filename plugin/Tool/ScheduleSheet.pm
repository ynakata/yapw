package plugin::Tool::ScheduleSheet;
#
# $Id: ScheduleSheet.pm,v 1.11 2009/10/13 07:37:29 white Exp $
#
use strict;
my $root;
my $PlugIns;
my $mode_edit;
my $target_sheet;
my $target_row;
my $target_col;
my $sheet;

our $style_tool_link = ' style="color:darkgreen;"';

sub install{
    my $class = shift;
    $PlugIns = shift;
    $root = shift;
    $PlugIns->add('parse_regexp',\&parse_regexp);
    $PlugIns->add('postprocess',\&postprocess);
    $PlugIns->add('lastprocess',\&lastprocess);
    $PlugIns->add('EditTools',\&EditTools);
}

sub parse_regexp{
    '%schedule_sheet\s+\d{6}%';
}

sub EditTools{
    return '<input name="plugin::Tool::ScheduleSheet::addSheet" type="submit" value="スケジュール表追加" /><br />';
}

sub addSheet{
    my($content) = shift; #scalar
    my($target)  = shift; #scalar
    my($cgi)     = shift; #reference to hash

    my($template) = q{

%schedule_sheet 200906%
%$type=
%$day=1-30
};
    $content .= $template;

    return ($content,'edit');
}

sub postprocess{
    my $source = shift;
    my $page_name = shift;

    return $source unless _is_target($source);
    if(ref($sheet)){
	return _postprocess_edit_row($source,$page_name) if($mode_edit eq 'row');
	return _postprocess_edit_col($source,$page_name) if($mode_edit eq 'col');
    }
    my $result;
    my $rest = $source;
    while((my($pre,$sheet,$post) = _parse($rest))){
	$result .= $pre;
	$result .= $sheet->view();

	$rest    = $post;
    }
    $result .= $rest;
    return $result;
}

sub lastprocess{
    my $source = shift;
    my $page_name = shift;

    if(ref($sheet) && $mode_edit){
	$source =~ s{<br />}{}g;
    }
    return $source;
}

sub edit{
    my $source    = shift;
    my $page_name = shift;
    my $cgi       = shift;

    return $source unless _is_target($source);

    $target_sheet  = $cgi->{"plugin::Tool::ScheduleSheet::edit"};
    if(defined $cgi->{row}){
	$mode_edit = 'row';
	$target_row = $cgi->{row};
    }elsif(defined $cgi->{col}){
	$mode_edit = 'col';
	$target_col = $cgi->{col};
    }elsif(defined $cgi->{all}){
	$mode_edit = 'row';
	$target_row = -1;
    }

    my $result;
    my $rest = $source;
    while((my($pre,$sheet,$post) = _parse($rest))){
	$rest = $post;
	if($sheet->id eq $target_sheet){
	    $result = $sheet;
	    last;
	}
    }
    return $source unless($result);
    $sheet = $result;
    unless(ref($sheet)){
	warn "sheet=$target_sheet is not valid.";
	exit;
    }
    return $sheet->as_source();
}

sub _postprocess_edit_row{
    my $source = shift;
    my $page_name = shift;

    my $caption = $sheet->{caption};
    my $result  = "<form><table border=1 style='width:100%' class='scheduleSheet'>";
    $result .= "<caption>$caption</caption>";

    my($line_title) = $sheet->titleline();
    $result .= "$line_title";

    foreach my $row ($sheet->rows()){
	if($row->isTarget($target_row)){
	    $result .= $row->editline($sheet);
	}else{
	    $result .=
		main::WikiLink($row->showline($sheet),$page_name,'noescape');
	}
    }
    $result .= "</table></form>";
    return $result;
}

sub _postprocess_edit_col{
    my $source = shift;
    my $page_name = shift;

    my $caption = $sheet->{caption};
    my $result  = "<form><table border=1 class='scheduleSheet'>";
    $result .= "<caption>$caption</caption>";
    
    my($line_title) = $sheet->titleline();
    $result .= "$line_title";
    my $row_count = 0;
    foreach my $row ($sheet->rows()){
	$result .= $row->line_edit_col($sheet,$target_col);
	$row_count++;
    }
    my $num_of_cols = $sheet->{cols} + 1;
    $result .= "<tr><td colspan=$num_of_cols><input type=submit name='plugin::Tool::ScheduleSheet::commit' value='反映' /><input type=hidden name='sheet' value='$target_sheet' /><input type=hidden name='col' value='$target_col' /></td></tr>\n";
    $result .= "</table></form>";
    return $result;
}

sub commit{
    my $source    = shift;
    my $page_name = shift;
    my $cgi       = shift;

    return $source unless _is_target($source);

    $target_sheet = $cgi->{sheet};
    my $result;
    my $sheet_count = 0;
    my $rest = $source;
    while((my($pre,$body,$post) = _parse($rest))){
	$result .= $pre;
	$rest    = $post;
	if($body->isTarget($target_sheet)){
	    $sheet = $body;
	    last;
	}
	$result .= $body->as_source;
    }
    if($sheet){
	$sheet->renew($cgi);
	$result .= $sheet->as_source;
    }
    $result .= $rest;
    if($source ne $result){
	&main::write_page($page_name,$source,$result);
	return '','redirect';
    }else{
	return $source;
    }
}

sub _parse{
    my($source) = shift;
    return unless($source =~ /^\%schedule_sheet\s+(\d{4})(\d{2})\%(.*?)\n/mo);
    my $body;
    my $defline = "$&";
    my $y = $1;
    my $m = $2;
    my $opts  = $3;
    my $pre  = "$`";
    my $post = "$'";
    while($post =~ s/^\%(.*?)\n//so){
	$body .= "$&";
    }
    if($post =~ s/^%(.*)$//){
	$body .= "$&\n";
    }
    $sheet =
	new plugin::Tool::ScheduleSheet::Sheet($y,$m,$defline,$opts,$body);
    return($pre,$sheet,$post);
}

sub _is_target{
    my($source) = shift;
    if($source =~
       /^\%schedule_sheet\s+\d{6}%/mo){
	return 1;
    }else{
	return 0;
    }
}

package plugin::Tool::ScheduleSheet::Sheet;
use Time::Local;

my @WDAY_TEXT
    = (
    '<span style="color:#800">日</span>'
    ,'月'
    ,'火'
    ,'水'
    ,'木'
    ,'金'
    ,'<span style="color:#008">土</span>'
    );

sub wday_text{
    my $self = shift;
    my $day  = shift;
    return $WDAY_TEXT[$self->wday($day)];
}

sub wday{
    my $self = shift;
    my $day  = shift;
    (localtime(timelocal(0,0,0,$day->[0],$self->month ,$self->year)))[6];
}

sub id{
    my $self = shift;
    $self->{id};
}

sub isTarget{
    my $self = shift;
    my $target = shift;
    $self->id() eq $target;
}

sub month{(shift)->{month} - 1}
sub year {(shift)->{year}}

sub new{
    my $class = shift;
    my $y     = shift;
    my $m     = shift;
    my $def_src = shift;
    my $opt   = shift;
    my $src   = shift;

    my $rest = $src;
    my $caption = "$y-$m";
    my $to_day;
    if($m == 12){
	$to_day = (localtime(timelocal(0,0,0,1,1,$y+1)-1))[3];
    }else{
	$to_day = (localtime(timelocal(0,0,0,1,$m,$y)-1))[3];
    }
    my $self = {
	year    => $y,
	month   => $m,
	from_day => 1,
	to_day  => $to_day,
	def_src => $def_src,
	cols    => 1,
	opt     => {},
	caption => $caption,
	id      => "$y$m",
	col_opts    => [],
	COL_OPTS    => [],
	col_styles  => [],
	col_editopt => [],
	holiday => {},
	row => {},
    };
    while($opt =~s/(.*?)=([^\s]*)//){
	my($type,$vals) = ($1,$2);
	$self->{opt}->{$type} ||= {};
	foreach(split(/,/,$vals)){
	    $self->{opt}->{$type}->{$_} = 1;
	}
    }
    if($rest =~ s/^\%(?:\%|\$type=)(.*?)\n//mo){
	# %% or %$type= : 列タイトル
	$self->{col_titles} = new plugin::Tool::ScheduleSheet::Row($1);
	$self->{cols} = $self->{col_titles}->count;
    }
    if($rest =~ s/^\%(?:\?|\$td=)(.*?)\n//mo){
	# %? or %$td= : 各列td/thタグへのオプション指定
	my $def = $1;
	$self->{col_opts} = new plugin::Tool::ScheduleSheet::Row($def);
	$self->{COL_OPTS} = new plugin::Tool::ScheduleSheet::Row($def);
	foreach(@{$self->{COL_OPTS}}){
	    if($_ =~ /^\d+\%?$/){
		$_ = "width='$_'";
	    }
	    $_ = " $_";
	    $_ =~ s/</&lt;/g;
	    $_ =~ s/>/&gt;/g;
	}
    }
    if($rest =~ s/^\%\$days?=(\d+)-(\d+)\n//mo){
	# 日付範囲
	$self->{from_day} = $1;
	$self->{to_day}   = $2;
    }
    if($rest =~ s/^\%\$holiday=(\d+(?:,\d+)*)//mo){
	# 休日指定
	map{ $self->{holiday}->{$_} = 1 } split(/,/,$1);
    }
    while($rest =~ s/^\%(.*?)\n//mo){
	# 列データ
	my $row = new plugin::Tool::ScheduleSheet::Row($1);
	$self->{row}->{$row->[0]} = $row;
    }

    return bless $self,$class;
}

sub titleline{
    my $self = shift;
    my $path = "%cgiURL%/%pathinfo%";

    my $line = '<tr>';
    $line .= '<th>日</th>';
    for(my $i=0;$i<$self->{cols};$i++){
	my $title = $self->{col_titles}->[$i];
	my $opt   = $self->{COL_OPTS}->[$i];
	$line .= sprintf('<th%s><a href="%s?plugin::Tool::ScheduleSheet::edit=%d&amp;col=%d" %s>%s</a></th>',$opt,$path,$self->id,$i + 1,$style_tool_link,$title);
    }
    $line .= "</tr>\n";
    return $line;
}

sub tool_newline{
    my $self = shift;
    my $path = "%cgiURL%/%pathinfo%";
    return sprintf("<tr><td colspan=%d style='text-align:center;'><a href='%s?plugin::Tool::ScheduleSheet::edit=%d&amp;row=new'>&lt;行追加&gt;</a></td><tr>\n",$self->{cols} + 1,$path,$self->{sheet_count});
}

sub renew{
    my $self = shift;
    my $cgi = shift;

    foreach my $key (keys %$cgi){
	next unless($key =~ /^r(\d+)c(\d+)$/);
	my($row,$col) = ($1,$2);
	unless($self->{row}->{$row}){
	    $self->{row}->{$row} = plugin::Tool::ScheduleSheet::Row->new($row);
	}
	$self->{row}->{$row}->[$col] = $cgi->{$key};
    }
    return;
}

sub as_source{
    my $self = shift;
    my $result = $self->{def_src};
    if($self->{col_titles}){
	$result .= '%$type='.substr($self->{col_titles}->as_source,1);
    }
    if(ref($self->{col_opts}) =~ /plugin/){
	#ソースで定義されているならblessされているはず。
	$result .= '%$td='.substr($self->{col_opts}->as_source,1);
    }
    if(ref($self->{col_styles}) =~ /plugin/){
	#ソースで定義されているならblessされているはず。
	$result .= '%$colstyle='.substr($self->{col_styles}->as_source,1);
    }
    if(ref($self->{col_editopt}) =~ /plugin/){
	#ソースで定義されているならblessされているはず。
	$result .= '%$editopt='.substr($self->{col_editopt}->as_source,1);
    }
    $result .= sprintf('%$day=%d-%d'."\n",$self->{from_day},$self->{to_day});
    $result .= sprintf('%$holiday=%s'."\n",join(',',keys %{$self->{holiday}}));
    foreach my $row ($self->rows()){
	$result .= $row->as_source;
    }
    return $result;
}

sub view{
    my $self = shift;

    my $view;
    my $caption = $self->{caption};
    $view = "<table border=1 class='scheduleSheet'>";
    $view .= "<caption>$caption</caption>";

    my($line_title) = $self->titleline();
    $view .= "$line_title";

    foreach my $row ($self->rows()){
	$view .= $row->showline($self);
    }
    $view .= "</table>";

    return $view;
}

sub rows{
    my $self = shift;
    map { $self->{row}->{$_} ||
	      plugin::Tool::ScheduleSheet::Row->new($_);
    } $self->{from_day} ... $self->{to_day};
}

package plugin::Tool::ScheduleSheet::Row;
use Time::Local;

sub new{
    my $class = shift;
    my $str   = shift;
    my $self  = [];
    my $current;
    foreach(split(/(\[.*?\]|,)/,$str)){
	if(/^,$/){
	    push(@$self,$current);
	    $current = "";
	}else{
	    $current .= $_;
	}
    }
    push(@$self,$current) if($current);
    return bless $self,$class;
}

sub count{
    my $self = shift;
    return scalar(@$self);
}

sub as_source{
    my $self = shift;
    my(@converted);
    return "%".join(',', map{s/\n/\/\//g;$_;} @$self)."\n";
}

sub isTarget{
    my $self = shift;
    my $target = shift;
    $self->[0] eq $target;
}

sub showline{
    my $self      = shift;
    my $sheet     = shift;

    my $line = sprintf("<tr%s><th class=schedule_sheet_day >%s%s</th>"
		       ,$self->bgstyle()
		       ,$self->_edit_link($sheet->id)
		       ,$sheet->wday_text($self)
	);

    for(my $i=1;$i < $sheet->{cols}+1;$i++){
	my $opt   = $sheet->{COL_OPTS}->[$i];
	my $style = $sheet->{col_styles}->[$i];
	my $content = $self->[$i];
	$content = "&nbsp;" if($content =~ /^\s*$/);
	$content =~ s{//}{<br />}g;
	$style = " style='$style'" if($style);
	$line .= sprintf("<td%s%s>%s</td>",
			 $opt,$style,$content);
    }
    $line .= "</tr>\n";
    return $line;
}

sub editline{
    my $self      = shift;
    my $sheet     = shift;

    my $sheet_count = $sheet->{sheet_count};
    my $line = sprintf('<tr %s><th%s>%s%s<input type=submit name="plugin::Tool::ScheduleSheet::commit" value="反映" /><input type=hidden name="sheet" value="%s" /><input type=hidden name="row" value="%s" /></th>'
		       ,$self->bgstyle()
		       ,$style_tool_link
		       ,$self->[0]
		       ,$sheet->wday_text($self)
		       ,$target_sheet
		       ,$target_row);

    for(my $i=1;$i<$sheet->{cols}+1;$i++){
	$line .= $self->_input_cell($sheet,'c',$self->[0],$i);
    }
    $line .= "<td><input type=submit name='plugin::Tool::ScheduleSheet::commit' value='反映' /></td>";
    $line .= "</tr>\n";
    return $line;
}

sub bgstyle{
    my $self = shift;
    if($self->isHoliday){
	return ' style="background-color:silver;"';
    }
    return '';
}

sub isHoliday{
    my $self = shift;
    my $wday = $sheet->wday($self);
    if($wday == 0 || $wday == 6 || $sheet->{holiday}->{$self->[0]}){
	return 1;
    }
    return undef;
}

sub line_edit_col{
    my $self      = shift;
    my $sheet     = shift;
    my $target_col = shift;

    my $line = sprintf("<tr%s><th>%s%s</th>"
		       ,$self->bgstyle()
		       ,$self->_edit_link($sheet->id())
		       ,$sheet->wday_text($self)
	);
    for(my $i=1;$i<$sheet->{cols}+1;$i++){
	if($i == $target_col){
	    $line .= $self->_input_cell($sheet,'r',$self->[0],$i);
	}else{
	    my $opt   = $sheet->{COL_OPTS}->[$i];
	    my $style = $sheet->{col_styles}->[$i];
	    $style = " style='$style'" if($style);
	    my $content = $self->[$i];
	    $content = "&nbsp;" if($content =~ /^\s*$/);
	    $line .= sprintf("<td%s%s>%s</td>",$opt,$style,$content);
	}
    }
    $line .= "</tr>\n";
    return $line;
}

sub _input_cell{
    my $self  = shift;
    my $sheet = shift;
    my $type  = shift;
    my $r     = shift;
    my $c     = shift;

    my $editopt = $sheet->{col_editopt}->[$c];
    $editopt = "size='$editopt'" if($editopt =~ /^\d+$/);
    $editopt = ' '.$editopt if($editopt);

    my $style = $sheet->{col_styles}->[$c];
    $style = " style='$style'" if($style);

    my $name;
    if($type eq 'r'){
	$name = 'r'.$r;
    }else{
	$name = 'c'.$c;
    }
    $name = sprintf('r%dc%d',$r,$c);

    if($sheet->{value_range}){
	my $currval = $self->[$c];
	my $ret = sprintf("   <td%s><select name='%s'>"
		       ,$style,$name);
	foreach my $v ('',@{$sheet->{value_range}}){
	    $ret .= sprintf('<option%s>%s</option>'
			    ,($v eq $currval)?' selected':''
			    ,$v);
	}
	$ret .= "</select></td>";
	return $ret;
    }else{
	my $content = $self->[$c];
	$content =~ s{//}{\n}g;
	return sprintf("   <td%s><textarea cols='30' rows='3' style='width:98%;' name='%s' %s>%s</textarea></td>"
		       ,$style,$name,$editopt,$content);
    }
}

sub _edit_link{
    my $self = shift;
    my $sheet_id = shift;
    my $path = "%cgiURL%/%pathinfo%";
    return sprintf('<a href="%s?plugin::Tool::ScheduleSheet::edit=%s&amp;row=%d"%s>%d</a>'
		   ,$path,$sheet_id,$self->[0],$style_tool_link,$self->[0]);
}

1;
