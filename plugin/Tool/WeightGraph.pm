package plugin::Tool::WeightGraph;
#
# $Id: WeightGraph.pm,v 1.2 2004/03/02 08:13:04 white Exp $
#

# pattern of number
my $P_NUM = '\d+(?:\.\d+)?';

sub install{
    my $class = shift;
    my $PlugIns = shift;

    $PlugIns->{'linkconvert'} ||= [];
    push(@{$PlugIns->{'linkconvert'}},\&linkconvert);

    $PlugIns->{'updatepage'} ||= [];
    push(@{$PlugIns->{'updatepage'}},\&updatepage);
}

sub linkconvert{
    my $source = shift;
    my $page_name = shift;
    $source =~ s/\!weight(?::$P_NUM-$P_NUM)?/
	sprintf("%s<img src='g\/%s.png?%s' \/>"
		,$1,main::URLencode($page_name),main::Timestamp($page_name))/eg;
    return $source,1;
}

sub updatepage{
    my $source = shift;
    my $page_name = shift;
    warn "called";
    make_graph($page_name,$source);
    return $source;
}

sub make_graph{
    my $target = shift;
    my $source = shift;
    my $GNUPLOT = "/usr/bin/gnuplot";
    my($tmp,$min,$max);
    if($source =~ /\[\!weight(?::($P_NUM)-($P_NUM))?\]/){
       $min = $1 || 30;
       $max = $2 || 100;
    }else{
       return;
    }
    while($source =~ /^(\d+)\/(\d+)\s+(\d+(?:\.\d+)?)/omg){
       my($m,$d,$w) = ($1,$2,$3);
       if(0){
	   $m = $m-1;
	   $d = $d+1;
       }
       $tmp .= sprintf("%d/%d %s\n",$m,$d,$w);
    }
    open(TMP,"> ./g/__data__");
    print TMP $tmp;
    close(TMP);
    open(PLOT,"| $GNUPLOT > ./g/$target.png");
    print PLOT qq!
	set xdata time
	set timefmt "%m/%d"
	set format x "%m/%d"
	set xlabel "date"
	set ylabel "weight"
	set title "WeightWikiGraph"
	set yrange[$min:$max]
	set terminal png small color
	plot "g/__data__" using 1:2 title "weight" with linespoints
	exit
	!; 
    close(PLOT);
}

1;
