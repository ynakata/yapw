package plugin::Tool::ScheduleCallender;
#
# $Id: ScheduleCallender.pm,v 1.2 2010/11/08 11:43:49 white Exp $
#
use strict;
my $root;
my $PlugIns;
my $mode_edit;
my $target_sheet;
my $target_row;
my $target_col;
my $sheet;

my $postValue;
my $filter;

our $style_tool_link = ' style="color:darkgreen;"';
my $MODULE_NAME = 'plugin::Tool::ScheduleCallender';

sub install{
    my $class = shift;
    $PlugIns = shift;
    $root = shift;
    $PlugIns->add('parse_regexp',\&parse_regexp);
    $PlugIns->add('postprocess',\&postprocess);
    $PlugIns->add('lastprocess',\&lastprocess);
}

sub parse_regexp{
    '%callender%';
}

sub postprocess{
    my $source = shift;
    my $page_name = shift;

    return $source unless _is_target($source);
    if(ref($sheet)){
	return _postprocess_edit_row($source,$page_name) if($mode_edit eq 'row');
	return _postprocess_edit_col($source,$page_name) if($mode_edit eq 'col');
    }
    my $view;

    my $callender =
	new plugin::Tool::ScheduleCallender::Callender($source,$filter);

    my $post = $MODULE_NAME.'::post';
    $view .= "<form><input type=submit name='$post' /><input type=text name='v' value='$postValue' /></form>";

    $view .= $callender->view();

    return $view;
}

sub lastprocess{
    my $source = shift;
    my $page_name = shift;

    if(ref($sheet) && $mode_edit){
	$source =~ s{<br />}{}g;
    }
    return $source;
}

sub edit{
    my $source    = shift;
    my $page_name = shift;
    my $cgi       = shift;

    return $source unless _is_target($source);

    $target_sheet  = $cgi->{"plugin::Tool::ScheduleSheet::edit"};
    if(defined $cgi->{row}){
	$mode_edit = 'row';
	$target_row = $cgi->{row};
    }elsif(defined $cgi->{col}){
	$mode_edit = 'col';
	$target_col = $cgi->{col};
    }elsif(defined $cgi->{all}){
	$mode_edit = 'row';
	$target_row = -1;
    }

    my $result;
    my $rest = $source;
    while((my($pre,$sheet,$post) = _parse($rest))){
	$rest = $post;
	if($sheet->id eq $target_sheet){
	    $result = $sheet;
	    last;
	}
    }
    return $source unless($result);
    $sheet = $result;
    unless(ref($sheet)){
	warn "sheet=$target_sheet is not valid.";
	exit;
    }
    return $sheet->as_source();
}

sub _postprocess_edit_row{
    my $source = shift;
    my $page_name = shift;

    my $caption = $sheet->{caption};
    my $result  = "<form><table border=1 style='width:100%' class='scheduleSheet'>";
    $result .= "<caption>$caption</caption>";

    my($line_title) = $sheet->titleline();
    $result .= "$line_title";

    foreach my $row ($sheet->rows()){
	if($row->isTarget($target_row)){
	    $result .= $row->editline($sheet);
	}else{
	    $result .=
		main::WikiLink($row->showline($sheet),$page_name,'noescape');
	}
    }
    $result .= "</table></form>";
    return $result;
}

sub _postprocess_edit_col{
    my $source = shift;
    my $page_name = shift;

    my $caption = $sheet->{caption};
    my $result  = "<form><table border=1 class='scheduleSheet'>";
    $result .= "<caption>$caption</caption>";
    
    my($line_title) = $sheet->titleline();
    $result .= "$line_title";
    my $row_count = 0;
    foreach my $row ($sheet->rows()){
	$result .= $row->line_edit_col($sheet,$target_col);
	$row_count++;
    }
    my $num_of_cols = $sheet->{cols} + 1;
    $result .= "<tr><td colspan=$num_of_cols><input type=submit name='plugin::Tool::ScheduleSheet::commit' value='反映' /><input type=hidden name='sheet' value='$target_sheet' /><input type=hidden name='col' value='$target_col' /></td></tr>\n";
    $result .= "</table></form>";
    return $result;
}

sub post{
    my $source    = shift;
    my $page_name = shift;
    my $cgi       = shift;
    return $source unless _is_target($source);

    my $v = $postValue = $cgi->{v};
    return $source unless $v;

    #コマンドとして受け取れなかった場合、filterとして認識する
    $filter = $v;
    return $source;
}

sub commit{
    my $source    = shift;
    my $page_name = shift;
    my $cgi       = shift;

    return $source unless _is_target($source);

    $target_sheet = $cgi->{sheet};
    my $result;
    my $sheet_count = 0;
    my $rest = $source;
    while((my($pre,$body,$post) = _parse($rest))){
	$result .= $pre;
	$rest    = $post;
	if($body->isTarget($target_sheet)){
	    $sheet = $body;
	    last;
	}
	$result .= $body->as_source;
    }
    if($sheet){
	$sheet->renew($cgi);
	$result .= $sheet->as_source;
    }
    $result .= $rest;
    if($source ne $result){
	&main::write_page($page_name,$source,$result);
	return '','redirect';
    }else{
	return $source;
    }
}

sub _is_target{
    my($source) = shift;
    my $pattern = parse_regexp();
    if($source =~
       /^$pattern/mo){
	return 1;
    }else{
	return 0;
    }
}

package plugin::Tool::ScheduleCallender::Callender;
use Time::Local;

sub new{
    my $class = shift;
    my $source = shift;
    my $filter = shift;
    my(@lines) = split(/\n/,$source);
    my $self = {};
    my(@filtered);
    {
	foreach(@lines){
	    next if(/^\%callender\%/);
	    if(/^\%\$(.*?)=(.*)$/){
		$self->{$1} = $2;
		next;
	    }
	    push(@filtered,$_);
	}
	(@lines) = @filtered;
    }
    if($filter){
	(@lines) = grep { /$filter/ } @lines;
    }
    my $ENTRY_CLASS = 'plugin::Tool::ScheduleCallender::Entry';
    my(@lines) =
	sort {$a->sort_order() cmp $b->sort_order()} map {$ENTRY_CLASS->new($_)} @lines;

    $self->{entry} = \@lines;

    return bless $self,$class;
}

sub view{
    my $self = shift;

    my $view;
    my $caption = $self->{caption};
    $view = "<table border=1 class='callender'>";
    $view .= "<caption>$caption</caption>";

    my($line_title) = $self->titleline();
    $view .= "$line_title";

    if($self->{type}){
	$view .= '<tr>';
	$view .= "<th></th>";
	foreach my $type (split(',',$self->{type})){
	    $view .= "<th>$type</th>";
	}
	$view .= '</tr>';
    }
    my $lastDate;
    foreach my $day ($self->days()){
	my $date = $day->date();
	$date =~ s{\/}{}g;
	if(length($date) == 8){
	    while($date && $lastDate && $date != ++$lastDate){
		my $blankDay = plugin::Tool::ScheduleCallender::Day->new($lastDate);
		$view .= $blankDay->showline($self);
	    }
	    $lastDate = $date;
	}
	$view .= $day->showline($self) if $day->date();
    }

    $view .= "</table>";

    return $view;
}

sub titleline{
}

sub entries{
    my $self = shift;
    return @{$self->{entry}};
}

sub days{
    my $self = shift;
    my(@days);
    my(@day);
    my $lastDay;
    foreach my $entry ($self->entries()){
	if($lastDay ne $entry->date){
	    my $module = $MODULE_NAME.'::Day';
	    push(@days,$module->new(@day));
	    undef(@day);
	    $lastDay = $entry->date;
	}
	push(@day,$entry);
    }
    return @days;
}

package plugin::Tool::ScheduleCallender::Day;
use Time::Local;
sub new{
    my $class = shift;
    my $date;
    my $holiday;
    unless(@_){
	$date = '';
    }elsif(ref($_[0])){
	$date = $_[0]->date();
    }else{
	$date = $_[0];
	if($date =~ /^(\d{4})(\d{2})(\d{2})$/){
	    $date = "$1/$2/$3";
	}
    }
    my $since_epoch;
    {
	$date =~ m{^(\d{4})/(\d{2})/(\d{2})$};
	my($day,$month,$year) = ($3,$2,$1);
	$since_epoch = timelocal(0,0,0,$day?$day:1,$month?$month-1:0,$year);
    }
    my(@entries);
    foreach(@_){
	next unless ref $_;
	if($_->tag eq '$holiday'){
	    $holiday = $_->text;
	    next;
	}
	push(@entries,$_);
    }
    return bless {
	date => $date,
	holiday => $holiday,
	entry => \@entries,
	wday => (localtime($since_epoch))[6],
    };
}

sub entries{ @{(shift)->{entry}} }
sub date   { (shift)->{date} }
sub holiday{ (shift)->{holiday} }

my(@WDAY) = ('Sun','Mon','Tue','Wed','Thu','Fri','Sat');
sub wday{
    my $self = shift;
    return $WDAY[$self->{wday}];
}
sub TR_class{
    my $self = shift;
    my(@classes);
    push(@classes,'holiday') if $self->holiday;
    push(@classes,$self->wday);
    return join(' ',@classes);
}

sub showline{
    my $self = shift;
    my $parent = shift;
    if($parent->{type}){
	my $view = sprintf('<tr class="%s"><td>%s</td>'
			   ,$self->TR_class,$self->date);
	my(%type);
	foreach my $entry ($self->entries){
	    my $tag = $entry->tag();
	    my $text = $entry->text();
	    $type{$tag} ||= [];
	    push(@{$type{$tag}},$text);
	}
	foreach my $type (split(',',$parent->{type})){
	    $type{$type} ||= [];
	    $view .= sprintf('<td>%s</td>'
			     ,join('<br />',@{$type{$type}}));
	}
	$view .= '</tr>';
	return $view;
    }else{
	my $view = sprintf('<tr class="%s"><td colspan=3>%s %s</td></tr>'
			   ,$self->TR_class,$self->date,$self->holiday);
	foreach my $entry ($self->entries){
	    my $tag = $entry->tag();
	    my $text = $entry->text();
	    $view .= sprintf('<tr><td>%s</td><td>%s</td></tr>'
			     ,$tag ,$text);
	}
	return $view;
    }
}


package plugin::Tool::ScheduleCallender::Entry;
use Time::Local;
sub showline{
    my $self = shift;
    my $tag = $self->{tag};
    my @class;
    if($tag =~ /^\$(.*)/){
	push(@class,$1);
	$tag = '';
    }
    return sprintf('<tr class="%s"><td>%s</td><td>%s</td><td>%s</td>'
		   ,join(',',grep {$_} ($tag,$self->wday(),@class))
		   ,$self->date
		   ,$tag
		   ,$self->text
		   ,$self->sort_order
	);
}

sub sort_order{
    my $self = shift;
    return $self->{_sort_order};
}

sub date { (shift)->{date} }
sub tag  { (shift)->{tag} }
sub text { (shift)->{text} }

my(@WDAY) = ('Sun','Mon','Tue','Wed','Thu','Fri','Sat');
sub wday{
    my $self = shift;
    return $WDAY[$self->{_wday}];
}

sub new{
    my $class = shift;
    my $source = shift;
    my $self  = {};
    my $orig = $source;
    #remove head '%'
    $source = substr($source,1);
    #year
    $source =~ s/^(\d{4}),//;
    my $year   = $1;
    #month
    $source =~ s/^(\d{1,2})?,//;
    my $month   = $1;
    #day
    $source =~ s/^(\d{1,2})?,//;
    my $day   = $1;
    #tag
    $source =~ s/^(.*?),//;
    my $tag   = $1;
    #property
    $source =~ s/^(.*?),//;
    my $property = $1;
    my $since_epoch = timelocal(0,0,0,$day?$day:1,$month?$month-1:0,$year);
    my $date;
    if($day){
	$date =  sprintf('%04d/%02d/%02d',$year, $month, $day);
    }elsif($month){
	$date =  sprintf('%04d/%02d',$year, $month);
    }elsif($year){
	$date =  sprintf('%04d',$year);
    }

    my $self = {
	year  => $year,
	month => $month,
	day   => $day,
	tag   => $tag,
	property => $property,
	text  => $source,
	orig  => $orig,
	date => $date,
	_wday => (localtime($since_epoch))[6],
    };
    $self->{_sort_order} = 
	sprintf('%04d%02d%02d%s,%s'
		,$year,$month||0,$day||0,$self->{tag},$self->{text});

    return bless $self,$class;
}


###old source

sub count{
    my $self = shift;
    return scalar(@$self);
}

sub as_source{
    my $self = shift;
    my(@converted);
    return "%".join(',', map{s/\n/\/\//g;$_;} @$self)."\n";
}

sub isTarget{
    my $self = shift;
    my $target = shift;
    $self->[0] eq $target;
}

sub editline{
    my $self      = shift;
    my $sheet     = shift;

    my $sheet_count = $sheet->{sheet_count};
    my $line = sprintf('<tr %s><th%s>%s%s<input type=submit name="plugin::Tool::ScheduleSheet::commit" value="反映" /><input type=hidden name="sheet" value="%s" /><input type=hidden name="row" value="%s" /></th>'
		       ,$self->bgstyle()
		       ,$style_tool_link
		       ,$self->[0]
		       ,$sheet->wday_text($self)
		       ,$target_sheet
		       ,$target_row);

    for(my $i=1;$i<$sheet->{cols}+1;$i++){
	$line .= $self->_input_cell($sheet,'c',$self->[0],$i);
    }
    $line .= "<td><input type=submit name='plugin::Tool::ScheduleSheet::commit' value='反映' /></td>";
    $line .= "</tr>\n";
    return $line;
}

sub bgstyle{
    my $self = shift;
    if($self->isHoliday){
	return ' style="background-color:silver;"';
    }
    return '';
}

sub isHoliday{
    my $self = shift;
    my $wday = $sheet->wday($self);
    if($wday == 0 || $wday == 6 || $sheet->{holiday}->{$self->[0]}){
	return 1;
    }
    return undef;
}

sub line_edit_col{
    my $self      = shift;
    my $sheet     = shift;
    my $target_col = shift;

    my $line = sprintf("<tr%s><th>%s%s</th>"
		       ,$self->bgstyle()
		       ,$self->_edit_link($sheet->id())
		       ,$sheet->wday_text($self)
	);
    for(my $i=1;$i<$sheet->{cols}+1;$i++){
	if($i == $target_col){
	    $line .= $self->_input_cell($sheet,'r',$self->[0],$i);
	}else{
	    my $opt   = $sheet->{COL_OPTS}->[$i];
	    my $style = $sheet->{col_styles}->[$i];
	    $style = " style='$style'" if($style);
	    my $content = $self->[$i];
	    $content = "&nbsp;" if($content =~ /^\s*$/);
	    $line .= sprintf("<td%s%s>%s</td>",$opt,$style,$content);
	}
    }
    $line .= "</tr>\n";
    return $line;
}

sub _input_cell{
    my $self  = shift;
    my $sheet = shift;
    my $type  = shift;
    my $r     = shift;
    my $c     = shift;

    my $editopt = $sheet->{col_editopt}->[$c];
    $editopt = "size='$editopt'" if($editopt =~ /^\d+$/);
    $editopt = ' '.$editopt if($editopt);

    my $style = $sheet->{col_styles}->[$c];
    $style = " style='$style'" if($style);

    my $name;
    if($type eq 'r'){
	$name = 'r'.$r;
    }else{
	$name = 'c'.$c;
    }
    $name = sprintf('r%dc%d',$r,$c);

    if($sheet->{value_range}){
	my $currval = $self->[$c];
	my $ret = sprintf("   <td%s><select name='%s'>"
		       ,$style,$name);
	foreach my $v ('',@{$sheet->{value_range}}){
	    $ret .= sprintf('<option%s>%s</option>'
			    ,($v eq $currval)?' selected':''
			    ,$v);
	}
	$ret .= "</select></td>";
	return $ret;
    }else{
	my $content = $self->[$c];
	$content =~ s{//}{\n}g;
	return sprintf("   <td%s><textarea cols='30' rows='3' style='width:98%;' name='%s' %s>%s</textarea></td>"
		       ,$style,$name,$editopt,$content);
    }
}

sub _edit_link{
    my $self = shift;
    my $sheet_id = shift;
    my $path = "%cgiURL%/%pathinfo%";
    return sprintf('<a href="%s?plugin::Tool::ScheduleSheet::edit=%s&amp;row=%d"%s>%d</a>'
		   ,$path,$sheet_id,$self->[0],$style_tool_link,$self->[0]);
}

1;
