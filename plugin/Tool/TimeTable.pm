package plugin::Tool::TimeTable;
#
# $Id: TimeTable.pm,v 1.5 2004/10/06 10:08:43 white Exp $
#
my $root;
my $mode_edit;
my $target_tool_count;
my $target_entry_count;

my $TIMEUNIT = 10; # in minute
my $UNIT_IN_HOUR = 60 / $TIMEUNIT;

sub install{
    my $class = shift;
    my $PlugIns = shift;
    $root = shift;
    $PlugIns->add('postprocess',\&postprocess);
}

sub postprocess{
    my $source = shift;
    my $page_name = shift;

    return $source unless _is_target($source);
    return edit_postprocess($source,$page_name) if($mode_edit);
    
    my $result;
    my $count = 0;
    my $rest = $source;
    while((my($pre,$define,$post) = _parse($rest))){
	$count++;
	$result .= $pre;
	$rest    = $post;
	
	$timetable = new plugin::Tool::TimeTable::TimeTable($define);

	$result .= "<table border=1>";
	$result .= $timetable->caption;
	$result .= $timetable->timeline;
	my $entry_count = 0;
	foreach $entry ($timetable->entries){
	    $entry_count++;
	    $result .= $entry->showline($timetable,$count,$entry_count);
	}
	$result .= "</table>";
    }
    $result .= $rest;
    return $result;
}

sub edit{
    my $source    = shift;
    my $page_name = shift;
    my $cgi       = shift;

    return $source unless _is_target($source);
    
    $target_tool_count  = $cgi->{plugin::Tool::ScheduleAdjuster::edit};
    $target_entry_count = $cgi->{entry};
    
    my $result;
    my $count = 0;
    my $rest = $source;
    while((my($pre,$timetable,$post) = _parse($rest))){
	$count++;
	$rest = $post;
	if($count == $target_tool_count){
	    $result = $timetable;
	    last;
	}
    }
    return $source unless($result);
    $mode_edit = 1;
    return $result;
}

sub edit_postprocess{
    my $source = shift;
    my $page_name = shift;

    $timetable = new plugin::Tool::TimeTable::TimeTable($source);
    
    my $caption = $timetable->{caption};
    $result .= "<table border=1>";
    $result .= "<caption>$caption</caption>";
    
    my($line_day,$line_wday) = $timetable->datelines();
    $result .= "$line_day$line_wday";
    
    my $entry_count = 0;
    foreach $entry (@{$timetable->{entry}}){
	$entry_count++;
	if($entry_count == $target_entry_count){
	    $result .= $entry->editline($timetable,$count,$entry_count);
	}else{
	    $result .= $entry->showline($timetable);
	}
    }
    $result .= "</table>";
    return $result;
}


sub commit{
    my $source    = shift;
    my $page_name = shift;
    my $cgi       = shift;
    
    return $source unless _is_target($source);
    
    $target_tool_count  = $cgi->{tool};
    $target_entry_count = $cgi->{entry};
    my $result;
    my $count = 0;
    my $rest = $source;
    while((my($pre,$timetable,$post) = _parse($rest))){
	$count++;
	$result .= $pre;
	$rest    = $post;
	unless($count == $target_tool_count){
	    $result .= $timetable;
	    next;
	}
	$timetable =
	    new plugin::Tool::ScheduleAdjuster::Timetable($timetable);
	$timetable->renew_entry($target_entry_count,$cgi);
	$result .= $timetable->as_source;
	last;
    }
    $result .= $rest;
    if($source ne $result){
	&main::write_page($page_name,$source,$result);
	return '','redirect';
    }else{
	return $source;
    }
}

sub _parse{
    my($source) = shift;
    return unless($source =~ /(^|[^\\])\%timetable\s*(.*?)\%(.*?)\n/);
    my $pre = "$`$1";
    my $timetable = "$&";
    my $post = "$'";
    while($post =~ s/^\%(.*?)\n//so){
	$timetable .= "$&";
    }
    if($post =~ /^%(.*)$/){
	$timetable .= "$&";
    }
    return($pre,$timetable,$post);
}

sub _is_target{
    my($source) = shift;
    if($source =~ /(^|[^\\])\%timetable\s*(.*?)\%/o){
	return 1;
    }else{
	return 0;
    }
}

package plugin::Tool::TimeTable::TimeTable;

sub new{
    my $class = shift;
    my $src   = shift;

    $src =~ /\%timetable\s*(.*?)\%(.*?)\n/;
    my($opt,$caption) = ($1,$2);
    my $rest = "$'";
    my $caption = $caption || "TimeTable";
    my $self = {
	def_src => $&,
	opt   => {},
	caption => $caption,
	entry => [],
    };
    while($opt =~ s/(.*?)=([^\s]*)//){
	my($type,$vals) = ($1,$2);
	$self->{opt}->{$type} ||= {};
	foreach(split(/,/,$vals)){
	    $self->{opt}->{$type}->{$_} = 1;
	}
    }
    $opt = $self->{opt};
    bless $self,$class;
    $self->_set_range();
    while($rest =~ s/^\%(.*?)\n//mo){
	my $entry = new plugin::Tool::TimeTable::Entry($1,$self);
	if(defined $entry){
	    push(@{$self->{entry}},$entry);
	}
    }
    return $self;
}

sub _set_range{
    my $self = shift;
    my($begin,$end);
    if($self->{opt}->{range}){
	my(@arg);
	foreach(keys %{$self->{opt}->{range}}){
	    /(\d+)(:\d+)?/;
	    push(@arg,$1);
	}
	@arg = sort {$a <=> $b} @arg;
	if(scalar(@arg) > 1){
	    $begin = $arg[0];
	    $end   = $arg[-1];
	}else{
	    $begin = $arg[0];
	    $end = 24;
	}
    }else{
	$begin = 0;
	$end   = 24;
    }
    $self->{begin} = $begin;
    $self->{end}   = $end;
}

sub begin{
    my $self = shift;
    return $self->{begin};
}

sub end{
    my $self = shift;
    return $self->{end};
}

sub caption{
    my $self = shift;
    my $caption = $self->{caption};
    return "<caption>$caption</caption>";
}

sub entries{
    my $self = shift;
    return sort {$a->cmp_value <=> $b->cmp_value} @{$self->{entry}};
}

sub timeline{
    my $self = shift;
    my $line1 = "<tr><td></td>";
    my $line2 = "<tr><td></td>";
    for(my $h = $self->{begin};$h < $self->{end}; $h++){
	$line1 .="<td colspan=$UNIT_IN_HOUR>$h</td>";
	$line2 .="<td></td>" x $UNIT_IN_HOUR;
    }
    $line1 .= "</tr>\n";
    $line2 .= "</tr>\n";
    return $line1.$line2;
}

sub selecter{
    my $self = shift;
    my $name = shift;
    my $value = shift;
    my(@input);
    push(@input,"<option value='' %s></option>");
    foreach(sort keys %{$self->{opt}->{types}}){
	push(@input,sprintf("<option value='%s' %s>%s</option>"
			    ,$_,($value eq $_)?"selected":"",$_));
    }
    return "<select name='$name'>".join("",@input)."</select>";
}

sub renew_entry{
    my $self = shift;
    my $target_entry_count = shift;
    my $cgi = shift;

    my $i = $target_entry_count - 1;
    my $entry = $self->{entry}->[$i];

    foreach(@{$self->{days}}){
	$d = $_->{day};
	if(defined $cgi->{$d}){
	    my $type = $cgi->{$d};
	    $entry->{def}->{$d} = $type;
	    $entry->{types}->{$type} = 1;
	}
    }
    return;
}

sub as_source{
    my $self = shift;
    my $result = $self->{def_src};
    foreach $entry (@{$self->{entry}}){
	$result .= $entry->as_source;
    }
    return $result;
}

package plugin::Tool::TimeTable::Entry;

sub new{
    my $class  = shift;
    my $src    = shift;
    my $parent = shift;

    if($src =~ /(\d{1,2}):(\d{1,2})\s*\-\s*(\d{1,2}):(\d{1,2})\s+/){
	my($begin_h,$begin_m,$end_h,$end_m,$name) = ($1,$2,$3,$4,"$'");
	return bless{
	    name   => $name,
	    begin  => [$begin_h,$begin_m],
	    end    => [$end_h,$end_m],
	    parent => $parent
	    },$class;
    }else{
	return;
    }
}

sub as_source{
    my $self = shift;
    return sprintf("%%%02d:%02d-%02d:%02d %s"
		   ,@{$self->begin},@{$self->end},$name);
}

sub cmp_value{
    my $self = shift;
    return $self->begin;
}

sub begin{
    my $self = shift;
    return ($self->{begin}->[0] * $UNIT_IN_HOUR
	    + int($self->{begin}->[1] / $TIMEUNIT));
}

sub end{
    my $self = shift;
    return ($self->{end}->[0] * $UNIT_IN_HOUR
	    + int($self->{end}->[1] / $TIMEUNIT));
}

sub parent{
    return $_[0]->{parent};
}

sub showline{	    
    my $self        = shift;
    my $timetable   = shift;
    my $count       = shift;
    my $entry_count = shift;
    
    my $name = $self->{name};
    my $begin = $self->begin - $self->parent->begin * $UNIT_IN_HOUR;
    my $end   = $self->end   - $self->parent->begin * $UNIT_IN_HOUR;
    my $END   = ($self->parent->end - $self->parent->begin) * $UNIT_IN_HOUR;

    my $line = "<tr><th nowrap>$name</th>";
    if($begin > 0){
	$line .= "<td colspan=$begin></td>";
    }
    if($end > $self->parent->end * $UNIT_IN_HOUR){
	$end = $self->parent->end * $UNIT_IN_HOUR;
    }
    my $length = $end - $begin;
    $line .= "<td colspan=$length style='background-color:red'></td>";
    if(($length = $END - $end) > 0){
	$line .= "<td colspan=$length></td>";
    }
    $line .= "</tr>";
    return $line;
}

sub editline{
    my $self        = shift;
    my $timetable   = shift;
    my $count       = shift;
    my $entry_count = shift;
    
    my $name = $self->{name};
    my $def  = $self->{def};
    my $line = "<tr><form><th>$name<input type=submit name='plugin::Tool::ScheduleAdjuster::commit' value='ȿ��' /><input type=hidden name='tool' value='$target_tool_count' /><input type=hidden name='entry' value='$target_entry_count' /></th>";
    
    foreach(@{$timetable->{days}}){
	my $d = $_->{day};
	$selecter = $timetable->selecter($d,$def->{$d});
	$line .= "<td>$selecter</td>";
    }
    $line .= "</form></tr>";
    return $line;
}

sub _edit_link{
    my $self = shift;
    my($count,$entry_count) = @_;
    return unless($count || $entry_count);
    my $path = "%cgiURL%/%pathinfo%";
    return "<a href='$path?plugin::Tool::ScheduleAdjuster::edit=$count&amp;entry=$entry_count'><�Խ�></a>";
}

1;

