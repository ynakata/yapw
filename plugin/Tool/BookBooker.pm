package plugin::Tool::BookBooker;
#
# $Id: BookBooker.pm,v 1.2 2004/05/26 04:29:11 white Exp $
#
my $root;

sub install{
    my $class = shift;
    my $PlugIns = shift;
    $root = shift;
    $PlugIns->add('postprocess',\&postprocess);
}

sub postprocess{
    my $source = shift;
    my $page_name = shift;

    return $source unless _is_target($source);
    my $result;
    my $count = 0;
    my $rest = $source;
    while((my($pre,$body,$post) = _parse($rest))){
	$count++;
	$result .= $pre;
	$rest    = $post;

	$booker = new plugin::Tool::BookBooker::Booker($body);

	$result .= $booker->to_html();
    }
    $result .= $rest;
}

sub _parse{
    my($source) = shift;
    return unless($source =~ /(^|[^\\])\%book_booker%(.*?)\n/);
    my $pre = "$`$1";
    my $body = "$&";
    my $post = "$'";
    while($post =~ s/^\%(.*?)\n//so){
	$body .= "$&";
    }
    if($post =~ /^%(.*)$/){
	$body .= "$&";
    }
    return($pre,$body,$post);
}

sub _is_target{
    my($source) = shift;
    if($source =~
       /(^|[^\\])\%book_booker\%/o){
	return 1;
    }else{
	return 0;
    }
}

package plugin::Tool::BookBooker::Booker;

sub new{
    my $class = shift;
    my $src   = shift;

    $src =~ /\%book_booker\%(.*?)\n/so;
    $src = "$'";
    my $caption = $1 || "BookBooker";
    my $self = {
	caption => $caption,
	state   => {},
	field   => [],
	entry   => []
    };
    while($src =~ s/^\%%(.*?)\n//so){
	my($src) = ($1);
	$src =~ /^(.*?)=(.*)$/;
	my($key,$value) = ($1,$2);
	if($key =~ /user|state/){
	    $self->{opt}->{$key} ||= {};
	    foreach(split(/,/,$value)){
		$self->{opt}->{$key}->{$_} = 1;
	    }
	}elsif($key =~ /field/){
	    foreach(split(/\%/,$value)){
		push(@{$self->{field}},$_);
	    }
	}
    }
    $self->{opt}->{state} ||= { '��' => 1 , '��' => '1' ,'��' => 1 };
    while($src =~ s/^\%(.*?)\n//so){
	my $entry = new plugin::Tool::BookBooker::Entry($1,$self);
	push(@{$self->{entry}},$entry);
    }
    return bless $self,$class;
}

sub fields{
    my $self = shift;
    return @{$self->{field}};
}

sub to_html{
    my $self = shift;
    my $result = "<div>";
    $result .= $self->_sort_tool;

    $result .= "<table border=1>";
    $result .= "<tr>";
    $result .= " <td></td>";
    foreach(keys %{$self->{opt}->{user}}){
	$result .= " <th>$_</th>";
    }
    $result .= "<tr>";

    $result .= $self->_to_html_entry;

    $result .= "</table>";
    $result .= "</div>";
    return $result;
}

sub _sort_tool{
    my $self = shift;
    return;
    my $result;
    $result  = "<p>";
    $result .= join(' | ',@{$self->{field}});
    $result .= "</p>";
    return $result;
}

sub _to_html_entry{
    my $self   = shift;
    my $result;
    foreach my $entry (@{$self->{entry}}){
	$result .= $entry->_tableline();
    }
    return $result;
}

package plugin::Tool::BookBooker::Entry;

sub new{
    my $class  = shift;
    my $src    = shift;
    my $parent = shift;
    my $self = {
	parent => $parent,
	value  => []
    };
    foreach(split(/\%/,$src)){
	push(@{$self->{value}},$_);
    }
    return bless $self,$class;
}

sub parent{
    my $self = shift;
    return $self->{parent};
}

sub _tableline{
    my $self = shift;
    my $mode = shift;
    my $parent = $self->parent;
    return if (scalar(@{$self->{value}}) <= scalar($parent->fields));

    my $result;
    $result .= "<tr>";
    $result .= $self->_th();

    $def = $self->{value}->[scalar($parent->fields)];
    print STDERR "$def\n";
    my %types;
    while($def =~ s/(.*?)=(\S*)//){
	my($type,$names) = ($1,$2);
	print STDERR "$type $names\n";
	foreach my $name (split(/,/,$names)){
	    $types{$name} = $type;
	    print STDERR "$name : $type\n";
	}
    }
    foreach my $name (keys %{$parent->{opt}->{user}}){
	if($types{$name}){
	    $result .= " <td>$types{$name}</td>";
	}else{
	    $result .= " <td>&nbsp;</td>";
	}
    }
    $result .= "</tr>";
}

sub _th{
    my $self = shift;
    return "<th>".$self->{value}->[3]."[".$self->{value}->[4]."]"."</th>";
}

1;

