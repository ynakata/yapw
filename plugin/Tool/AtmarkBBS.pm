package plugin::Tool::AtmarkBBS;
use strict;
#
# $Id: AtmarkBBS.pm,v 1.13 2007/03/11 17:34:23 white Exp $
#
my $root;
my $PlugIns;
sub install{
    my $class = shift;
    $PlugIns = shift;
    $root = shift;
    $PlugIns->add('linkconvert',\&linkconvert);
    $PlugIns->add('parse_regexp',\&parse_regexp);
    $PlugIns->add('ignore_staticsign',\&ignore_staticsign);
}

sub parse_regexp{
    return '\[\@(?:\[.*?\]|[^\[\]]*?)+\]';
}

sub ignore_staticsign{
    return '\[\@(?:\[.*?\]|[^\[\]]*?)+\]';
}

sub linkconvert{
    my($text,$page_name) = @_;

    unless($text =~ /^\@(?:\[.*?\]|[^\[\]]*?)+/){
	return $text,0;
    }

    $text   =~ /^\@*(.*?)(?::(.*))?$/s;
    my $label  = $1;
    my $format = $2 || '[]';

    my(@format) = split(/(\(.*?\)|<.*?>|\[.*?\]|\\n|\n)/,$format);
    my $path = "%cgiURL%/%pathinfo%";
    my $form .= "<form action='$path' method='get'>";
    my $count = 0;

    my($option,$content);
    my $no_antespam = 1;
    foreach(@format){
	if(/^\n$/){
	    $form .= "<br />\n";
	}elsif(/^\\n/){
	}elsif(/^\s*$/){
	}elsif(/\[\%date\%\]/){
	}elsif(/^\[(.*?)(?:[\/](.*?))?\]/){
	    $count++;
	    my $name = "at$count";
	    $option  = $1 || "size='50'";
	    $content = $2 || "";
	    if($option =~ /(cols|rows)/i){
		if($PlugIns->is_installed('Advance::SimpleAnteSpam')){
		    $content = '%_simpleAnteSpam_%'.$content;
		    $form .= "<input type='hidden' name='as' value='$count'>";
		    $name .= "as";
		    $no_antespam = 0;
		}
		$form .= "<textarea name='$name' $option>$content</textarea>";
	    }else{
		$form .= "<input type='text' name='$name' $option value='$content' />";
	    }
	    next;
	}elsif(/^<.*?>$/){
	    $form .= $_;
	}elsif(/^\((.*?)\)$/){
	    $form .= $1;
	}else{
	    # anything else
	    $form .= $_;
	}
    }
    if($count == 0){
	$count++;
	$option = "size='50'";
	$form  .= "<input type='text' name='at$count' $option />";
    }
    $form .= "<input type='hidden' name='atwrite' value='$label'>";
    $form .= "<input type='submit' name='plugin::Tool::AtmarkBBS::write' value='���' />";
    if($PlugIns->is_installed('Advance::SimpleAnteSpam') && $no_antespam){
	$form .= "<input type='hidden' name='as' value='skip'>";
    }
    $form .= "</form>";

    return $form,1;
}

sub write{
    my $oldsrc    = shift;
    my $page_name = shift;
    my $cgi       = shift;
    if($ENV{REQUEST_METHOD} ne "GET"){
	return;
    }

    my($label)     = $cgi->{atwrite};
    $cgi->{addsign} = 1;
    $cgi->{adddate} = 1;

    my $newsrc = _newsrc($page_name,$oldsrc,$label,$cgi);
    $newsrc = main::staticAlias($newsrc);
    foreach my $func ($main::PlugIns->list('updatepage')){
	my $action;
	($newsrc,$action) = &$func($newsrc,$page_name,'through');
	if($action eq 'abort'){
	    warn 'abort action';
	    &main::mode_redirect($page_name);
	    exit(0);
	}
    }
    if($oldsrc ne $newsrc){
	&main::write_page($page_name,$oldsrc,$newsrc);
	return '','redirect';
    }
    return;
}

sub _newsrc{
    my $target = shift;
    my $source = shift;
    my $label  = shift;
    my $cgi    = shift;

    if($PlugIns->is_installed('Advance::SimpleAnteSpam')){
	abort($target) unless $cgi->{as};
	unless($cgi->{as} eq 'skip'){
	    my $count = $cgi->{as};
	    my $name  = 'at'.$count;
            my($new,$action) = plugin::Advance::SimpleAnteSpam::updatepage($cgi->{$name.'as'},$target);
	    abort($target) if($action eq 'abort');
	    &main::mode_redirect($target) unless $new;
	    $cgi->{$name} = $new;
	}
    }
    unless($source =~ /(?:^|[^\\])\[(\@+)$label(?::((\[.*?\]|.*?)+))?\]/s){
	&main::mode_redirect($target);
    }
    my $before = $`;
    my $tool   = $&;
    my $after  = $';#$';
    my($mode);
    if(length($1) >= 2){
	$mode = 'next';
    }else{
	$mode = 'previous';
    }
    my $format = $2 || '[]';
    my(@format) = split(/(\(.*?\)|<.*?>|\[.*?\]|\\\\|\n|\\n)/s,$format);
    my $addsource = '';
    my $verify    = '';
    my $count = 0;
    foreach(@format){
	warn "formatting{$_}: $addsource";
	if(/^\s*$/ || /^<.*?>$/){
	    $addsource .= $_;
	}elsif(/^\((.*?)\)$/){
	}elsif(/\[\%date\%\]/){
	    my($date_str) = main::getDate();
	    $addsource .= "($date_str)";
	    $verify    .= "($date_str)";
	}elsif(/^\[.*?\]/){
	    $count++;
	    $addsource .= $cgi->{"at$count"};
	    $verify    .= $cgi->{"at$count"};
	    next;
	}elsif(/^\\\\$/){
	    $addsource .= "\\";
	}elsif(/^\\n$/){
	    $addsource .= "\n";
	}else{
	    $addsource .= $_;
	}
    }
    if($count == 0){
	$count++;
	$addsource .= $cgi->{"at$count"};
	$verify    .= $cgi->{"at$count"};
    }
    if($verify =~ /^\s*$/){
	&main::mode_redirect($target);
    }
    if($mode eq 'previous'){
	$source = $before."\n".$addsource.$tool.$after;
    }else{
        $source = $before.$tool."\n".$addsource.$after;
    }
    warn $addsource;
    return $source;
}

sub abort{
    my $target  = shift;
    warn 'abort action at ',join(' line ',(caller())[1,2]),"\n";
    &main::mode_redirect($target);
    exit(0);
}
1;
