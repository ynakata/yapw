package plugin::Tool::ScheduleAdjuster;
#
# $Id: ScheduleAdjuster.pm,v 1.11 2004/10/06 10:08:43 white Exp $
#
my $root;
my $mode_edit;
my $target_tool_count;
my $target_entry_count;

sub install{
    my $class = shift;
    my $PlugIns = shift;
    $root = shift;
    $PlugIns->add('postprocess',\&postprocess);
}

my @wday_en = qw( sun mon tue wed thu fri sat );
my @wday_jp = qw( �� �� �� �� �� �� �� );
my @days_of_month = (0,31,28,31,30,31,30,31,31,30,31,30,31);

sub postprocess{
    my $source = shift;
    my $page_name = shift;

    return $source unless _is_target($source);
    return edit_postprocess($source,$page_name) if($mode_edit);
    
    my $result;
    my $count = 0;
    my $rest = $source;
    while((my($pre,$scheduler,$post) = _parse($rest))){
	$count++;
	$result .= $pre;
	$rest    = $post;
	
	$scheduler = new plugin::Tool::ScheduleAdjuster::Scheduler($scheduler);

	my $caption = $scheduler->{caption};
	$result .= "<table border=1>";
	$result .= "<caption>$caption</caption>";
	
	my($line_day,$line_wday) = $scheduler->datelines();
	$result .= "$line_day$line_wday";
	
	my $entry_count = 0;
	foreach $entry (@{$scheduler->{entry}}){
	    $entry_count++;
	    my $line = $entry->showline($scheduler,$count,$entry_count);
	    $result .= $line;
	}
	$result .= "</table>";
    }
    $result .= $rest;
    return $result;
}

sub edit{
    my $source    = shift;
    my $page_name = shift;
    my $cgi       = shift;

    return $source unless _is_target($source);
    
    $target_tool_count  = $cgi->{plugin::Tool::ScheduleAdjuster::edit};
    $target_entry_count = $cgi->{entry};
    
    my $result;
    my $count = 0;
    my $rest = $source;
    while((my($pre,$scheduler,$post) = _parse($rest))){
	$count++;
	$rest = $post;
	if($count == $target_tool_count){
	    $result = $scheduler;
	    last;
	}
    }
    return $source unless($result);
    $mode_edit = 1;
    return $result;
}

sub edit_postprocess{
    my $source = shift;
    my $page_name = shift;

    $scheduler = new plugin::Tool::ScheduleAdjuster::Scheduler($source);
    
    my $caption = $scheduler->{caption};
    $result .= "<table border=1>";
    $result .= "<caption>$caption</caption>";
    
    my($line_day,$line_wday) = $scheduler->datelines();
    $result .= "$line_day$line_wday";
    
    my $entry_count = 0;
    foreach $entry (@{$scheduler->{entry}}){
	$entry_count++;
	if($entry_count == $target_entry_count){
	    $result .= $entry->editline($scheduler,$count,$entry_count);
	}else{
	    $result .= $entry->showline($scheduler);
	}
    }
    $result .= "</table>";
    return $result;
}


sub commit{
    my $source    = shift;
    my $page_name = shift;
    my $cgi       = shift;
    
    return $source unless _is_target($source);
    
    $target_tool_count  = $cgi->{tool};
    $target_entry_count = $cgi->{entry};
    my $result;
    my $count = 0;
    my $rest = $source;
    while((my($pre,$scheduler,$post) = _parse($rest))){
	$count++;
	$result .= $pre;
	$rest    = $post;
	unless($count == $target_tool_count){
	    $result .= $scheduler;
	    next;
	}
	$scheduler =
	    new plugin::Tool::ScheduleAdjuster::Scheduler($scheduler);
	$scheduler->renew_entry($target_entry_count,$cgi);
	$result .= $scheduler->as_source;
	last;
    }
    $result .= $rest;
    if($source ne $result){
	&main::write_page($page_name,$source,$result);
	return '','redirect';
    }
    return $source;
}

sub _parse{
    my($source) = shift;
    return unless($source =~ /(^|[^\\])\%schedule_adjuster\s+(\d+)\s+(\d+)(?:\s+(.*?))?\%(.*?)\n/);
    my $pre = "$`$1";
    my $scheduler = "$&";
    my $post = "$'";
    while($post =~ s/^\%(.*?)\n//so){
	$scheduler .= "$&";
    }
    if($post =~ s/^%(.*)$//o){
	$scheduler .= "$&";
    }
    return($pre,$scheduler,$post);
}

sub _is_target{
    my($source) = shift;
    if($source =~
       /(^|[^\\])\%schedule_adjuster\s+(\d+)\s+(\d+)(?:\s+(.*?))?\%/o){
	return 1;
    }else{
	return 0;
    }
}

sub _days_of_month_{
    my($year,$month) = @_;
    my $first_wday = (gmtime(_timegm(0,0,0,1,$month,$year,'GMT')))[6];

    if($month != 2){
	return $days_of_month[$month],$first_wday;
    }else{
	if( ($year % 4 == 0 && $year % 100 != 0) || ($year % 400 == 0)){
	    return 29,$first_wday;
	}else{
	    return 28,$first_wday;
	}
    }
}

sub _timegm{
    my($second,$minute,$hour,$day,$month,$year) = @_;
    my($i,$time);
    
#print LOG    "$year-$month-$day-$hour-$minute-$second-$timezone\n";
#print STDERR "$year-$month-$day-$hour-$minute-$second-$timezone\n";
    $year += 1900 if($year < 1900);
    my(@DAYOFMONTH) = (31,28,31,30,31,30,31,31,30,31,30,31);
    if($year % 4 == 0 && ($year % 100 != 0 || $year % 400 == 0)){
        @DAYOFMONTH = (31,29,31,30,31,30,31,31,30,31,30,31);
    }
    # $time means days
    my $Y = $year - 1;
    $time = $Y * 365 + int($Y/4) - int($Y/100) + int($Y/400);
    $time -= 719162;# days since 1970;
    $month--;for($i = 0;$i < $month;$i++){$time += $DAYOFMONTH[$i];}
    $time += $day-1;
    
    # $time means hours
    $time *= 24;$time += $hour;
    # $time means minutes
    $time *= 60;$time += $minute;
    # $time means seconds(UNIX standard time)
    $time *= 60;$time += $second;

    return $time;
}

package plugin::Tool::ScheduleAdjuster::Scheduler;

sub new{
    my $class = shift;
    my $src   = shift;

    $src =~
	/\%schedule_adjuster\s+(\d+)\s+(\d+)(?:\s+(.*?))?\%(.*?)\n/s;
    my($year,$month,$opt,$caption) = ($1,$2,$3,$4);
    my $rest = "$'";
    my $caption = $caption || "schedule of $1/$2";
    my $self = {
	def_src => $&,
	year  => $year,
	month => $month,
	opt   => {},
	caption => $caption,
	entry => [],
	days  => [],
    };
    while($opt =~s/(.*?)=([^\s]*)//){
	my($type,$vals) = ($1,$2);
	$self->{opt}->{$type} ||= {};
	foreach(split(/,/,$vals)){
	    $self->{opt}->{$type}->{$_} = 1;
	}
    }
    $self->{opt}->{types} ||= { '��' => 1 , '��' => '1' ,'��' => 1 };
    while($rest =~ s/^\%(.*?)\n//mo){
	my $entry = new plugin::Tool::ScheduleAdjuster::Entry($1);
	push(@{$self->{entry}},$entry);
	foreach($entry->types){
	    $self->{opt}->{types}->{$_} = 1;
	}
    }
    $opt = $self->{opt};
    my($days,$wday) =
	plugin::Tool::ScheduleAdjuster::_days_of_month_($year,$month);
    for(my $i=1;$i <= $days ; $i++){
	my $wday = ($wday + $i - 1) % 7;
	my $wday_en = $wday_en[$wday];
	my $wday_jp = $wday_jp[$wday];
	if(defined $opt->{only}){
	    next unless($opt->{only}->{$i}
			|| $opt->{only}->{$wday_en}
			|| $opt->{only}->{$wday_jp});
	}
	push(@{$self->{days}},{
	    day     => $i,
	    wday    => $wday_en,
	    wday_jp => $wday_jp
	    });
    }
    return bless $self,$class;
}

sub datelines{
    my $self = shift;
    my $line_day  = "<tr><td></td>";
    my $line_wday = "<tr><td></td>";
    foreach(@{$self->{days}}){
	$line_day .= sprintf("<th class='calender_%s'>%02d</th>"
			     ,$_->{wday},$_->{day});
	$line_wday .= sprintf("<th class='calender_%s'>%s</th>"
			      ,$_->{wday},$_->{wday_jp});
    }
    $line_day .= "</tr>\n";
    $line_wday .= "</tr>\n";
    return $line_day,$line_wday;
}

sub selecter{
    my $self = shift;
    my $name = shift;
    my $value = shift;
    my(@input);
    push(@input,"<option value='' %s></option>");
    foreach(sort keys %{$self->{opt}->{types}}){
	push(@input,sprintf("<option value='%s' %s>%s</option>"
			    ,$_,($value eq $_)?"selected":"",$_));
    }
    my $size = scalar(keys %{$self->{opt}->{types}}) + 1;
    return "<select name='$name' size='$size'>".join("",@input)."</select>";
}

sub renew_entry{
    my $self = shift;
    my $target_entry_count = shift;
    my $cgi = shift;

    my $i = $target_entry_count - 1;
    my $entry = $self->{entry}->[$i];

    foreach(@{$self->{days}}){
	$d = $_->{day};
	if(defined $cgi->{$d}){
	    my $type = $cgi->{$d};
	    $entry->{def}->{$d} = $type;
	    $entry->{types}->{$type} = 1;
	}
    }
    return;
}

sub as_source{
    my $self = shift;
    my $result = $self->{def_src};
    foreach $entry (@{$self->{entry}}){
	$result .= $entry->as_source;
    }
    return $result;
}

package plugin::Tool::ScheduleAdjuster::Entry;

sub new{
    my $class = shift;
    my $str   = shift;

    my($name,$src) = split(/\s/,$str,2);
    my $def = {};
    my $types = {};
    while($src =~s/(.*?)=(\d{1,2}(?:,\d{1,2})*)\s*//){
	my($type,$days) = ($1,$2);
	$types->{$type} = 1;
	foreach(split(/,/,$days)){
	    $def->{$_} = $type;
	}
    }
    return bless{
	name  => $name,
	def   => $def,
	types => $types,
	},$class;
}

sub types{
    my $self = shift;
    return keys %{$self->{types}};
}

sub as_source{
    my $self = shift;
    my $result .= sprintf("%%%s ",$self->{name});
    my(%list);
    while(my($d,$t) = each(%{$self->{def}})){
	$list{$t} ||= [];
	push(@{$list{$t}},$d);
    }
    my(@result);
    while(my($t,$array) = each(%list)){
	if(length($t)){
	    push(@result,"$t=".join(',',sort @$array));
	}
    }
    $result .= join(' ',@result)."\n";
    return $result;
}

sub showline{	    
    my $self        = shift;
    my $scheduler   = shift;
    my $count       = shift;
    my $entry_count = shift;
    
    my $name = $self->{name};
    my $def  = $self->{def};
    my $edit = $self->_edit_link($count,$entry_count);
    my $line = "<tr><th>$name $edit</th>";
    
    foreach(@{$scheduler->{days}}){
	my $d = $_->{day};
	my $wday = $_->{wday};
	if($def->{$d}){
	    $line .= "<td class=calender_$wday>$def->{$d}</td>";
	}else{
	    $line .= "<td class=calender_$wday>&nbsp;</td>";
	}
    }
    $line .= "</tr>";
    return $line;
}

sub editline{
    my $self        = shift;
    my $scheduler   = shift;
    my $count       = shift;
    my $entry_count = shift;
    
    my $name = $self->{name};
    my $def  = $self->{def};
    my $line = "<tr><form><th>$name<input type=submit name='plugin::Tool::ScheduleAdjuster::commit' value='ȿ��' /><input type=hidden name='tool' value='$target_tool_count' /><input type=hidden name='entry' value='$target_entry_count' /></th>";
    
    foreach(@{$scheduler->{days}}){
	my $d = $_->{day};
	$selecter = $scheduler->selecter($d,$def->{$d});
	$line .= "<td>$selecter</td>";
    }
    $line .= "<td><input type=submit name='plugin::Tool::ScheduleAdjuster::commit' value='ȿ��' /></td>";
    $line .= "</form></tr>";
    return $line;
}

sub _edit_link{
    my $self = shift;
    my($count,$entry_count) = @_;
    return unless($count || $entry_count);
    my $path = "%cgiURL%/%pathinfo%";
    return "<a href='$path?plugin::Tool::ScheduleAdjuster::edit=$count&amp;entry=$entry_count'><�Խ�></a>";
}

1;
