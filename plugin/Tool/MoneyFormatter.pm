package plugin::Tool::MoneyFormatter;
use strict;
sub install{
    my $class   = shift;
    my $PlugIns = shift;
    $PlugIns->add('preprocess',\&preprocess);
    $PlugIns->add('postprocess',\&postprocess);
}

sub preprocess{
    my $content = shift;
    my $target  = shift;
    $content =~ s{(^|[^\\])\%moneyformatter\%}{$1%moneyformatter%}i;
    return $content;
}

sub postprocess{
    my $content = shift;
    my $target  = shift;

    $content =~ s{(^|[^\\])\%moneyformatter\%}{
	$1<form method=post><p><input type="text" name="bare" size='100' /> <input type="submit" name="plugin::Tool::MoneyFormatter::submit" value="����" /></p></form>
	}g;
    return $content;
}

sub submit{
    my $oldsrc    = shift;
    my $page_name = shift;
    my $cgi       = shift;
    if($ENV{REQUEST_METHOD} ne "POST"){
	return '','redirect';
    }
    my($bare) = $cgi->{bare};

    my($pattern) = $main::PATTERN{TEXT};
    $bare =~ s/($pattern)��/$1 /g;
    my(@token) = split(/ +/,$bare);
    my($price, $day, $time, $memo);
    foreach(@token){
        if(/^\d+$/){
            $price = $_;
        }elsif(/^\d{1,2}:\d\d$/){
            $time = $_;
        }elsif(/^\d{1,2}\/\d{1,2}$/){
            $day = $_;
        }else{
            if($memo){
                $memo .= " ";
            }
            $memo .= $_;
        }
    }
    if($price){
	unless($day){
	    my($m,$d) = (localtime)[4,3];
	    $day = sprintf('%d/%d',$m+1,$d);
	}
	unless($time){
	    my($h,$m,$s) = (localtime)[2,1,0];
            $time = sprintf('%02d:%02d',$h,$m);
        }

	my $newsrc = $oldsrc;
	$newsrc =~ s{(\%moneyformatter\%)}{
	    sprintf("%s\n%5s,%5s,%7d,\"%s\"",$1,$day,$time,$price,$memo);
	}ie;
	&main::write_page($page_name,$oldsrc,$newsrc);
    }
    return '','redirect';
}

1;
