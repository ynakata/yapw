package plugin::Tool::PageGrep;
#
# $Id: PageGrep.pm,v 1.6 2004/03/19 08:14:02 white Exp $
#
my $KEY;
sub install{
    my $class = shift;
    my $PlugIns = shift;
    $PlugIns->add('linkconvert',\&linkconvert);
}

sub linkconvert{
    my $source = shift;
    my $page_name = shift;
    if(/\{PageGrep\}/){
	$page_name = main::URLencode($page_name);
	$source =~ s/\{PageGrep\}/<form>grep:<input type=text name='key' value='$KEY'><input type=submit name='plugin::Tool::PageGrep::grep' value='grep page'><input type=submit name='plugin::Tool::PageGrep::sort_grep' value='sort grep page'><\/form>/g;
	return $source,1;
    }
    return $source;
}

sub sort_grep{
    my($source,$cgi) = @_;
    $KEY = $cgi->{'key'};
    my($prefix,@list) = split_prefix($source);
    unless(length($KEY)){
	return $prefix.join("\n",sort @list);
    }
    return $prefix.join("\n",sort grep {/$KEY/} @list);
}

sub grep{
    my($source,$page_name,$cgi) = @_;
    $KEY = $cgi->{'key'};
    unless(length($KEY)){
	return $source;
    }
    my($prefix,@list) = split_prefix($source);
    return $prefix.join("\n",grep {/$KEY/} @list);
}

sub sort{
    my($source,$cgi) = @_;
    my($prefix,@list) = split_prefix($source);
    return $prefix.join("\n",sort @list);
}

sub split_prefix{
    my $source = shift;
    my @list = split(/(?:\r\n|\r|\n)/,$source);
    my $prefix;
    while(@list){
	my $line = shift(@list);
	$prefix .= "$line\n";
	last if $line =~ /\[\{PageGrep\}\]/;
    }
    return $prefix,@list;
}
1;
