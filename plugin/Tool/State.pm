package plugin::Tool::State;
#
# $Id: State.pm,v 1.6 2009/05/25 07:18:48 white Exp $
#

my $root;
my $PlugIns;
my(@STATES) = qw(��� ��α ���� ��λ);
my $ENABLE_DISPSTATE = 1;

sub install{
    my $class = shift;
    $PlugIns = shift;
    $root = shift;
    $PlugIns->add('postprocess',\&postprocess);
    $PlugIns->add('make_index',\&make_index);
    $PlugIns->add('mklink',\&mklink);
    $PlugIns->add('SpecialPage',\&SpecialPage);
    $PlugIns->add('pagenamelist',\&PageNameList);
    $PlugIns->add('NavigationToIndexPages',\&NavigationToIndexPages);
}

sub postprocess{
    my $source = shift;
    my $page_name = shift;
    
    unless($source =~ /(^|[^\\])\%statetool\%/){
	return $source;
    }
    
    my $state = $root->__($page_name)->STATE();
    if(ref($state)){
	return $source;
    }
    my $form;

    my $path = "%cgiURL%/%pathinfo%";
    $form .= "<form action='$path' method='get'>\n";
    foreach(@STATES){
	my $checked = "";
	if($state =~ /$_/){
	    $checked ="checked";
	}
	$form .= "<input name='chstate' type=radio value='$_' $checked />$_\n";
    }
    $form .= "<input type='submit' name='plugin::Tool::State::chstate' value='�ѹ�' /></form>\n";

    $source =~ s/(^|[^\\])\%statetool\%/$1$form/g;
    return $source;
}

sub chstate{
    my($source,$page_name,$cgi) = @_;
    my($state) = $cgi->{chstate};
    $root->__($page_name)->STATE($state);
    return '','redirect';
}

sub make_index{
    my(@Index) = @_;
    foreach(@Index){
	my $name = $_->{name};
	my $state = $root->__($name)->STATE();
	$state = undef if ref($state);
	$_->{"state"} => $state;
    }
    my $i = 1;
    my %s = map {$_,$i++} @STATES;
    @Index = sort { $s{$a->{"state"}} <=> $s{$b->{"state"}} } @Index;
    foreach (@Index){
	$_->{misc} .= sprintf("\\[%s]",$_->{state}) if($_->{state});
    }
    return 'state',@Index;
}

sub show_index{
    my $pagename = shift;
    return ($pagename eq 'IndexPageByState')?"state":undef;
}

sub mklink{
    my($path,$face,$misc,$name,$label) = @_;
    if($ENABLE_DISPSTATE){
	$state = $root->__($name)->STATE();
	$state = undef if ref($state);
	$misc .= sprintf("\\[%s]",$state) if($state);
    }
    return ($path,$face,$misc);
}

sub SpecialPage{
    my $pagename = shift;
    my $order;
    unless($pagename eq 'IndexPageByState'){
	return 0;
    }

    my(@Index) = $root->__();
    @Index =
	map{ my $time =  $root->__($_)->__timestamp("SOURCE");
	     my $name = $_;
	     my $state = $root->__($_)->STATE();
	     $state = undef if ref($state);
	     $name =~ s/\%/\\\%/g;
	     { time     => $time,
	       moddate  => time_str($time),
	       URI      => "$main::SELFNAME/".main::URLencode($_),
	       name     => $name,
	       state    => $state
	       }
	 } @Index;
    @Index = sort { $b->{"time"} <=> $a->{"time"} } @Index;
    my $i = 1;
    my %s = map {$_,$i++} @STATES;
    @Index = sort { 
	($s{$a->{"state"}} && !$s{$b->{"state"}})?-1:
	($s{$b->{"state"}} && !$s{$a->{"state"}})?1:
	$s{$a->{"state"}} <=> $s{$b->{"state"}} 
    } @Index;
    foreach (@Index){
	$_->{misc} .= sprintf("\\[%s]",$_->{state}) if($_->{state});
    }
    my $content;
    foreach (@Index){
	$content .= 
	    sprintf(" *\\[%s]<a href='%s?time=%s'>%s</a>%s\n",
		    @$_{qw(moddate URI time name misc)}  );
    }
    return 1,main::makeView($pagename,$content);
}

sub NavigationToIndexPages{
    return q{<a href="%yapw%/IndexPageByState" class='l2'>���ֽ�</a>};
}

sub PageNameList{
    return qw(IndexPageByState);
}

sub time_str{
    my($time) = @_;
    my(%time);
    @time{qw(sec min hour mday mon
		year wday yday isdist)} = localtime($time);
    $time{year} += 1900;
    $time{mon}++;
    return sprintf("%04d/%02d/%02d %02d:%02d",
		   @time{qw(year mon mday hour min)});
}
1;
