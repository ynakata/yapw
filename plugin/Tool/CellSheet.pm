package plugin::Tool::CellSheet;
#
# $Id: CellSheet.pm,v 1.30 2013/01/04 16:22:59 white Exp $
#
use strict;
my $root;
my $PlugIns;
my $mode_edit;
my $target_sheet_count;
my $target_row_count;
my $target_col_count;
my $sheet;
my $func_off = 0;

sub install{
    my $class = shift;
    $PlugIns = shift;
    $root = shift;
    my $cgi = $PlugIns->cgi();
    if($ENV{HTTP_USER_AGENT} =~ /bot/){
	$func_off = 1;	
    }
    $PlugIns->add('postprocess',\&postprocess);
}

sub postprocess{
    my $source = shift;
    my $page_name = shift;

    return $source if $func_off;
    return $source unless _is_target($source);
    if(ref($sheet)){
	return _postprocess_edit_row($source,$page_name) if($mode_edit eq 'row');
	return _postprocess_edit_col($source,$page_name) if($mode_edit eq 'col');
    }
    my $result;
    my $sheet_count = 0;
    my $rest = $source;
    while((my($pre,$body,$post) = _parse($rest))){
	$sheet_count++;
	$result .= $pre;
	$rest    = $post;
	
	$sheet = new plugin::Tool::CellSheet::Sheet($body,$sheet_count);

	my $caption = $sheet->{caption};
	$result .= "<table border=1>";
	$result .= "<caption>$caption</caption>";
	
	my($line_title) = $sheet->titleline();
	$result .= "$line_title";
	
	my $row_count = 0;
	foreach my $row (@{$sheet->{row}}){
	    $result .= $row->showline($sheet,$row_count);
	    $row_count++;
	}
	$result .= $sheet->tool_newline();
	$result .= "</table>";
    }
    $result .= $rest;
    return $result;
}

sub edit{
    my $source    = shift;
    my $page_name = shift;
    my $cgi       = shift;

    return $source unless _is_target($source);
    
    $target_sheet_count  = $cgi->{"plugin::Tool::CellSheet::edit"};
    if(defined $cgi->{row}){
	$mode_edit = 'row';
	$target_row_count = $cgi->{row};
    }elsif(defined $cgi->{col}){
	$mode_edit = 'col';
	$target_col_count = $cgi->{col};
    }elsif(defined $cgi->{all}){
	$mode_edit = 'row';
	$target_row_count = -1;
    }

    my $result;
    my $sheet_count = 0;
    my $rest = $source;
    while((my($pre,$body,$post) = _parse($rest))){
	$sheet_count++;
	$rest = $post;
	if($sheet_count == $target_sheet_count){
	    $result = $body;
	    last;
	}
    }
    return $source unless($result);
    $sheet = new plugin::Tool::CellSheet::Sheet($result."\n",$sheet_count);
    unless(ref($sheet)){
	warn "sheet_count=$sheet_count is not valid.";
	exit;
    }
    $target_row_count = $sheet->add_row() if($target_row_count eq 'new');
    return $result;
}

sub _postprocess_edit_row{
    my $source = shift;
    my $page_name = shift;

    my $caption = $sheet->{caption};
    my $result  = "<form><table border=1>";
    $result .= "<caption>$caption</caption>";
    
    my($line_title) = $sheet->titleline();
    $result .= "$line_title";
    
    my $row_count = 0;
    foreach my $row (@{$sheet->{row}}){
	if($row_count == $target_row_count || $target_row_count == -1){
	    $result .= $row->editline($sheet,$row_count);
	}else{
	    $result .=
		main::WikiLink($row->showline($sheet,$row_count),$page_name,'noescape');
	}
	$row_count++;
    }
    $result .= "</table></form>";
    return $result;
}

sub _postprocess_edit_col{
    my $source = shift;
    my $page_name = shift;

    my $caption = $sheet->{caption};
    my $result  = "<form><table border=1>";
    $result .= "<caption>$caption</caption>";
    
    my($line_title) = $sheet->titleline();
    $result .= "$line_title";
    my $row_count = 0;
    foreach my $row (@{$sheet->{row}}){
	$result .= $row->line_edit_col($sheet,$row_count,$target_col_count);
	$row_count++;
    }
    my $num_of_cols = $sheet->{cols} + 1;
    $result .= "<tr><td colspan=$num_of_cols><input type=submit name='plugin::Tool::CellSheet::commit' value='反映' /><input type=hidden name='sheet' value='$target_sheet_count' /><input type=hidden name='col' value='$target_col_count' /></td></tr>\n";
    $result .= "</table></form>";
    return $result;
}

sub commit{
    my $source    = shift;
    my $page_name = shift;
    my $cgi       = shift;
    
    return $source unless _is_target($source);
    
    $target_sheet_count  = $cgi->{sheet};
    $target_row_count = $cgi->{row};
    my $result;
    my $sheet_count = 0;
    my $rest = $source;
    while((my($pre,$body,$post) = _parse($rest))){
	$sheet_count++;
	$result .= $pre;
	$rest    = $post;
	unless($sheet_count == $target_sheet_count){
	    $result .= $body;
	    next;
	}
	$sheet =
	    new plugin::Tool::CellSheet::Sheet($body."\n");
	$target_row_count = $sheet->add_row() 
	    if($target_row_count == scalar(@{$sheet->{row}}));
	$sheet->renew($cgi);
	$result .= $sheet->as_source;
	last;
    }
    $result .= $rest;
    if($source ne $result){
	&main::write_page($page_name,$source,$result);
	return '','redirect';
    }else{
	return $source;
    }
}

sub _parse{
    my($source) = shift;
    return unless($source =~ /^\%cell_sheet\s+(\d+)\%(.*?)\n/mo);
    my $pre  = "$`";
    my $body = "$&";
    my $post = "$'";
    while($post =~ s/^\%(.*?)\n//so){
	$body .= "$&";
    }
    if($post =~ s/^%(.*)$//o){
	$body .= "$&";
    }
    return($pre,$body,$post);
}

sub _is_target{
    my($source) = shift;
    if($source =~
       /^\%cell_sheet\s+\d+%/mo){
	return 1;
    }else{
	return 0;
    }
}

package plugin::Tool::CellSheet::Sheet;

sub new{
    my $class = shift;
    my $src   = shift;
    my $num   = shift;

    $src =~ /^\%cell_sheet\s+(\d+)(?:\s+(.*?))?\%(.*?)\n/s;
    my($cols,$opt,$caption) = ($1,$2,$3);
    my $rest = "$'";
    my $caption = $caption || "sheet $num";
    my $self = {
	def_src => $&,
	cols    => $cols,
	opt     => {},
	caption => $caption,
	sheet_count => $num,
	col_opts    => [],
	COL_OPTS    => [],
	col_styles  => [],
	col_editopt => [],
	row => [],
    };
    while($opt =~s/(.*?)=([^\s]*)//){
	my($type,$vals) = ($1,$2);
	$self->{opt}->{$type} ||= {};
	foreach(split(/,/,$vals)){
	    $self->{opt}->{$type}->{$_} = 1;
	}
    }
    if($rest =~ s/^\%(?:\%|\$coltitle=)(.*?)\n//mo){
	# %% or %$coltitle= : 列タイトル
	$self->{col_titles} = new plugin::Tool::CellSheet::Row($1);
    }
    if($rest =~ s/^\%(?:\?|\$td=)(.*?)\n//mo){
	# %? or %$td= : 各列td/thタグへのオプション指定
	my $def = $1;
	$self->{col_opts} = new plugin::Tool::CellSheet::Row($def);
	$self->{COL_OPTS} = new plugin::Tool::CellSheet::Row($def);
	foreach(@{$self->{COL_OPTS}}){
	    if($_ =~ /^\d+\%?$/){
		$_ = "width='$_'";
	    }
	    $_ = " $_";
	    $_ =~ s/</&lt;/g;
	    $_ =~ s/>/&gt;/g;
	}
    }
    if($rest =~ s/^\%(?:\@|\$colstyle=)(.*?)\n//mo){
	# %@ or %$colstyle= : 各列の表示スタイル指定
	$self->{col_styles} = new plugin::Tool::CellSheet::Row($1);
	foreach(@{$self->{col_styles}}){
	    $_ =~ s/</&lt;/g;
	    $_ =~ s/>/&gt;/g;
	    $_ =~ s/\'//g;
	}
    }
    if($rest =~ s/^\%(?:\#|\$editopt=)(.*?)\n//mo){
	# %# : 入力時INPUTタグへのオプション指定
	$self->{col_editopt} = new plugin::Tool::CellSheet::Row($1);
	foreach(@{$self->{col_editopt}}){
	    $_ =~ s/</&lt;/g;
	    $_ =~ s/>/&gt;/g;
	    $_ = $_;
	}
    }
    if($rest =~ s/^\%\$valuerange=(.*?)\n//mo){
	$self->{value_range} = [split(/,/,$1)];
    }
    while($rest =~ s/^\%(.*?)\n//mo){
	# 列データ
	my $row = new plugin::Tool::CellSheet::Row($1);
	push(@{$self->{row}},$row);
    }
    return bless $self,$class;
}

sub titleline{
    my $self = shift;
    my $path = "%cgiURL%/%pathinfo%";
    my $count = $self->{sheet_count};

    my $offset = ($sheet->{value_range})? 1 : 0;
    my $line = '<tr>';
    if($offset){
	if($self->{col_titles}){
	    my $title = $self->{col_titles}->[0];
	    my $opt   = $self->{COL_OPTS}->[0];
	    $line .= sprintf('<th%s>%s</th>',$opt,$title);
	}else{
	    $line .= '<td></td>';
	}
    }else{
	$line .= sprintf('<td><a href="%s?plugin::Tool::CellSheet::edit=%d&all=1">@</a></td>',$path,$count);
    }
    
    if($self->{col_titles}){
	for(my $i=$offset;$i<$self->{cols};$i++){
	    my $title = $self->{col_titles}->[$i];
	    my $opt   = $self->{COL_OPTS}->[$i];
	    $line .= sprintf('<th%s><a href="%s?plugin::Tool::CellSheet::edit=%d&amp;col=%d">@%s</a></th>',$opt,$path,$count,$i,$title);
	}
    }else{
	for(my $i=$offset;$i<$self->{cols};$i++){
	    $line .= sprintf("<th><a href='%s?plugin::Tool::CellSheet::edit=$count&amp;col=%d'>\@%d</a></th>",$path,$i,$i + 1);
	}
    }
    $line .= "</tr>\n";
    return $line;
}

sub tool_newline{
    my $self = shift;
    my $path = "%cgiURL%/%pathinfo%";
    return sprintf("<tr><td colspan=%d style='text-align:center;'><a href='%s?plugin::Tool::CellSheet::edit=%d&amp;row=new'>&lt;行追加&gt;</a></td><tr>\n",$self->{cols} + 1,$path,$self->{sheet_count});
}

sub renew{
    my $self = shift;
    my $cgi = shift;

    foreach my $key (keys %$cgi){
	next unless($key =~ /^r(\d+)c(\d+)$/);
	my($row,$col) = ($1,$2);
	$self->{row}->[$row]->[$col] = $cgi->{$key};
    }
    return;
}

sub add_row{
    my $self = shift;
    my $row = plugin::Tool::CellSheet::Row->new("");
    push(@{$self->{row}},$row);
    return scalar(@{$self->{row}})-1;
}

sub as_source{
    my $self = shift;
    my $result = $self->{def_src};
    if($self->{col_titles}){
	$result .= '%$coltitle='.substr($self->{col_titles}->as_source,1);
    }
    if(ref($self->{col_opts}) =~ /plugin/){
	#ソースで定義されているならblessされているはず。
	$result .= '%$td='.substr($self->{col_opts}->as_source,1);
    }
    if(ref($self->{col_styles}) =~ /plugin/){
	#ソースで定義されているならblessされているはず。
	$result .= '%$colstyle='.substr($self->{col_styles}->as_source,1);
    }
    if(ref($self->{col_editopt}) =~ /plugin/){
	#ソースで定義されているならblessされているはず。
	$result .= '%$editopt='.substr($self->{col_editopt}->as_source,1);
    }
    if(ref($self->{value_range})){
	#ソースで定義されているならリファレンスであるはず。
	$result .= '%$valuerange='.join(',',@{$self->{value_range}})."\n";
    }
    foreach my $row (@{$self->{row}}){
	$result .= $row->as_source;
    }
    return $result;
}

package plugin::Tool::CellSheet::Row;

sub new{
    my $class = shift;
    my $str   = shift;
    my $self  = [];
    my $current;
    foreach(split(/(\[.*?\]|,)/,$str)){
	if(/^,$/){
	    push(@$self,$current);
	    $current = "";
	}else{
	    $current .= $_;
	}
    }
    push(@$self,$current) if($current);
    return bless $self,$class;
}

sub as_source{
    my $self = shift;
    return "%".join(',',@$self)."\n";
}

sub showline{	    
    my $self      = shift;
    my $sheet     = shift;
    my $row_count = shift;
    
    my $offset = ($sheet->{value_range})? 1 : 0;
    my $line = sprintf("<tr><th>%s%s</th>"
		       ,$self->_edit_link($sheet->{sheet_count},$row_count)
		       ,($offset)?($self->[0] || '&nbsp;'):($row_count+1));
    
    for(my $i=$offset;$i<$sheet->{cols};$i++){
	my $opt   = $sheet->{COL_OPTS}->[$i];
	my $style = $sheet->{col_styles}->[$i];
	my $content = $self->[$i];
	$content = "&nbsp;" if($content =~ /^\s*$/);
	$style = " style='$style'" if($style);
	$line .= sprintf("<td%s%s>%s</td>",
			 $opt,$style,$content);
    }
    $line .= "</tr>\n";
    return $line;
}

sub editline{
    my $self      = shift;
    my $sheet     = shift;
    my $row_count   = shift;

    my $sheet_count = $sheet->{sheet_count};
    my $offset = ($sheet->{value_range})? 1 : 0;
    my $line = sprintf('<tr><th>%s<input type=submit name="plugin::Tool::CellSheet::commit" value="反映" /><input type=hidden name="sheet" value="%s" /><input type=hidden name="row" value="%s" /></th>'
		       ,($offset)?($self->[0] || '&nbsp;'):($row_count+1)
		       ,$target_sheet_count
		       ,$target_row_count);

    for(my $i=$offset;$i<$sheet->{cols};$i++){
	$line .= $self->_input_cell($sheet,'c',$row_count,$i);
    }
    $line .= "<td><input type=submit name='plugin::Tool::CellSheet::commit' value='反映' /></td>";
    $line .= "</tr>\n";
    return $line;
}

sub line_edit_col{
    my $self      = shift;
    my $sheet     = shift;
    my $row_count  = shift;
    my $target_col = shift;
    
    my $offset = ($sheet->{value_range})? 1 : 0;
    my $line = sprintf("<tr><th>%s%s</th>"
		       ,$self->_edit_link($sheet->{sheet_count},$row_count)
		       ,($offset)?($self->[0] || '&nbsp;'):($row_count));
    for(my $i=$offset;$i<$sheet->{cols};$i++){
	if($i == $target_col){
	    $line .= $self->_input_cell($sheet,'r',$row_count,$i);
	}else{
	    my $opt   = $sheet->{COL_OPTS}->[$i];
	    my $style = $sheet->{col_styles}->[$i];
	    $style = " style='$style'" if($style);
	    my $content = $self->[$i];
	    $content = "&nbsp;" if($content =~ /^\s*$/);
	    $line .= sprintf("<td%s%s>%s</td>",$opt,$style,$content);
	}
    }
    $line .= "</tr>\n";
    return $line;
}

sub _input_cell{
    my $self  = shift;
    my $sheet = shift;
    my $type  = shift;
    my $r     = shift;
    my $c     = shift;
    
    my $editopt = $sheet->{col_editopt}->[$c];
    $editopt = "size='$editopt'" if($editopt =~ /^\d+$/);
    $editopt = ' '.$editopt if($editopt);
    
    my $style = $sheet->{col_styles}->[$c];
    $style = " style='$style'" if($style);

    my $name;
    if($type eq 'r'){
	$name = 'r'.$r;
    }else{
	$name = 'c'.$c;
    }
    $name = sprintf('r%dc%d',$r,$c);
    
    if($sheet->{value_range}){
	my $currval = $self->[$c];
	my $ret = sprintf("   <td%s><select name='%s'>"
		       ,$style,$name);
	foreach my $v ('',@{$sheet->{value_range}}){
	    $ret .= sprintf('<option%s>%s</option>'
			    ,($v eq $currval)?' selected':''
			    ,$v);
	}
	$ret .= "</select></td>";
	return $ret;
    }else{
	return sprintf("   <td%s><input type=text value='%s' name='%s'%s></td>"
		       ,$style,$self->[$c],$name,$editopt);
    }
}

sub _edit_link{
    my $self = shift;
    my($count,$row_count) = @_;
    return unless($count || $row_count);
    my $path = "%cgiURL%/%pathinfo%";
    return sprintf('<a href="%s?plugin::Tool::CellSheet::edit=%d&amp;row=%d">#</a>'
		   ,$path,$count,$row_count);
}

1;
