package plugin::Edit::PerParagraph;
use strict;
use plugin::ToHTML::Common;
#
# $Id: PerParagraph.pm,v 1.3 2007/06/21 07:21:46 white Exp $
#
my $root;
my $PlugIns;
my $PLUGINNAME = 'plugin::Edit::PerParagraph';
my $target_paragraph;
my $action;
my $target_head;

sub install{
    my $class = shift;
    $PlugIns = shift;
    $root = shift;
    $PlugIns->add('ToHTML::Heading::_heading',\&_heading);
}

sub _heading{
    my $lv       = shift;
    my $title    = shift;
    my $anchor   = shift;
    my $opt_text = shift;
    my $count    = shift;

    $$opt_text .= sprintf(q[ <a href='%%self%%?%s::edit=%s'>edit</a>]
			  ,$PLUGINNAME,$count);
}

sub edit{
    my $source    = shift;
    my $page_name = shift;
    my $cgi       = shift;

    $target_paragraph = $cgi->{$PLUGINNAME.'::edit'};
    return('','redirect') if(! main::existcheck($page_name));
    my(@pars) = split_paragraph($source);

    $action = sprintf('%s/%s?%s::edited',
		      '%cgiURL%',
		      main::URLencode($page_name)
		      ,$PLUGINNAME);
    $target_head = $pars[$target_paragraph]->{'head'};
    main::mode_edit($page_name,
		    ,join("\n",@{$pars[$target_paragraph]->{'body'}})
		    ,\&edit_callback);
    return $source;
}

sub edit_callback{
    my $content   = shift;
    my $page_name = shift;

    my $caller = $PLUGINNAME.'::edited';

    $$content = $target_head.$$content;
    $$content =~ s/<form action=".*?"/<form action="$action=$target_paragraph"/;
    $$content =~ s/<input type="hidden" name="write".*?>/<input type="hidden" name="$caller" value="$target_paragraph" \/>/;

    return;
}

sub edited{
    my $oldsrc    = shift;
    my $page_name = shift;
    my $cgi       = shift;

    $target_paragraph = $cgi->{$PLUGINNAME.'::edited'};
    my $source = $cgi->{text};

    foreach my $func ($PlugIns->list('updatepage')){
	($source,$action) = &$func($source,$page_name);
	if($action eq 'abort'){
	    print main::redirect($page_name);
	    exit(0);
	}
    }
    my(@pars) = split_paragraph($oldsrc);
    $pars[$target_paragraph]->{'body'} = [$source];

    my $newsrc;
    foreach my $par (@pars){
	$newsrc .= $par->{'head'}."\n" if $par->{'head'};
	$newsrc .= join("\n",@{$par->{'body'}})."\n";
    }
    main::mode_write($page_name,$newsrc,'through');
}

sub split_paragraph{
    my $source = shift;
    my(@pars,$head,@body);
    foreach (split(/\n/,$source)){
	if(/^(\@+)(.*)$/){
	    push(@pars,{'head' => $head, 'body' => [@body]});
	    $head = $_;
	    undef @body;
	}else{
	    push(@body,$_);
	}
    }
    push(@pars,{'head' => $head, 'body' => \@body});

    return @pars;
}

1;
