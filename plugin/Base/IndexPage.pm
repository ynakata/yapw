package plugin::Base::IndexPage;
#
# $Id: IndexPage.pm,v 1.14 2009/05/25 07:16:28 white Exp $
#
my $root;
my $Plugins;

sub install{
    my $class = shift;
    $PlugIns = shift;
    $root = shift;

    $PlugIns->add('SpecialPage',\&SpecialPage);
    $PlugIns->add('SpecialPageTimestamp',\&SpecialPageTimestamp);
    $PlugIns->add('PostDelete',\&PostDelete);
    $PlugIns->add('pagenamelist',\&PageNameList);
    $PlugIns->add('NavigationToIndexPages',\&NavigationToIndexPages);
    $PlugIns->add('post_writepage',\&PostWritePage);
}

sub SpecialPage{
    my $pagename = shift;
    my $order;
    if($pagename eq 'IndexPage'){
	$order = 'name';
    }elsif($pagename eq 'IndexPageByTime'){
	$order = 'time';
    }elsif($pagename eq 'IndexPageByName'){
	$order = 'name';
    }else{
	foreach my $func ($PlugIns->list('show_index')){
	    $order = &$func($pagename);
	    last if $order;
	}
    }
    unless($order){
	return undef;
    }

    if(ref($root->__('__')->__('IndexPages')->$order())){
	make_index();
    }
    # read cache and return.
    my $content = $root->__('__')->__('IndexPages')->$order();
    return 1,main::makeView($pagename,$content,'through');
}

sub SpecialPageTimestamp{
    my $pagename = shift;
    if($pagename eq 'IndexPage'
       || $pagename eq 'IndexPageByTime'
       || $pagename eq 'IndexPageByName'){
	return $root->__('__')->__('IndexPages')->__timestamp('name');
    }
}

sub PageNameList{
    return qw(IndexPage IndexPageByTime IndexPageByName);
}

sub NavigationToIndexPages{
    return q{<a href="%yapw%/IndexPage">����</a>
<a href="%yapw%/IndexPageByTime" class='l2'>������</a>};
}

sub PostWritePage{
    my $page_name = shift;
    make_index();
    return $page_name;
}

sub PostDelete{
    make_index();
}

sub make_index{
    my(@Index) = $root->__();
    @Index = grep { $_ ne '__' } @Index;
    @Index =
	map{ my $time =  $root->__($_)->__timestamp("SOURCE");
	     my $name = $_;
	     $state = undef if ref($state);
	     $name =~ s/\%/\\\%/g;
	     {
		 time     => $time,
		 moddate  => time_str($time),
#		 URI      => "$main::SELFNAME/".main::URLencode($_),
		 URI      => main::URLencode($_),
		 name     => $name,
	     }
	 } @Index;
    # name orderd index.
    @Index = sort { $a->{name} cmp $b->{name} } @Index;
    make_index_save_cache('name',@Index);

    @Index = sort { $b->{"time"} <=> $a->{"time"} } @Index;
    make_index_save_cache('time',@Index);

    foreach my $func ($PlugIns->list('make_index')){
	my($order,@MyIndex) = &$func(@Index);
	make_index_save_cache($order,@MyIndex);
    }
}

sub make_index_save_cache{
    my $index_name = shift;
    my(@Index) = @_;
    my $content;
    foreach (@Index){
	$content .= 
	    sprintf(" *\\[%s]<a href='%s?time=%s'>%s</a>%s\n"
		    ,$_->{moddate}
		    ,$_->{URI}
		    ,$_->{time}
		    ,$_->{name}
		    ,$_->{misc}
	    );
    }
    # save to cache
    my $pagename;
    $pagename = 'IndexPageByTime' if($index_name eq 'time');
    $pagename ||= 'IndexPage';
    $content = main::makeView($pagename,$content,'template');
    $root->__('__')->__('IndexPages')->$index_name($content);
}

sub time_str{
    my($time) = @_;
    my(%time);
    @time{qw(sec min hour mday mon
		year wday yday isdist)} = localtime($time);
    $time{year} += 1900;
    $time{mon}++;
    return sprintf("%04d/%02d/%02d %02d:%02d",
		   @time{qw(year mon mday hour min)});
}

1;
