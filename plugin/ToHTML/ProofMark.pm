package plugin::ToHTML::ProofMark;
use strict;
use plugin::ToHTML::Common;

sub install{
    my $class   = shift;
    my $PlugIns = shift;
    $PlugIns->add('PreConvert',\&PreConvert);
}

sub PreConvert{
    my $pagename = shift;
    my $content  = shift;
    return $content unless $content =~ />>/;
    return eval_without_pre(\&_Convert,$content);
}

sub _Convert{
    my $content = shift;
    $content =~ s{([^\\])\[(.*?)>>(.*?)\]}{$1<del>$2</del><ins>$3</ins>}mg;
    return $content;
}
1;
