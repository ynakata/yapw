package plugin::ToHTML::DL;
use strict;
use plugin::ToHTML::Common;

sub install{
    my $class   = shift;
    my $PlugIns = shift;
    my $root    = shift;

    $PlugIns->add('PostConvert',\&PostConvert);
}

sub PostConvert{
    my $pagename = shift;
    my $content  = shift;
    return $content unless $content =~ /^:.+?:/m;
    return eval_without_pre(\&_PostConvert,$content);
}

sub PLAIN{0}
sub DL   {1}

sub _PostConvert{
    my $content = shift;
    my $result;
    my $mode = PLAIN;
    foreach(split(/\n/,$content)){
	if(/^:(.+?):(.*)$/){
	    $result .= "<dl>" if $mode == PLAIN;
	    $mode = DL;
	    $result .= "<dt>$1</dt><dd>$2</dd>\n";
	}elsif($mode == DL && /^\s+:(.*)$/){
	    $result .= "<dd>$1</dd>\n";
	}else{
	    $result .= "</dl>" if $mode == DL;
	    $result .= "$_\n";
	}
    }
    return $result;
}
1;
