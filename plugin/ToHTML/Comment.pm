package plugin::ToHTML::Comment;
use strict;
use plugin::ToHTML::Common;

my $PlugIns;

sub install{
    my $class   = shift;
    $PlugIns = shift;
    my $root    = shift;

    $PlugIns->add('PreConvert',\&PreConvert);
}

sub PreConvert{
    my $pagename = shift;
    my $content  = shift;
    $content = eval_without_pre(\&_remove_comment,$content);
    return $content;
}

sub _remove_comment{
    my $text = shift;
    $text =~ s{^\/\/.*?$}{}mg;
    return $text;
}

1;
