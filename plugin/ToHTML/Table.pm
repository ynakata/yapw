package plugin::ToHTML::Table;
use strict;
use plugin::ToHTML::Common;

sub install{
    my $class   = shift;
    my $PlugIns = shift;
    my $root    = shift;

    $PlugIns->add('PostConvert',\&PostConvert);
}

sub PostConvert{
    my $pagename = shift;
    my $content  = shift;
    return eval_without_pre(\&_Convert,$content);
}

sub _Convert{
    my($text) = @_;
    my $is_table = 0;
    my $esc_line = 0;
    my($table,$result,$table_opt);
    foreach(split(m{\n}s,$text)){
	if($esc_line){
	    if(s/\\$//s){
		$esc_line = 1;
		$table .= "$_\\n";
	    }else{
		$esc_line = 0;
		$table .= "$_\n";
	    }
	    next;
	}
	if(/^\|\|(.*?)\|\|/s && !$table_opt){
	    $is_table = 1;
	    $table_opt = $1;
	    next;
	}elsif(s/^\|//s){
	    $is_table = 1;
	    if(s/\\$//s){
		$esc_line = 1;
		$table .= "$_\\n";
	    }else{
		$table .= "$_\n";
	    }
	    next;
	}else{
	    if($table){
		$is_table = 0;
		$result .=  _table_maketable($table_opt,$table);
		$table = ""; $table_opt = "";
	    }
	    $result .= "$_\n";
	}
    }
    $result .= _table_maketable($table_opt,$table) if($table);
    $result =~ s/\\n/\n/g;
    return $result;
}

sub _table_maketable{
    my($table_opt,$text) = @_;
    my($result);
    $table_opt = " ".$table_opt if($table_opt);
    my(@rowspan_order);
    foreach(reverse split("\n",$text)){
	my($line,@line,$row_no);
	@line = split(/\|/,$_);
	$row_no = 0;
	while(@line){
	    $_ = shift(@line);
	    while(s/\\$//){
		$_ =  shift(@line)."|";
	    }
	    if(s/^\^\s*$//){
		$rowspan_order[$row_no]++;
		$row_no++;
		next;
	    }
	    my($rowspan);
	    if($rowspan_order[$row_no] > 0){
		$rowspan = sprintf(" rowspan=%d",($rowspan_order[$row_no]+1));
		$rowspan_order[$row_no] = undef;
	    }
	    my($colspan);
	    if(s/^(~+)//){
		$colspan = " colspan='".(length($2) + 1)."'";
	    }elsif(s/^(\d+)~//){
		$colspan = " colspan='".($1)."'";
	    }
	    my($span_order) = "$colspan$rowspan";
	    if(s/^\*/$1/){
		$line .= "<th$colspan>$_</th>";
	    }elsif(s/^\!/$1/){
		$line .= "<td class='strong'$span_order>$_</td>";
	    }else{
		$line .= "<td$span_order>$_</td>";
	    }
	    $row_no++;
	}
	$result = "<tr>$line</tr>\n".$result if($line);
    }
    $result = "<table$table_opt>\n$result</table>\n" if($result);
    return $result;
}
1;

