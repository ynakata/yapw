package plugin::ToHTML::Common;
use strict;
require Exporter;
our(@ISA) = qw(Exporter);
our(@EXPORT) = qw(eval_without_pre);

sub eval_without_pre{
    my $func = shift;
    my $text = shift;
    my $result;
    foreach(split(m{(<pre>.*?</pre>)}s,$text)){
	if(/^<pre>/){
	    $result .= $_;
	}else{
	    $result .= &$func($_,@_);
	}
    }
    return $result;
}

1;
