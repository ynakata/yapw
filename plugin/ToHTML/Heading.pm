package plugin::ToHTML::Heading;
use strict;
use plugin::ToHTML::Common;

my $listofheadings;
my $PlugIns;

sub install{
    my $class   = shift;
    $PlugIns = shift;
    my $root    = shift;

    $PlugIns->add('PostConvert',\&PostConvert);
}

sub PostConvert{
    my $pagename = shift;
    my $content  = shift;
    my $count;
    $content = eval_without_pre(\&_heading,$content,\$count);
    $content =~ s/(^|[^\\])\%listofheadings\%/$1$listofheadings/g;
    return $content;
}

sub _heading{
    my $text = shift;
    my $count = shift;

    my($lv,$lv_prev,$anchor,$title,@anchor);
    $text =~ s/^(\@+)(.*)$/{
	$$count++;
	$lv = length($1) + 1;
	undef(@anchor) if($lv < $lv_prev);
	$lv_prev = $lv;
	$title = $2;
	$anchor = $2;
	$anchor =~ s{<.*?>}{}g;
	$anchor[$lv - 2] = $anchor;
	$anchor = join(':',@anchor);
	my $opt_text;
	foreach my $func ($PlugIns->list('ToHTML::Heading::_heading')){
	    &$func(\$lv,\$title,\$anchor,\$opt_text,$$count);
	}
	$listofheadings .= ' ' x $lv . sprintf("* <a href='%%self%%\#%s'>%s<\/a>\n",$anchor,$title);
	sprintf("<h%d>%s<a name='%s' href='%%self%%\#%s'>.<\/a>%s<\/h%d>\n"
		      ,$lv,$title,$anchor,$anchor,$opt_text,$lv)}/mgex;
    return $text;
}
1;
