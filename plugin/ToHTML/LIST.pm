package plugin::ToHTML::LIST;
use strict;
use plugin::ToHTML::Common;

sub install{
    my $class   = shift;
    my $PlugIns = shift;
    my $root    = shift;

    $PlugIns->add('PostConvert',\&PostConvert);
}

sub PostConvert{
    my $pagename = shift;
    my $content  = shift;
    return $content unless $content =~ /^(?:\s|��)*\*/m;
    return eval_without_pre(\&_Convert,$content);
}

sub _Convert{
    my $content = shift;

    my($result);
    my(@level);
    my($mode) = 0;
    foreach(split(/\n/,$content)){
	/^((?:\s|��)*)(.*?)$/;
	my $level     = length($1);
	my $indent    = ' ' x $level;
	my $space     = $1;
	my $content   = $2;
	my $list_mark = 0;
	do{$level++; 
	   $list_mark++;} if($content =~ s/^\*/<li>/);
	$list_mark++ if($content =~ s/^<li>\#/<li>/);
	
	my $current = $level[0];
	if($level < $current->{level}){
	    $result .= _unshift_level($level,\@level,\$mode);
	    $result .= '</li>' if($mode && $list_mark);
	}elsif($level == $current->{level}){
	    $result .= '</li>' if($mode && $list_mark);
	}else{# if($indent > $current->{level});
	    if($list_mark){
		my $type = ('','ul','ol')[$list_mark];
		unshift(@level,{ level => $level,
				 type  => $type       });
		$result .= ' ' x ($level - 1) . "<$type>\n";
	    }
	}
	$result .= $space.$content."\n";
	$mode |= $list_mark;
	$mode = 0 if($level[0] eq '');
    }
    $result .= _unshift_level(0,\@level,\$mode);
    $result =~ s{\n(\s*)</li>}{</li>\n$1}g;
    return $result;
}

sub _unshift_level{
    my($indent,$level,$mode) = @_;
    my($result,$lv,$ty);
    while(@$level){
	$lv = $$level[0]->{level};
	$ty = $$level[0]->{type};
	last unless ($indent < $lv);
	$result .= ' ' x ($lv - 1) . "</li></$ty>\n" if($$mode);
	shift(@$level);
    }
    $mode = 0 if($lv eq '');
    return $result;
}
1;

