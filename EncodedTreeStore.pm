package EncodedTreeStore;
#
# $Id: EncodedTreeStore.pm,v 1.4 2012/02/10 15:37:49 white Exp $
#
use strict;
use TreeStore;
use vars qw(@ISA $AUTOLOAD);
use Jcode;
my $filenameCode = 'utf8';
my $internalCode = 'euc';

@ISA = qw(TreeStore);

sub join_path{
    my($self) = @_;
    @_ = &TreeStore::__PARSEARG(@_);
    my $last = pop(@_);
    $last = &filenameEncode($last);
    push(@_,$last);
    $self->SUPER::join_path(@_);
}

sub read_dir{
    my($self) = shift;
    return grep {$_ = filenameDecode($_)} $self->SUPER::read_dir(@_);
}

sub filenameEncode{
    local($_) = @_;
Jcode::convert(\$_,$filenameCode);
    s/([\/\-\*\%\<\>\|])/sprintf("%%%02X",unpack("C", $1))/eg;
    return $_;
}

sub filenameDecode{
    local($_) = @_;
    s/\%([0-9A-Fa-f][0-9A-Fa-f])/pack("C", hex($1))/eg;
Jcode::convert(\$_,$internalCode);
    return $_;
}
