#!/usr/bin/perl
use lib qw(./);
require 'jcode.pl';
use yapw;

@Index = yapw::readIndex('array');
foreach $line(@Index){
    print STDERR "$line ";
}print STDERR "\n";

foreach $subject (@Index){
    local(@source,@Findex);
    $source = yapw::readSource($subject,'scalar');
    @Findex = yapw::makeFindex($source);
    $Findex{$subject} = \@Findex;

    print STDERR "FIndex $subject: ";
    foreach $line(@Findex){
	print STDERR "$line ";
	$RIndex{$line} .= "$subject\n";
    }print STDERR "\n";
}

#foreach $subject (@Index){
#    $ref = $Findex{$subject};
#    foreach(@$ref){
#	print $_;
#	$RIndex{$_} .= "$subject\n";
#    }
#}

foreach $subject (@Index){
    print STDERR "RINDEX $subject:\n";
    print STDERR $RIndex{$subject};
    print STDERR "\n";
    @list = split(/\n/,$RIndex{$subject});
    &yapw::makeRindex($subject,\@list);
}
