package TreeStore;
#
# $Id: TreeStore.pm,v 1.28 2005/07/12 09:51:41 white Exp $
#
use strict;
use vars qw(@ISA $AUTOLOAD @EXPORT);

my(%cache);
my($BaseDir);
my($CREATE_MODE_DIR)  = 0777;
my($CREATE_MODE_FILE) = 0666;

use constant OBJECT   => 1;
use constant PROPERTY => 2;
use constant UNDEF    => 0;

sub DPRINTOBJ{
    my $obj = shift;
    return sprintf "%s:%s:%s\n",ref($obj),$obj->{Type},$obj->{path};
}

sub new{
    my $self  = shift;
    my $class = ref($self) || $self;
    my($base) = bless { BASE => 1 } , $class;
    $self = {path => "" , base => $base} if($class eq $self);
    my $root = $base->read(@_);
    return bless $root,"TreeStore::Object";
}

sub create_mode{
    my $self = shift;
    if(ref($self)){
	my $mode = shift;
	$self->{'CREATE_MODE_FILE'} = $mode;
	$self->{'CREATE_MODE_DIR'}  = ($mode | 0111);
    }else{
	my $mode = $self;
	$CREATE_MODE_DIR  = $mode;
	$CREATE_MODE_FILE = ($mode | 0111);
    }
}

sub read{
    my($base,$path) = @_;

    if(not defined $base->{cache}->{$path}){
	$base->{cache}->{$path} = { path => $path, base => $base };
	my $child = $base->{cache}->{$path};
	if(-d $path){
	    $child->{Type} = OBJECT;
	}elsif(-f _){
	    my @read = $base->read_file($path);
	    $child->{Type}  = PROPERTY;
	    $child->{value} = \@read;
	}else{
	    $child->{Type}  = UNDEF;
	}
    }
    return $base->{cache}->{$path};
}

sub read_dir{
    (my $self,my $path,@_) = &__PARSEARG(@_);
    my @result = $self->read_dir_cache($path);
    if(wantarray){
        return @result;
    }else{
        return join("\n",@result);
    }
}

sub read_file{
    (my $self,my $path,@_) = &__PARSEARG(@_);
    my @result = $self->read_file_cache($path);
    if(wantarray){
        return @result;
    }else{
        return join("\n",@result);
    }
}

sub read_file_cache{
    (my $self,my $path) = @_;
    if(not defined $self->{cache}->{$path}->{value}){
	open(READ,"< $path");
	my @read = <READ>;
	close(READ);
	chomp(@read);
	$self->{cache}->{$path}->{value} = \@read;
    }
    return @{$self->{cache}->{$path}->{value}};
}

sub read_dir_cache{
    (my $self,my $path) = @_;
    if(not defined $self->{cache}->{$path}->{value}){
	opendir(DIR,"$path");
	my(@read)= readdir(DIR);
	closedir(DIR);
	
	my %tmp;
	map {$tmp{$_} = $_} @read;
	delete $tmp{"."};
	delete $tmp{".."};
	$self->{cache}->{$path}->{value} = [ keys %tmp ];
    }
    return @{$self->{cache}->{$path}->{value}};
}

sub write_file{
    (my $self,my $path,@_) = &__PARSEARG(@_);
    my($content) = shift;
    my($parent)= $path =~ /^(.*)\/.*?$/;
    if($parent =~ /^\/*$/){
	print STDERR "abort open $path [$parent]\n";
    }
    MK_PARENT_DIR($parent);
    open(WRITE,"> $path") or warn "open error $path [$parent]";
    print WRITE $content;
    close(WRITE);
#    my @tmp = split(/\n/,$content);
#    $self->{cache}->{$path}->{value} = \@tmp;
}

sub MK_PARENT_DIR{
    my $dir = shift;
    my($parent)= $dir =~ /^(.*)\/.*?$/;
    if($parent =~ /^\/*$/){
	warn "no parent";
	return undef;
    }
    if(! -e $parent){
	warn "recursive mkdir '$parent'";
	MK_PARENT_DIR($parent);
    }
    if(! -e $dir){
	mkdir($dir,$CREATE_MODE_DIR) or warn "mkdir error $dir";
    }
    return 1;
}

sub join_path{
    (my $self,@_) = &__PARSEARG(@_);
    local($_);
    while(@_){
	$_ = shift(@_);
	last if($_ !~ /^\/*$/);
    }
    my $path = $_;
    foreach(@_){
	next if($_ =~ /^\/*$/);
	if($path =~ /\/$/){
	    $path .= $_;
	}else{
	    $path .= "/$_";
	}
    }
    return $path;
}

sub __PARSEARG{
    my($path) = shift;
    my($self);
    if(ref($path)){
	$self = $path;
	$path = shift;
	$path = $path->{path} if ref($path);
    }
    return ($self,$path,@_);
}

sub delete{
    my($self,$path) = @_;
    print STDERR "deleteing '$path'.\n";
    if(-d $path){
	rmdir($path) or print STDERR "rmdir $path failed\n";
    }elsif(-f $path){
	unlink($path) or print STDERR "unlink $path failed\n"
    }
}

sub DESTROY{
# Flush out objects
    my($self) = shift;
    foreach(sort {length($b) <=> length($a)} keys %{$self->{cache}}){
	my $child = $self->{cache}->{$_};
#	print STDERR "$child->{path} $child->{Type} $child->{Modify} $child->{value}\n";
	if($child->{Modify} eq 'delete'){
	    if($child->{Type} == OBJECT){
		$self->delete($child->{path})
	    }elsif($child->{Type} == PROPERTY){
		$self->delete($child->{path});
	    }
	    $child->{Modify} = 'deleted';
	}elsif($child->{Modify} eq 'deleted'){
	}else{
#	    print " $child->{path}";
	    my($access,$renew);
	    if($child->{SetTimestamp}){
		$access = (stat($child->{path}))[8];
		$renew  = $child->{SetTimestamp};
#		print ":SetTimestamp";
	    }
	    if($child->{Modify} eq 'write'){
		$self->write_file($child->{path},join("\n",@{$child->{value}}));
#		print ":write";
	    }
	    utime($access,$renew,$child->{path}) if($child->{SetTimestamp});
#	    print "\n";
	}
    }
}
1;

package TreeStore::Object;
use strict;
use vars qw(@ISA $AUTOLOAD);

sub __{
    my($self) = shift;
    local($_);
    if(scalar(@_) > 0){
	if(not defined($_[0])){
	    # remove object.
warn "set delete : $self->{path}";
	    my(@list) = $self->__();
	    foreach (@list){
		my $child = $self->__($_);
		if(ref($child) && $child != $self){
		    $child->__(undef);
		}else{
		    $AUTOLOAD = ref($self)."::".$_;
		    $self->AUTOLOAD(undef);
		}
	    }
	    my $cache = $self->{base}->{cache}->{$self->{path}};
	    $cache->{value}  = [];
	    $cache->{Modify} = 'delete';
	}else{
	    # return listed childs
	    if(wantarray){
		my(@return);
		foreach(@_){
		    $AUTOLOAD = ref($self)."::".$_;
		    my $ret = $self->AUTOLOAD();
		    push(@return,$ret) if ref($ret);
		}
		return @return;
	    }else{
		my $ret = shift;
		$AUTOLOAD = ref($self)."::".$ret;
		$ret = $self->AUTOLOAD(@_);
		$ret = $self if !ref($ret);
		return $ret;
	    }
	}
    }
    if(wantarray){
	return $self->{base}->read_dir($self);
    }else{
	return $self;
    }
}

sub __timestamp{
    my $self = shift;
    my($renew);
    if(ref($_[0])){
	$_ = shift;
	$renew  = $_->{set};
    }
    if(scalar(@_) == 0){
	@_ = $self->__();
    }
    my $result = 0;
    my($base) = $self->{base};
    foreach my $target (@_){
	my $path = $base->join_path($self->{path},$target);
	my $cache = $base->{cache}->{$path};
	if($renew){
	    $cache->{path}         = $path;
	    $cache->{SetTimestamp} = $renew;
#	    print "Setting $cache->{path}\n";
	}
	my $timestamp = $cache->{TimeStamp} || (stat($path))[9];
	$result = $timestamp if ($timestamp > $result);
    }
    return $result;
}

sub __isWriteable{
    my $self = shift;
    my $path = $self->{path};
    return (-w $path);
}

sub __path{
    my $self = shift;
    my $path = $self->{path};
    TreeStore::MK_PARENT_DIR("$path/__");
    return $path;
}

sub AUTOLOAD{
    my($self) = shift;
    my($class,$method) = $AUTOLOAD =~ /^(TreeStore::Object::)(.+?)$/;

    my($base) = $self->{base};
    my($path) = $base->join_path($self->{path},$method);

    my $child = $base->read($path);
    if($child->{Type} == TreeStore::UNDEF() && scalar(@_) == 0){
	if(wantarray){
	    return ();
	}else{
	    return bless {path => $child->{path},
			  base => $child->{base},
			  Type => $child->{Type} };
	}
    }
    if($child->{Type} == TreeStore::PROPERTY()
       or $child->{Type} == TreeStore::UNDEF() ){
	#if $child is property(file)
	if(scalar(@_) > 0){
	    # Modify Property
	    if(not defined($_[0])){
		$child->{value}  = [];
		$child->{Modify} = 'delete';
		#delete file
	    }else{
		my(@content) = @_;
		my $cache = $base->{cache}->{$self->{path}};
		$child->{value}  = \@content;
		$child->{Type} = TreeStore::PROPERTY();
		$child->{Modify} = 'write';
		$child->{TimeStamp} = time();
		$base->write_file($path,join("\n",@{$child->{value}}));
	    }
	    return $base->{cache}->{$path}->{value};
	}else{
	    # Fetch Property
	    if(wantarray){
		return @{$child->{value}};
	    }else{
#		printf "%s %s %s\n",$self->{path},$method,$child->{value};
		return join("\n",@{$child->{value}});
	    }
	}
    }elsif($child->{Type} == TreeStore::OBJECT()){
	if(scalar(@_) > 0){
	    if(not defined($_[0])){
		$child->{Modify} = 'delete';
	    }
	}
	if(wantarray){
	    return $base->read_dir($child);
	}else{
	    return bless {path => $child->{path},
			  base => $child->{base},
			  Type => $child->{Type} };
	}
    }
}

sub DESTROY{
    my($self) = shift;
    my $base = $self->{base};
    $base->DESTROY();
#    print STDERR "--\n";
    foreach(keys %{$base->{cache}}){
	$_ = $base->{cache}->{$_};
#	print STDERR "  $_->{path} $_->{Type} $_->{Modify} $_->{value}\n";
    }
}

1;

