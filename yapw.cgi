#!/usr/bin/perl
#
# yapw.cgi ( http://white.d51.net/yapw/yapw.cgi )
# $Id: yapw.cgi,v 1.259 2013/03/25 16:22:31 white Exp $
use lib qw(./);
use Jcode;
use EncodedTreeStore;
use strict;
no strict "refs";
use Algorithm::Diff qw(traverse_sequences);
use MyCGIlib qw(:ALL);
umask 0;

my $DEBUG_ON = 0;

#
# export-mode setting.
#
use vars qw(%mode %TemplateFILE
	    %exportlist %exportlist_r
	    $ExportDIR $ExportPageList $DEBUG);

if($ARGV[0] =~ /^--export(.*?)/){
    shift;
    $mode{export} = 1;
    $ExportDIR = "EXPORT";
    $TemplateFILE{export} = "export.html";
}

# To Do:
#   * Auto embedding <base> element.
#
my(%cgi,%cookie,%TemplateCache,%cache,%Index,$commit_error);
my(%RealName);
my(%AliasName);
my($root);
my($target);

END{
#    print STDERR "request over:$target\n";
}

use vars
    qw($Test %PATTERN
       $SIGN $ANONYSIGN
       $dataDIR $TemplateDIR
       $DEFAULT_INDEX
       $URLencoding $ENABLE_SUEXEC $VERSIONING_SECOND
       $KeepBraket $NonExistMark $TIMESTAMPQUERY_DISABLE
       $HTTPDIR $SERVER $CGIpath $baseURL $SELFNAME $rootlevel
       $BR_SENSITIVE
       $enableElems $enableEmptyElems
       $enableInlineElems $enableInlineBlockElems 
       $enableBlockElems $enableNonEmptyElems
       $mustcloseBlockElems $uncloseableBlockElems
       $NameSpaceMark $Protocol
       @PLUGIN $PlugIns
       );

default_config();
require 'yapw.conf';
commit_config();

$root = new EncodedTreeStore($dataDIR);
if($root->{Type} eq TreeStore::UNDEF){
    mode_error("error : データディレクトリがありません。");
    exit(0);
}


if($ARGV[0] =~ /^--rebuild_rindex/){
    shift;
    my @Index =  $root->__();
    my(%Findex,%RIndex);
    foreach my $subject (@Index){
	print STDERR "read SOURCE: $subject\n";
	my $source = $root->__($subject)->SOURCE();
	my @Findex = makeFindex($source);
	$Findex{$subject} = \@Findex;
	foreach my $line(@Findex){
	    $RIndex{$line} .= "$subject\n";
	}
    }
    foreach my $subject (@Index){
	my(@list) = split(/\n/,$RIndex{$subject});
	$root->__($subject)->RINDEX(@list);
	print STDERR "write RINDEX: $subject\n";
    }
    exit;
}


# yapw.cgi is modal:
#  index, view, edit, set sign, add, write and more

%cgi    = getQuery();
use YAML;
#warn Dump(%cgi);
$PlugIns = new YaPw::CORE::PluginManager(\%cgi);
foreach my $plugin (@PLUGIN){
    $PlugIns->install($plugin,$root);
}
%cookie = getCookie();
if(defined $ENV{PATH_INFO}){
    $target = $ENV{PATH_INFO};
    $target =~ s/^(\/$SELFNAME)+/\/$SELFNAME/;
    $target =~s/^\///;
    $target = URLdecode($target);
    # 内部コードはEUC-JP
    codeconv(\$target,'euc');
    if($target =~ /^$/){
	undef $target;
    }
    if(length($target)){
	$rootlevel = '../';
    }
    while($target =~ /\//g){
	$rootlevel .= '../';
    }
    foreach my $func ($PlugIns->list('resolve_pathinfo')){
	$target = &$func($target);
    }
    $target =~ s{yapw.cgi/?}{}g;
}

if($ENV{REQUEST_METHOD} eq "HEAD"){
    foreach my $func ($PlugIns->list('SpecialPage')){
	my($r,$page) = &$func($target,'HEAD');
	if($r){
	    print httpHead("");
	    exit(0);
	}
    }
    print httpHead($target);
    exit(0);
}

if(defined $ExportDIR){
    $URLencoding = "export";
    $ExportPageList = $ARGV[0];
    mode_export();
    exit(0);
}

if(defined $cgi{exec_setsign}){
    mode_setsign($cgi{setsign});
    if(length($cgi{add})){
	$target = $cgi{add};
	mode_edit($cgi{add});
    }elsif(length($cgi{search})){
	mode_search($cgi{search});
	exit(0);
    }
}elsif(defined $cgi{exec_add} && length($cgi{add})){
    $target = $cgi{add};
    mode_edit($target);
}elsif(defined $cgi{exec_search} && length($cgi{search})){
    mode_search($cgi{search});
    exit(0);
}

if(defined $cgi{order} || defined $cgi{list}){
    $target = "IndexPage";
}

if(not defined $target){
    mode_redirect($DEFAULT_INDEX);
}

foreach my $func ($PlugIns->list('SpecialPage')){
    my($r,$page) = &$func($target);
    if($r){
	print httpHead();
	print $page;
	exit(0);
    }
}

if(defined $target){
    mode_edit($target)    if defined $cgi{edit} && (length($target) > 0);
    mode_write($target)   if defined $cgi{write};
    mode_delete($target)  if defined $cgi{delete};
    mode_view($target);
}

# anything else
$target = $DEFAULT_INDEX;
if(existcheck($DEFAULT_INDEX)){
    mode_view($target);
}else{
    mode_edit($target);
}
exit(0);

# each mode functions 
#   / call from yapw.cgi

sub mode_setsign{
    my($sign) = @_;
    $sign = $cgi{setsign};
    $cookie{yapwsign} = $sign;
    print setCookie("yapwsign",$sign,{path => $HTTPDIR});
}

sub mode_view{
    my($target) = @_;
    mode_redirect() if(! existcheck($target));
    my $view =  makeView($target);
    print httpHead($target);
    print $view;
    print $DEBUG if $DEBUG_ON;
    exit(0);
}

sub mode_edit{
    my($target) = shift;
    my($content);
    if(@_){
	$content = shift;
    }elsif(existcheck($target)){
	$content  = $root->__($target)->SOURCE();
	$content  = "" if (ref $content);
    }

    foreach my $func ($PlugIns->list('pre_edit')){
	$content = &$func($target,$content);
    }
    $content  =~ s{\&}{\&amp;}g;
    $content  =~ s{\<}{\&lt;}g;
    $content  =~ s{\>}{\&gt;}g;
    print httpHead($target);
    $content = resolveTemplate("edit",$content,$target,"edit");
    if(@_){
	my $func = shift;
	&$func(\$content,\$target);
    }
    print resolveTemplate("base.html",$content,$target,"edit");
    exit(0);
}

sub mode_reedit{
    my($target) = @_;
    my $oldsource  = $root->__($target)->SOURCE();
    my $content    = $oldsource;
    foreach my $func ($PlugIns->list('pre_edit')){
	$content = &$func($target,$content);
    }
    my $content = difftext($content,$cgi{text});
    $content = join("\n",map {$_ =~ s/(.)$//;$_ = $1.$_ } split(/\n/,$content));

    $content  = "" if (ref $content);
    $content  =~ s{\&}{\&amp;}g;
    $content  =~ s{\<}{\&lt;}g;
    $content  =~ s{\>}{\&gt;}g;
    print httpHead($target);
    $content  = resolveTemplate("reedit",$content,$target,"edit");
    print resolveTemplate("base.html",$content,$target,"edit");
    exit(0);
}

sub mode_search{
    my($skey) = @_;
    my(@list) = searchWord($skey,1);

    my(@Index) = $root->__();
    foreach(@Index){
	push(@list,$_) if(/$skey/);
    }
    @list =
	map{ my $time =  $root->__($_)->__timestamp("SOURCE");
	     my $name = $_;
	     $name =~ s/\%/\\\%/g;
	     { time     => $time,
	       moddate  => time_str($time),
	       URI      => "$CGIpath/".URLencode($_),
	       name     => $name
	       }
	 } @list;
    if($cgi{order} eq 'time'){
	@list = sort { $b->{"time"} <=> $a->{"time"} } @list;
    }else{
	@list = sort { $a->{name} cmp $b->{name} } @list;
    }
    my $content = "%searchform%\n";
    foreach (@list){
	$content .= 
	    sprintf(" *\\[%s]<a href='%s?time=%s'>%s</a>\n",
		    @{%$_}{qw(moddate URI time name)}  );
    }
    print httpHead();
    print makeView("検索結果 [$skey]",$content);
}

sub mode_error{
    my($message) = @_;
    print httpHead();
    print makeView("Error",$message);
}

# override 'quotemeta'
sub quotemeta{
    local($_) = shift;
    s/([^A-Za-z_0-9\x80-\xff])/\\$1/g;
    return $_;
}

sub difftext {
    my($old,$new) = @_;
    local(@main::old) = split(/\n/,$old);
    local(@main::new) = split(/\n/,$new);
    local($main::result,$main::del,$main::add);
    traverse_sequences
	(
	 \@main::old,\@main::new,
	 {
	     MATCH     => sub {$main::result .= $main::del.$main::add.$main::old[$_[0]]." \n";$main::del  = ""; $main::add = ""} ,
	     DISCARD_A => sub {$main::del .= $main::old[$_[0]]."-\n"},
	     DISCARD_B => sub {$main::add .= $main::new[$_[1]]."+\n"},
	 }
	 );
    $main::result .= $main::del.$main::add;
    return $main::result;
}

sub clear_indent{
    my($source) = @_;
    my(@source) = split(/\n/,$source);
    foreach(@source){
	if(/^[^-+ ]/ && !/^$/){
	    return $source;
	}
    }
    foreach(@source){
	s/^[-+ ]//;
    }
    return join("\n",@source);
}

sub write_page{
    my($target) = shift;
    my($oldsource) = shift;
    my($newsource) = shift;
    foreach my $func ($PlugIns->list('pre_writepage')){
	$newsource = &$func($target,$oldsource,$newsource);
    }
    unless($root->__($target)->SOURCE()){
	unless($newsource){
	    warn "null write to $target ignored.";
	    return;
	}
    }
    addRindex($target,$oldsource,$newsource);
    dropRindex($target,$oldsource,$newsource);
    $root->__($target)->SOURCE("$newsource");
    foreach my $func ($PlugIns->list('post_writepage')){
	$target = &$func($target,$oldsource,$newsource);
    }
}

sub mode_write{
    my($target) = shift;
    my($newsource) = shift || $cgi{text};
    my($action) = shift;

    my ($mode);
    mode_reedit($target) if(conflictcheck($target));
    my $oldsource = $root->__($target)->SOURCE();
    if (ref $oldsource){
	warn "  warn: oldsource was ref.\n";
	$oldsource  = "";
    }
    $newsource = clear_indent($newsource);
    $newsource = staticAlias($newsource);
    foreach my $func ($PlugIns->list('updatepage')){
	($newsource,$action) = &$func($newsource,$target,$action);
	if($action eq 'abort'){
	    print redirect($target);
	    exit(0);
	}
    }
    # call plugin function.
    unless($mode){
	($newsource,$mode) = CallPluginDirectFunction($target,$newsource);
    }
    if($mode eq 'edit'){
	mode_edit($target,$newsource);
	exit(0);
    }
    if($oldsource ne $newsource){
	# create board if not exist.
	if(!existcheck($target)){
	    unless($newsource){
		warn "null write to $target ignored.";
		return;
	    }
	    $root->__($target)->SOURCE("");
	    $root->__($target)->RINDEX("");
	    $root->__($target)->RINDEX(searchWord($target));
	}
	write_page($target,$oldsource,$newsource);
    }
    print redirect($target);
    exit(0);
}

sub mode_redirect{
    my($target) = @_;
    print redirect($target);
    exit(0);
}

sub mode_delete{
    my($target) = @_;
    my $source = $root->__($target)->SOURCE();
    $source  = "" if (ref $source);
    if(is_empty($source)){
	$root->__($target)->__(undef);
	foreach my $func ($PlugIns->list('PostDelete')){
	    $target = &$func($target);
	}
 	print redirect();
 	print httpHead();
 	exit(0);
    }else{
 	print redirect($target);
    }
}

sub mode_export{
    open(ExportPageList,$ExportPageList);
    while(<ExportPageList>){
	my($orig,$filename) = split(/[\s\t]+/,$_);
	$exportlist{$orig} = $filename;
	$exportlist_r{$filename} = $orig;
    }
    close(ExportPageList);
    my @Index = $root->__();
    foreach(@Index){
	my $output = $exportlist{$_} || $_;
	print "$_\n";
	my $result = makeExport($_);
	open(FILE,"> $ExportDIR/$output");
	print FILE $result;
	close(FILE);
    }
}

sub existcheck{
    my($target) = @_;
    umask 0;
    if(!ref($root->__($target))){
	$DEBUG .="<p>  not exist:$target</p>\n";
	return 0;
    }
    return 0 if($root->__($target)->{Type} != TreeStore::OBJECT());
    if(ref($root->__($target)->RINDEX())){
	$root->__($target)->RINDEX("");
	$root->__($target)->RINDEX(searchWord($target));
    }
    if(ref($root->__($target)->STATE())){
	$root->__($target)->STATE("");
    }
    $DEBUG .="<p>  exist check returns 1 for $target</p>";
    return 1;
}

sub conflictcheck{
    my($target) = @_;
    if($root->__($target)->__timestamp('SOURCE') != $cgi{timestamp}){
	return 1;
    }else{
	return 0;
    }
}

sub redirect{
    my($target) = @_;
    my($filename,$timestamp);
    if($target eq ''){
	print "Location: $baseURL/$SELFNAME\r\n\r\n";
    }else{
	$filename  = URLencode($target);
	$timestamp = &TimestampQuery($target); 
	print "Location: $baseURL/$SELFNAME/$filename$timestamp\r\n\r\n";
    }
}

sub URLencode{
#ARG = $plain | RETURNS = $encoded;
    return &{$URLencoding."encode"}($_[0]);
}

sub URLdecode{
#ARG = $encoded | RETURNS = $plain;
    return &{$URLencoding."decode"}($_[0]);
}

sub codeconv{
    my $str_ref = shift;
    my $code    = shift;
    &Jcode::convert($str_ref,$code);
}

sub exportencode{
    local($_) = @_;
    $_ = $exportlist{$_} || $_;
    return $_;
}

sub exportdecode{
    local($_) = @_;
     $_ = $exportlist_r{$_} || $_;
     return $_;
}

sub httpHead{
    my($target) = shift;
    my(%opt) = @_;
    my $result;
    $opt{'content-type'} ||= 'text/html; charset=euc-jp';
    $result = 'Content-type: '.$opt{'content-type'}."\r\n";
    if(!$Test){
	my $time = Timestamp($target);
	my ($sec,$min,$hour,$day,$mon,$year,$wday,$yday,$stim) = gmtime($time);
	$wday = ("Sun","Mon","Tue","Wed","Thu","Fri","Sat")[$wday];
	$mon  = ("Jan","Feb","Mar","Apr","May","Jun"
		 ,"Jul","Aug","Sep","Oct","Nov","Dec")[$mon];
	$year += 1900;
	$result .=
	    sprintf("Last-Modified: %s, %02d %s %04d %02d:%02d:%02d GMT\r\n"
		    ,$wday,$day,$mon,$year,$hour,$min,$sec);
    }
    $result .= "\r\n";
    return $result;
}

sub getSign{
    $cookie{yapwsign} = $ANONYSIGN if($cookie{yapwsign} =~ /^\s*$/);
    return $cookie{yapwsign};
}

sub time_str{
    my($time) = @_;
    my(%time);
    @{%time}{qw(sec min hour mday mon
		year wday yday isdist)} = localtime($time);
    $time{year} += 1900;
    $time{mon}++;
    return sprintf("%04d/%02d/%02d %02d:%02d",
		   @{%time}{qw(year mon mday hour min)});
}

sub makeView{
    my($target,$content,$mode) = @_;
    my(%Index)   = $root->__();
    my(@Index)   = sort {length($a) <=> length($b);} keys %Index;

    $content||= $root->__($target)->SOURCE();
    $content  = undef if (ref $content);

    # call plugin function.
    unless($mode){
	($content,$mode) = CallPluginDirectFunction($target,$content);
    }

    $PATTERN{Index} = pattern_index();
    if(is_empty($content)){
	$content = &resolveTemplate("delete",'',$target);
    }elsif($mode eq 'through'){
	$content =~ s/(^|[^\\])\%sign\%/"$1".&getSign();/eg;
	return $content;
    }else{
	$content = makeHtml($target,$content,$mode);
    }
    return resolveTemplate("base.html",$content,$target,$mode);
}

sub makeExport{
    my($target) = @_;
    my $Template = readTemplate("export");

    my $content  = $root->__($target)->SOURCE();
    $content  = undef if (ref $content);
    $PATTERN{Index} = pattern_index();
    $content = makeHtml($target,$content);
    return resolveTemplate($TemplateFILE{export},$content,$target);
}

sub makeHtml{
    my($target,$content,$mode) = @_;

    unless($mode eq 'template'){
	$content =~ s/(^|[^\\])\%sign\%/"$1".&getSign();/eg;
    }
    foreach my $func ($PlugIns->list('preprocess')){
	$content = &$func($content,$target);
    }
    foreach my $func ($PlugIns->list('PreConvert')){
	$content = &$func($target,$content);
    }
    $content = WikiLink($content,$target);
    foreach my $func ($PlugIns->list('PostConvert')){
	$content = &$func($target,$content);
    }
    foreach my $func ($PlugIns->list('postprocess')){
	$content = &$func($content,$target);
    }
    $content = format_plain($content);
    foreach my $func ($PlugIns->list('lastprocess')){
	$content = &$func($content,$target);
    }
    $content =~ s/\\\[(.*?)\]/[$1]/g;

    return $content;
}

sub makeFindex{
#ARG = ($content) | RETURNS = (@FindexLIST);
    my($content) = @_;
    my(%result);

    $PATTERN{Index} = pattern_index();
    while($content =~ m{($PATTERN{Index})}og){
	$result{$1} = 1;
    }
    return sort keys %result;
}

sub compareFindex{
#ARG = (\@FindexLIST1,\@FindexLIST2) | RETURNS = @diffLIST;
# only returns entry defined in LIST1, but not defined in LIST2.
    my($NEWFindex,$OLDFindex) = @_;
    my($line,@diffLIST,%OLDFindex);
    local($_);
    foreach(@$OLDFindex){
	$OLDFindex{$_} = 1;
    }
    foreach $line(@$NEWFindex){
	push(@diffLIST,$line) if not defined $OLDFindex{$line};
    }
    return @diffLIST;
}

sub addRindex{
#ARG = ($referer,$oldsource,$newsource) | RETURNS = () | EFFECTS renew Rindexs;
    my($referer,$oldsource,$newsource) = @_;
    my(@oldIndex)   = makeFindex($oldsource);
    my(@newIndex)   = makeFindex($newsource);
    my(@targetLIST) = compareFindex(\@newIndex,\@oldIndex);
    foreach my $target (@targetLIST){
	my @rindex = $root->__($target)->RINDEX();
	$root->__($target)->RINDEX((@rindex,"$referer"));
    }
}

sub dropRindex{
#ARG = ($referer,$oldsource,$newsource) | RETURNS = () | EFFECTS renew Rindexs;
    my($referer,$oldsource,$newsource) = @_;
    my(@oldIndex)   = makeFindex($oldsource);
    my(@newIndex)   = makeFindex($newsource);
    my(@targetLIST) = compareFindex(\@oldIndex,\@newIndex);
    foreach my $target(@targetLIST){
	my(@content)= sort $root->__($target)->RINDEX();
	my $rindex;
	chomp(@content);
	foreach my $line(@content){
	    $rindex .= "$line\n" unless ($line eq $referer);
	}
	$root->__($target)->RINDEX($rindex);
    }
}

sub searchWord{
    my($key,$flag) = @_;
    my(@result);
    local($_);
    my(@Index) = $root->__();
    foreach my $func ($PlugIns->list('Search')){
	@Index = &$func($key);
	last;
    }
    foreach my $target (@Index){
	my $content = $root->__($target)->SOURCE();
	my $qkey = &quotemeta($key);
	$content  = undef if (ref $content);
	if($flag){
	    if($content =~ m{$qkey}og){
		push(@result,$target);
	    }
	}else{
	    my $context = namespace_context($target);
	    my $qkey = $key;
	    $qkey =~ s/($context)//;
	    $PATTERN{Index} = pattern_index($target,$qkey);
	    foreach( ($content =~ m{($PATTERN{Index}|$qkey)}g) ){
		if($_ =~ /$qkey/){
		    push(@result,$target);
		    last;
		}
	    }
	}
    }
    return @result;
}

sub Timestamp{
    my($target) = @_;
    if($target){
	foreach my $func ($PlugIns->list('resolve_pagename')){
	    $target = &$func($target);
	}
	foreach my $func ($PlugIns->list('SpecialPageTimestamp')){
	    my $time = &$func($target);
	    return $time if $time;
	}
	return $root->__($target)->__timestamp("SOURCE");
    }else{
	my($time) = 0;
	foreach($root->__()){
	    my $t = $root->__($_)->__timestamp("SOURCE");
	    $time = $t if $t > $time;
	}
	return $time;
    }
}

sub TimestampQuery{
    my($target)   = @_;
    if($TIMESTAMPQUERY_DISABLE != 1){
       return '?time='.Timestamp($target);
    }else{
       return;
    }
}

sub namespace_context{
    my($current) = @_;
    my(@context,$tmp);
    if($NameSpaceMark){
	foreach(split(/$NameSpaceMark/,$current)){
	    next unless($_);
	    if(length($tmp) == 0){
		$tmp = $_.$NameSpaceMark;
	    }else{
		$tmp = $tmp.$_.$NameSpaceMark;
	    }
	    push(@context,$tmp);
	}
    }
    return join('|',map{$_ = &quotemeta($_)}
		sort {length($b) <=> length($a)} @context);
}

sub pattern_index{
    my $Target = shift;
    $Target ||= $target;
    foreach my $func ($PlugIns->list('resolve_pagename')){
	$Target = &$func($Target);
    }
    local($_);
    # make context pattern
    my $context = namespace_context($Target);
    # make name-map.
    foreach (sort {length($a) <=> length($b)} ($root->__())){
	if(/^($context)(.*)$/){
            $RealName{$2} = $_ unless ($1.$2 eq $target);
        }
        $RealName{$_} = $_;
    }
    foreach (@_){
	$RealName{$_} = $_;
    }
    
    my(@pagenames);
    push(@pagenames,keys %RealName);
    foreach my $func ($PlugIns->list('pagenamelist')){
	push(@pagenames,&$func());
    }
    my(@Index) = sort {length($b) <=> length($a)} @pagenames;
	if(wantarray){
		return @Index;
	}else{
	    return join('|', map {$_ = &quotemeta($_)} @Index);
	}
}

sub realname{
    my $name = shift;
    return $RealName{$name};
}

sub WikiLink{
    local($_);
    my($content,$subject,$mode) = @_;
    my(@buffer,$result);

    $PATTERN{Index} = pattern_index();
    unless($PATTERN{PlugIns}){
	$PATTERN{PlugIns} ||= $PlugIns->parse_regexp();
	$PATTERN{PlugIns2} = $PATTERN{PlugIns};
	unless($PATTERN{PlugIns}){
	    $PATTERN{PlugIns}  = '';
	}else{
	    $PATTERN{PlugIns} .= '|';
	}
    }
    foreach(split(m{(  \\\\ | # \\ (escape backslash)
		       <a.*?>.*?</a> | # html's <a> element
		       <pre.*?>.*?</pre> | # html's <pre> element
		       \\?(?:$PATTERN{PlugIns}\[.*?\]) | 
		       </?(?:$enableElems)(?:\s.*?)?>| # enable elements
		       $PATTERN{URL})
		    }sxo,$content)){
	if(/^\\\\/){
	    $_ = '\\';
	}elsif( m{^\\} || m{^<a.*?>.*?</a>}i ){
	}elsif(m{^</?(?:$enableElems)(?:\s.*?)?>}o){
	    if(m{^(<pre.*?>)(.*?)(</pre>)}s){
		my($pre,$epre) = ($1,$3);
		$_ = $2;
		my(@save) = ($KeepBraket,$NonExistMark);
		$KeepBraket   = 1;
		$NonExistMark = '';
		s{&}{&amp;}g;
		s{<}{&lt;}g;
		s{>}{&gt;}g;
		s{(^|[^\\])(\[(.*?)\]|$PATTERN{Index})}{"$1".mklink($2,'pre')}eg;
		$_ = $pre.$_.$epre;
		($KeepBraket,$NonExistMark) = @save;
	    }
	}elsif(m{^$PATTERN{URL}}){
	    $_ = "<a href='$_'>$_</a>";
	}elsif(m{^<pre>}){
	}else{
	    unless($PATTERN{PlugIns2} &&
		   s{($PATTERN{TEXT})($PATTERN{PlugIns2})}{$1.mklink($2)}eog){
		if($mode ne 'noescape'){
		    s{&}{&amp;}g;
		    s{&amp;([\#a-zA-Z0-9]+);}{&$1;}g;
		    s{<}{&lt;}g;
		    s{>}{&gt;}g;
		}
		s{($PATTERN{TEXT})
		      (\\?)(\[(.*?)\]|
			    $PATTERN{Index}|
			    $PATTERN{WikiName})}{
				($2)?"$1$3":$1.mklink($3)}exg;
	    }
	}
	$result .= $_;
    }
    return $result;
}

sub mklink{
    my($name) = shift;
    my($opt)  = shift;
    my($face,$body,$label);
    my($prev,$post);

    # apply plugins
#    warn '[mklink: name=',$name,' opt=',$opt,']',"\n";
    unless($opt){
	if($name =~ s/^\[(.*?)\]$/$1/){
	    ($prev,$post) = ('[' , ']') if $KeepBraket;
	    foreach my $func ($PlugIns->list('linkconvert')){
		my($new,$end) = &$func($name,$target);
		return $new if($end);
	    }
	}
    }
    foreach my $func ($PlugIns->list('linkconvert2')){
	my $end;
	($name,$end) = &$func($name,$target);
	return $name if($end);
    }
    # parsing name
    ($face,$body) = mklink_splittext($name);
    # linking.
    if($body =~ /^$PATTERN{URL}/){
	return "<a href='$body'>$face</a>"; 
    }
    $PATTERN{Index} = pattern_index();
    if($body =~ /^($PATTERN{Index})(\#.*?)?$/){
	my $misc;
	my($name,$label) = ($1,$2);
	my $path = mklink_getpath($name,1)."$label";
	foreach my $func ($PlugIns->list('mklink')){
	    ($path,$face,$misc) = &$func($path,$face,$misc,$name,$label);
	}
	return "$prev<a href='$path'>$face</a>$misc$post";
    }
    if($body =~ /^\#/){
	my $misc;
	my $path = mklink_getpath($target,1).$body;
	foreach my $func ($PlugIns->list('mklink')){
	    ($path,$face,$misc) = &$func($path,$face,$misc,$name,$label);
	}
	return "$prev<a href='$path'>$face</a>$misc$post";
    }
    if($body =~ /^\*(.*)/){
	return "$prev<span id='$1'></span>$post";
    }
    if($NonExistMark){
	my $path  = mklink_getpath($body,0);
	if($body eq $face){
	    return "$prev$name$post<a href='$path?edit'>$NonExistMark</a>";
	}else{
	    return "$prev$face$post<a href='$path?edit'>$NonExistMark</a>";
	}
    }else{
	return "$prev$name$post";
    }
}

sub mklink_splittext{
    my($text) = shift;
    if($text =~ /.,./){
	return split(/,/,$text,2);
    }else{
	return $text, $RealName{$text} || $text;
    }
}

sub mklink_getpath{
    my($name,$enable_timestamp) = @_;
    if($mode{export}){
	return $exportlist{$name} || $name;
    }
    my $time  = TimestampQuery($name) if $enable_timestamp;
    return $rootlevel.$SELFNAME.'/'.URLencode($name).$time;
}

sub readTemplate{
#ARG = $templateName | RETURNS $Template;
#  %Template cashes Template files.
    my($label) = @_;
    my(%Template);
    if(exists $Template{$label}){
	return $Template{$label};
    }else{
	return ($TemplateCache{$label} ||= readfile("$TemplateDIR/$label"));
    }
}

sub readfile{
    my($path) = @_;
    my($result);
    open(FILE,"< $path");
    if(wantarray){
	my @result = <FILE>;
	close(FILE);
	return @result;
    }else{
	while(<FILE>){
	    $result .= $_;
	}
	close(FILE);
	return $result;
    }
}

sub staticAlias{
#ARG = ($source) | RETURNS = ($source);
    my($source) = @_;
    my($result);
    my(@ign,@ign_esc);
    foreach my $func ($PlugIns->list('ignore_staticsign')){
	my $re = &$func;
	push(@ign,$re);
	push(@ign_esc,"\\\\$re");
    }
    my $re_split  = join('|',@ign_esc,@ign);
    my $re_ignore = join('|',@ign);
    my(@source);
    if($re_split){
	@source = split(m{($re_split)}io,$source);
    }else{
	@source = ($source);
    }
    foreach(@source){
	if(!$re_ignore || !/^(?:$re_ignore)/io){
	    s/(^|[^\\])\%sign\%/"$1".&doSign();/emg;
	}
	$result .= $_;
    }
    return $result;
}

sub resolveTemplate{
    my($TemplateName,$content,$subject,$mode) = @_;
    $content =~ s/\\\%/\\\\\%/mg if($mode eq "edit");
    local($_) = resolveTemplate_sub($TemplateName,$content,$subject,$mode);
    s{(^|[^\\])\%(.*?)\%}{(${$2})?"$1${$2}":"$1\%$2\%";}eg;
    s{(^|[^\\])\%findex\%}{$1.&showFindex($content,$subject);}se if($mode ne "edit");
    s/\\\%/\%/mg;
    return $_;
}

sub resolveTemplate_sub{
    my($TemplateName,$content,$subject,$mode) = @_;
    local($_) = readTemplate($TemplateName);

    s/(^|[^\\])\%navigation\%/$1.&resolveTemplate_sub("navigation",$content,$subject,$mode)/eg;
    s/(^|[^\\])\%sign\%/"$1".&getSign();/eg unless $mode eq 'template';
    s{(^|[^\\])\%content\%}{$1$content}g;
    s/(^|[^\\])\%searchform\%/"search:".$1.&resolveTemplate_sub("searchform",$content,$subject,$mode)/eg;
    s{(^|[^\\])\%baseURL\%}{$1$baseURL}g;
    s{(^|[^\\])\%_NavigationToIndexPages_\%}{$1.&NavigationToIndexPages()}eg;
    s{(^|[^\\])\%_NavigationToEdit_\%}{$1.&NavigationToEdit($subject)}eg;
    s{(^|[^\\])\%_NavigationToAdvance_\%}{$1.&NavigationToAdvance($subject)}eg;
    s{(^|[^\\])\%self\.edit\%}{$1%self%?time=%timestamp%&amp;edit}g;
    s{(^|[^\\])\%self\%}{$1%yapw%/%pathinfo%}g;
    s{(^|[^\\])\%yapw\%}{$1%rootlevel%$SELFNAME}g;
    s{(^|[^\\])\%rootlevel\%}{$1$rootlevel}g;
    s{(^|[^\\])\%cgiURL\%}{$1$CGIpath}g;
    s{(^|[^\\])\%pathinfo\%}{$1.&URLencode($subject)}eg;
    s{(^|[^\\])\%rindex\%}{$1.&showRindex($subject)}eg;
    s{(^|[^\\])\%timestamp\%}{$1.&Timestamp($subject)}eg;
    s{(^|[^\\])\%subject\%}{$1.$subject || "INDEX::"}eg;
    s{(^|[^\\])\%commit_error\%}{$1$commit_error}g;
    s{(^|[^\\])\%edittools\%}{$1.&CallPluginEditTools()}eg;
    return $_;
}

sub doSign{
#ARG = () | RETURNS = ($date_and_sign);
    my($SIGN,$sign,$date);
    $sign             = getSign()  if(defined $cgi{addsign});
    if(defined $cgi{adddate}){
	my($sec,$min,$hour,$day,$mon,$year,$wday,$yday,$isdist)
	    =localtime(time);$mon++;$year += 1900;
	$date = sprintf("%04d/%02d/%02d %02d:%02d"
			,$year,$mon,$day,$hour,$min);
    }
    $SIGN = "<span class='signature'>--$date $sign<\/span>" if($sign || $date);
    return $SIGN;
}

sub CallPluginDirectFunction
{
    my($target) = shift;
    my($content) = shift;

    my $return_mode;

    if(@PLUGIN){
	my $PLUGIN_PATTERN = join('|',@PLUGIN);
	foreach my $key (keys %cgi){
	    if($key =~ /^plugin::(?:$PLUGIN_PATTERN)/){
		my($directive);
		($content,$directive) = &{$key}($content,$target,\%cgi);
		if($directive eq 'redirect'){
		    mode_redirect($target);
		    exit;
		}elsif($directive eq 'through'){
		    $return_mode = 'through';
		}elsif($directive eq 'edit'){
		    $return_mode = 'edit';
		}
	    }
	}
    }
    return ($content, $return_mode);
}

sub CallPluginEditTools{
    my $result;
    foreach my $func ($PlugIns->list('EditTools')){
	$result .= &$func();
    }
    return $result;
}

sub NavigationToIndexPages{
    my $result;
    foreach my $func ($PlugIns->list('NavigationToIndexPages')){
	$result .= &$func();
    }
    return $result;
}

sub NavigationToEdit{
    my $result;
    my $ignore;
    foreach my $func ($PlugIns->list('IgnoreEdit')){
	$ignore ||= &$func($target);
    }
    if( !$ignore && $target && $root->__($target)->__isWriteable() ){
	$result .= '<a href="%self.edit%">編集</a>';
    }
    foreach my $func ($PlugIns->list('NavigationToEdit')){
	$result .= &$func();
    }
    return $result;
}

sub NavigationToAdvance{
    my $result;
    foreach my $func ($PlugIns->list('NavigationToAdvance')){
	$result .= &$func();
    }
    return $result;
}

sub getDate{
    my($sec,$min,$hour,$day,$mon,$year,$wday,$yday,$isdist)
	=localtime(time);$mon++;$year += 1900;
    return sprintf("%04d/%02d/%02d %02d:%02d",$year,$mon,$day,$hour,$min);
}

sub showRindex{
# prepare Rindex
    my($target) = @_;
    my(@result);
    my(@Rindex)   = sort $root->__($target)->RINDEX();
    foreach my $line(@Rindex){
	next if($line =~ /^\s*$/);
	next if($line eq $target);
	my $filename  = URLencode($line);
	my $timestamp = Timestamp($line);
	push(@result,"<a href='$CGIpath/$filename?time=$timestamp'>$line</a>\n");
    }
    return join(" | ",@result);
}

sub showFindex{
    my($content,$subject) = @_;
    my(%Findex,@Findex);
    while($content =~ m{(<a href=.*?>(.*?)</a>)}ig){
	my($body,$name) = ($1,$2);
	next if($name eq $NonExistMark);
	next if($name =~ /^<img/);
	next if($name eq $subject);
	$Findex{$name} = $body;
    }
    map{push(@Findex,$Findex{$_})}(sort keys %Findex);
    return join(" | ",@Findex);
}

sub is_empty{
    my($text) = @_;
    if($text =~ /^\s*$/){
	return 1;
    }else{
	return 0;
    }
}

sub eval_without_pre{
    my($func,$text) = @_;
    my $result;
    foreach(split(m{(<pre.*?>.*?</pre>)}s,$text)){
	if(/^\<pre/){
	    $result .= $_;
	}else{
	    $result .= &$func($_);
	}
    }
    return $result;
}

sub format_plain{
    return eval_without_pre(\&format_plain_core,@_);
}

sub format_plain_core{
    local($_) = @_;
    my($result,$elem,@paragraph,@temp,@elem);

    s{^\-\-\-\-}{<hr />}mg;
    @paragraph = split(m{(</?(?:$enableNonEmptyElems)(?: .*?)?>)}soi,$_);
    
#    foreach(@paragraph){
#	$DEBUG .= "$_\n";
#    }
    while(@paragraph){
	$_ = shift(@paragraph);
	if(m{<($enableBlockElems)( .*?)?>}oi){
	    my($elem) = $1;
	    push(@temp,$_);
	    while(@paragraph){
		$_ = shift(@paragraph);
		push(@temp,$_);
		last if(m{^<\/($elem)( .*?)?>}i);
	    }
	}else{
	    my $par;
	    while(@paragraph){
		if(m{</?($enableBlockElems)( .*?)?>}oi){
		    $par .= "\n\n$_";
		    $_ = "";
		    last;
		}
		$par .= $_;
		$_ = shift(@paragraph);
	    }
	    $par .= $_;
	    push(@temp,split(/(?:\s*?\n){2,}/s,$par));
	}
    } @paragraph = @temp;

    while(@paragraph){
	$_ = shift(@paragraph);
	if(m{</($enableBlockElems)>}oi){
	    my($elem) = $1;
	    while(@elem && $elem[0] ne $elem){
		shift(@elem);
	    }
	    shift(@elem);
	}elsif(m{^<($enableBlockElems)( .*?)?>}oi){
	    unshift(@elem,$1);
	}elsif(!@elem){
	    $_ = "<p>$_</p>\n";
	    s{(?:$PATTERN{CR}){2,}}{</p>\n<p>}smg;
	}
	s{\\\n}{}mg;
	s{\n{2,}}{</p><p>}mg;
	s/\/$/<br \/>\n/mg;
	s{\n}{<br />\n}mg if($BR_SENSITIVE);
	s{<(/?)($enableBlockElems|$enableInlineBlockElems)>(\n*)<br />}{<$1$2>$3}sog;
	s{<br />(\n*)\s*<(/?)($enableBlockElems|$enableInlineBlockElems)>}{$1<$2$3>}sog;
	s/^\s*<br \/>$//mg;
	s{</tr><br />}{</tr>}mg;
	s{<p>\s*</p>}{\n}g;
	$result .= "$_\n";
    }
    return $result;
}

sub commit_config{
    $enableBlockElems    = "$uncloseableBlockElems|$mustcloseBlockElems";
    $enableNonEmptyElems = "$enableInlineElems|$enableBlockElems";
    $enableElems         = "$enableNonEmptyElems|$enableEmptyElems";
}

sub default_config{
## default configurations
    $Test = 0;

# localhost configuration
    my $baseDIR      = ($ENV{SCRIPT_FILENAME} =~ /^(.*)\/.*?$/)[0];
    $dataDIR      = "./DATA";
    $TemplateDIR  = "./Template";
    $SELFNAME     = ($ENV{SCRIPT_FILENAME} =~ /^.*\/(.*?)$/)[0];
    $NameSpaceMark= "::";

# http configuration
    $HTTPDIR      = ($ENV{SCRIPT_NAME} =~ /^(.*)\/.*?$/)[0];
    $CGIpath      = $ENV{SCRIPT_NAME};
    $Protocol     = ($ENV{HTTPS})?'https':'http';
    $baseURL    ||= "$Protocol://$ENV{HTTP_HOST}$ENV{SCRIPT_NAME}";
    $baseURL      =~ s{/+}{/}g;
    $baseURL      =~ s{http:/}{http://};
    $baseURL      =~ s/\/([^\/]*?)$/\//;
    $ENABLE_SUEXEC  = 'no';

    $URLencoding  = 'url'; # url / base64
  
# Displaying configuration
    $KeepBraket = 0;
    $NonExistMark = '?';
    $BR_SENSITIVE     = 0;
    
# html-elements enable list
    $enableInlineElems =
	"a|em|strong|cite|dfn|code|samp|kbd|var|abbr|acronym|".
	    "sub|sup|pre|address|tt|i|b|s|big|small|span|del|ins";
    $uncloseableBlockElems  = "blockquote|p|pre|dl|ul|ol|h2|h3|h4";
    $mustcloseBlockElems    = "dt|dd|li|div";
    $enableInlineBlockElems = "del|ins";
    $enableBlockElems = "$uncloseableBlockElems|$mustcloseBlockElems|$enableInlineBlockElems";
    $enableEmptyElems = "img|br|hr";
    $enableNonEmptyElems = "$enableInlineElems|$enableBlockElems";
    $enableElems      = "$enableNonEmptyElems|$enableEmptyElems";
    
# user-sign&auth
    $ANONYSIGN = "somebody";
    $SIGN      = "";

# matching patterns
    $PATTERN{URL}    = '(?:(?:https?|ftp)://|mailto:)[\w;/?:@&=\+\$\.,\-!~\*\%\'\(\)\#]+';
    $PATTERN{WikiName}= '(?:[A-Z]+[a-z]+){2,}';
    $PATTERN{CR}     = '\r\n|\n|\r';
    $PATTERN{TEXT}   = '(?:^|\s|\G)(?:[\x00-\x7f]|[\x80-\xff][\x80-\xff])*?'
}

package YaPw::CORE::PluginManager;
use YAML;
sub new{
    my $class = shift;
    my $cgi   = shift;
#warn Dump($cgi);
    return bless {
	'_names_' => {},
       	'cgi' => $cgi,
    },$class;
}

sub install{
    my $self = shift;
    my $plugin = shift;
    my $root   = shift;

    my $module = "plugin::${plugin}";
    my $eval = 'require '.$module.';'."\n"
	.$module.'->install($self,$root);';
    eval $eval;
    if($@){
        die $@;
    }
    $self->{_names_}->{$plugin} = 1;
}

sub cgi{
    my $self = shift;
    return $self->{cgi};
}

sub is_installed{
    my $self = shift;
    my $plugin_name = shift;
    return exists($self->{_names_}->{$plugin_name});
}

sub add{
    my $self = shift;
    my $type = shift;
    my $func = shift;
    $self->{$type} ||= [];
    push(@{$self->{$type}},$func);
}

sub list{
    my $self = shift;
    my $type = shift;
    if(defined $self->{$type}){
	return @{$self->{$type}};
    }else{
	return ();
    }
}

sub parse_regexp{
    my $self = shift;
    my @re;
    foreach my $func ($self->list('parse_regexp')){
	push(@re,&$func());
    }
    return join('|',@re);
}
