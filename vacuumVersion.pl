#!/usr/bin/perl
use lib qw(./);
use strict;
use EncodedTreeStore;
use Algorithm::Diff qw(traverse_sequences);
sub difftext {
    my($old,$new) = @_;
    local(@main::old) = split(/\n/,$old);
    local(@main::new) = split(/\n/,$new);
    local($main::result,$main::del,$main::add);
    traverse_sequences
	(
	 \@main::old,\@main::new,
	 {
	     MATCH     => sub {$main::result .= $main::del.$main::add.$main::old[$_[0]]." \n";$main::del  = ""; $main::add = ""} ,
	     DISCARD_A => sub {$main::del .= $main::old[$_[0]]."-\n"},
	     DISCARD_B => sub {$main::add .= $main::new[$_[1]]."+\n"},
	 }
	 );
    $main::result .= $main::del.$main::add;
    return $main::result;
}
use vars
    qw($Test %PATTERN
       $SIGN $ANONYSIGN
       $dataDIR $TemplateDIR
       $DEFAULT_INDEX
       $URLencoding $ENABLE_SUEXEC $VERSIONING_SECOND
       $KeepBraket $NonExistMark $TIMESTAMPQUERY_DISABLE
       $HTTPDIR $SERVER $CGIpath $baseURL $SELFNAME
       $BR_SENSITIVE
       $enableElems $enableEmptyElems
       $enableInlineElems $enableInlineBlockElems 
       $enableBlockElems $enableNonEmptyElems
       $mustcloseBlockElems $uncloseableBlockElems
       $NameSpaceMark
       @PLUGIN $PlugIns
       );

$dataDIR = './DATA';
require 'yapw.conf';
my $root = new EncodedTreeStore($dataDIR);
print "$dataDIR\n";

my(@Index) = grep { $_ ne '__' } $root->__();
foreach my $page(@Index){
    print "target:$page\n";
    my $VERSION   = $root->__($page)->__('VERSION');
    my(@versions) = sort $VERSION->__();

    my $old     = shift @versions;
    my $old_src = $VERSION->$_();
    foreach my $new(@versions){
	my $new_src = $VERSION->$new();
	my $diffed = difftext($old_src,$new_src);
	if($diffed =~ /\-$/mg){
	    print "no  delete $old\n";
	}else{
	    print "delete $old\n";
	    $VERSION->$old(undef);
	}
	$old     = $new;
	$old_src = $new_src;
    }
}
